/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmap.cli;

import edu.upf.bg.colorscale.ColorScalePoint;
import edu.upf.bg.colorscale.IColorScaleHtml;
import edu.upf.bg.colorscale.impl.LinearColorScale;
import edu.upf.bg.colorscale.impl.LinearTwoSidedColorScale;
import edu.upf.bg.colorscale.impl.PValueColorScale;
import edu.upf.bg.progressmonitor.IProgressMonitor;
import edu.upf.bg.progressmonitor.NullProgressMonitor;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.dom.util.DOMUtilities;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.gitools.matrix.model.MatrixView;
import org.gitools.matrix.model.ObjectMatrix;
import org.gitools.matrix.sort.MatrixViewSorter;
import org.gitools.matrix.sort.ValueSortCriteria;
import org.gitools.persistence.PersistenceException;
import org.gitools.persistence.text.ObjectMatrixTextPersistence;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.svg.SVGDocument;

import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.Properties;

/**
 * Static class with the operations involved in the SVGMap image generation:
 *
 * This process is done in getRegionsSVGFileFromValue method, and it mainly consists
 * on given an SVG, a data file (with REGION + KEY + data columns), a keyword
 * and a column index perform a search through all the data file for all the rows
 * that contains the same keys that the input keyword,
 * get the value of the column selected and color the svg objects that have the same
 * name as the column region of the data file. The color can be determined by a
 * input scale otherwise from a default linear binary scale
 * 
 */

public class SvgOperations {

	private static Logger logger = Logger.getLogger(SvgOperations.class);

    public static IColorScaleHtml DEFAULT_SCALE = new LinearColorScale(
                new ColorScalePoint(0, Color.WHITE),
                new ColorScalePoint(1, Color.GREEN)
    );

	/**
	 *  Load an svg file from a string path
	 */
	public static SVGDocument readSVG(String path) throws IOException {

		String parser = XMLResourceDescriptor.getXMLParserClassName();
		SAXSVGDocumentFactory sax = new SAXSVGDocumentFactory(parser);

		File tmp = new File(path);
		SVGDocument doc = sax.createSVGDocument(tmp.toURI().toString());

		return doc;
	}

    public static MatrixView readMappingFile(String path) throws IOException, PersistenceException  {
        return readMappingFile(path, new NullProgressMonitor());
    }

	/**
	 * Return a MatrixView object from a path where the data file is located
	 * @param path
	 * @return
	 * @throws IOException
	 * @throws PersistenceException
	 */
	public static MatrixView readMappingFile(String path, IProgressMonitor progressMonitor) throws IOException, PersistenceException {

		ObjectMatrixTextPersistence dataMatrix = new ObjectMatrixTextPersistence();
		ObjectMatrix data = new ObjectMatrix();

		dataMatrix.read(new File(path), data, progressMonitor);

		MatrixView mv = new MatrixView(data);
		MatrixViewSorter.sortByLabel(mv, true, ValueSortCriteria.SortDirection.ASCENDING, false, ValueSortCriteria.SortDirection.ASCENDING);
		return mv;
	}

	/**
	 * Given a svg image and a matrixview object it retrieves another SVG with highlighted regions following an scale
	 * where it contains the searched value
	 *
	 */
	public static SVGDocument createSVGRegionsFromValue(SVGDocument svgOrigin, MatrixView data, int column, String searchValue, IColorScaleHtml sc, List<String> header) throws Exception {
		String id, key; Object obj; double val;
		try {

			svgOrigin = makeTooltip(svgOrigin);

			for (int i = 0; i < data.getRowCount(); i++) {
				key = (String) data.getRowLabel(i);
				if (searchValue.equalsIgnoreCase(key)) {

					for (int j = 0; j < data.getColumnCount(); j++) {
						if (data.getCell(i, j) != null) {

							Element elementTag = svgOrigin.getElementById(data.getColumnLabel(j));
							if (elementTag == null) {
								logger.info("Region referenced in data file not found in SVG image: "+ data.getColumnLabel(j));
								continue;
							}

							String text = elementTag.getAttribute("style");
							obj = data.getCellValue(i, j,column);

							if (obj != null && !obj.toString().equals("NaN")) {
								val = Double.parseDouble(obj.toString());
								text = fillElementStyle(text, val, sc);

								elementTag.setAttribute("style", text);
								elementTag.setAttribute("onmousemove", "ShowTooltip(evt, \'" + header.get(0) + ": " + data.getColumnLabel(j) + "\',\'" + header.get(column + 2)+ ": " + val + "\')");
								elementTag.setAttribute("onmouseout", "HideTooltip(evt)");

								//Painting the child nodes of the element
								NodeList n = elementTag.getChildNodes();
								Element aux = null;
								for (int k = 0; k < n.getLength(); k++)
								{
									try {
										if (n.item(k) instanceof Element) {
											aux = (Element) n.item(k);
											text = aux.getAttribute("style");
											obj = data.getCellValue(i, j, column);

											if (obj != null && !obj.toString().equals("NaN")) {
												val = Double.parseDouble(obj.toString());
												if (text == null || text.isEmpty()) {
													aux.setAttribute("style", "fill-opacity: 1;fill: " + sc.valueRGBHtmlColor(val));

												}
												else {
													text = fillElementStyle(text, val, sc);
													aux.setAttribute("style", text);
												}
												screenNode(aux, val, sc, data.getColumnLabel(j));
											}
										}
									} catch (Exception e) {
										continue;
									}
								}
							}

						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("SVG region processing error");
		}
		return svgOrigin;
	}

	private static String fillElementStyle(String text, double val, IColorScaleHtml sc) {
		int k = 0;
		boolean opc = false;
		if (sc == null)
			sc = DEFAULT_SCALE;

		String[] styleStr = text.split(";");
		for (String style : styleStr) {
			if (style.contains("fill-opacity")) {
				styleStr[k] = "fill-opacity: 1";
				opc = true;
			} else {
				if (style.contains("fill")) {
					styleStr[k] = "fill: " + sc.valueRGBHtmlColor(val);
				}
			}
			k++;
		}
		text = StringUtils.join(styleStr, ";");
		if (!opc) {
			text += ";fill-opacity: 1";
		}
		return text;
	}

	private static void screenNode(Element elementTag, double val, IColorScaleHtml sc, String region){

		Element aux;
		String text = "";

		NodeList n = elementTag.getChildNodes();
		for (int k = 0; k < n.getLength(); k++)
		{
			aux = (Element) n.item(k);
			text = aux.getAttribute("style");
			text = fillElementStyle(text, val, sc);
			aux.setAttribute("style", text);
			screenNode(aux,val,sc, region);
		}
	}

	public static SVGDocument makeScalable(SVGDocument svgDom) {

		Element svgRoot = svgDom.getDocumentElement();

		Element scriptElement = svgDom.createElementNS("svgNS", "script");
		scriptElement.setAttributeNS(null, "xlink:href", "js/SVGPan.js");

		Element gElement = svgDom.createElement("g");
		gElement.setAttribute("id", "viewport");

		while (svgRoot.hasChildNodes()) {
			gElement.insertBefore(svgRoot.getLastChild(), gElement.getFirstChild());
		}

		svgRoot.setAttributeNS(null, "width", "100%");
		svgRoot.setAttributeNS(null, "height", "100%");
		svgRoot.setAttributeNS(null, "id", "zoomableSVG");
		svgRoot.appendChild(scriptElement);
		svgRoot.appendChild(gElement);
		return svgDom;
	}

	public static SVGDocument makeTooltip(SVGDocument svgDom) throws IOException {

		String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
		
		Element svgRoot = svgDom.getDocumentElement();

		Element ele = svgDom.createElementNS(svgNS, "script");
		String scriptTxt = convertStreamToString(SvgOperations.class.getResourceAsStream("/tooltip/tooltip.js"));
		ele.setTextContent(scriptTxt);
		svgRoot.appendChild(ele);

		ele = svgDom.createElementNS(svgNS, "style");
		scriptTxt = convertStreamToString(SvgOperations.class.getResourceAsStream("/tooltip/tooltip.css"));
		ele.setTextContent(scriptTxt);
		svgRoot.appendChild(ele);

		// Create the rectangle.
		Element rectangle = svgDom.createElementNS(svgNS, "rect");
		rectangle.setAttributeNS(null, "id", "tooltip_bg");
		rectangle.setAttributeNS(null, "class", "tooltip_bg");
		rectangle.setAttributeNS(null, "x", "0");
		rectangle.setAttributeNS(null, "y", "0");
		rectangle.setAttributeNS(null, "rx", "4");
		rectangle.setAttributeNS(null, "ry", "8");
		rectangle.setAttributeNS(null, "width", "55");
		rectangle.setAttributeNS(null, "height", "40");
		rectangle.setAttributeNS(null, "visibility", "hidden");
		svgRoot.appendChild(rectangle);

		Element text = svgDom.createElementNS(svgNS, "text");
		text.setAttributeNS(null, "id", "tooltip");
		text.setAttributeNS(null, "class", "tooltip");
		text.setAttributeNS(null, "x", "0");
		text.setAttributeNS(null, "y", "0");
		text.setAttributeNS(null, "visibility", "hidden");

		Element tspan = svgDom.createElementNS(svgNS, "tspan");
		tspan.setAttributeNS(null, "id", "tooltipRegion");
		text.appendChild(tspan);

		tspan = svgDom.createElementNS(svgNS, "tspan");
		tspan.setAttributeNS(null, "id", "tooltipValue");
		text.appendChild(tspan);

		svgRoot.appendChild(text);
		return svgDom;
	}
	
	/**
	 *  Write an SVG memory object to a file
	 * @param doc
	 * @param path
	 * @return
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static File writeDomToFile(SVGDocument doc, String path) throws FileNotFoundException, UnsupportedEncodingException, IOException {
		return writeDomToFile(doc, path, false);
	}

	/**
	 *  Write an SVG memory object to a file
	 * @param doc
	 * @param path
	 * @return
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static File writeDomToFile(SVGDocument doc, String path, boolean deleteOnExit) throws FileNotFoundException, UnsupportedEncodingException, IOException {

		File f = new File(path);
		if (deleteOnExit) {
			f.deleteOnExit();
		}
		OutputStream os = new FileOutputStream(f);
		Writer outf = new OutputStreamWriter(os, "UTF-8");
		DOMUtilities.writeDocument(doc, outf);
		outf.flush();
		outf.close();

		return f;
	}

	/**
	 * Converts an SVG file to a jpg format file and returns the path
	 * @param pathOrigenSvg
	 * @return
	 * @throws Exception
	 */
	public static String convertSvg2Jpg(String pathOrigenSvg) throws Exception {
		return convertSvg2Jpg(pathOrigenSvg, false);
	}

	/**
	 * Converts an SVG file to a jpg format file and returns the path
	 * @param pathOrigenSvg
	 * @return
	 * @throws Exception
	 */
	public static String convertSvg2Jpg(String pathOrigenSvg, boolean deleteOnExit) throws Exception {

		// Create a JPEG transcoder
		JPEGTranscoder t = new JPEGTranscoder();

		// Set the transcoding hints.
		t.addTranscodingHint(JPEGTranscoder.KEY_QUALITY,
				new Float(.8));

		t.addTranscodingHint(JPEGTranscoder.KEY_XML_PARSER_VALIDATING,
						Boolean.FALSE);

		//Replace slash if needed
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			pathOrigenSvg = pathOrigenSvg.replace("/", "\\");
		}
		// Create the transcoder input.
		String svgURI = new File(pathOrigenSvg).toURL().toString();
		TranscoderInput input = new TranscoderInput(svgURI);

		// Create the transcoder output.
		String destFile = pathOrigenSvg.substring(0, new File(pathOrigenSvg).getPath().lastIndexOf("."));
		destFile += ".jpg";

		File jpgFile = new File(destFile);
		if (deleteOnExit) {
			jpgFile.deleteOnExit();
		}

		OutputStream ostream = new FileOutputStream(jpgFile);
		TranscoderOutput output = new TranscoderOutput(ostream);

		// Save the image.
		t.transcode(input, output);

		// Flush and close the stream.
		ostream.flush();
		ostream.close();

		return destFile;
	}

	public static String convertSvg2Png(String pathOrigenSvg) throws Exception {
		return convertSvg2Png(pathOrigenSvg, false);
	}

	/**
	 * Converts an SVG file to a png format file and returns the path
	 * @param pathOrigenSvg
	 * @return
	 * @throws Exception
	 */
	public static String convertSvg2Png(String pathOrigenSvg, boolean deleteOnExit) throws Exception {

		// Create a JPEG transcoder
		PNGTranscoder t = new PNGTranscoder();

		t.addTranscodingHint(JPEGTranscoder.KEY_XML_PARSER_VALIDATING,
						Boolean.FALSE);

		//Replace slash if needed
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			pathOrigenSvg = pathOrigenSvg.replace("/", "\\");
		}
		// Create the transcoder input.
		String svgURI = new File(pathOrigenSvg).toURL().toString();
		TranscoderInput input = new TranscoderInput(svgURI);

		// Create the transcoder output.
		String destFile = pathOrigenSvg.substring(0, new File(pathOrigenSvg).getPath().lastIndexOf("."));
		destFile += ".png";

		File pngFile = new File(destFile);
		if (deleteOnExit) {
			pngFile.deleteOnExit();
		}

		OutputStream ostream = new FileOutputStream(pngFile);
		TranscoderOutput output = new TranscoderOutput(ostream);

		// Save the image.
		t.transcode(input, output);

		// Flush and close the stream.
		ostream.flush();
		ostream.close();

		return destFile;
	}

    private static String convertStreamToString(InputStream is)
            throws IOException {

        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

	/**
	 * Print svg file as String
	 * @param svgDom
	 * @return
	 * @throws IOException
	 */
	public static String toString(SVGDocument svgDom) throws IOException {

		StringWriter stringWriter = new StringWriter();
		DOMUtilities.writeDocument(svgDom, stringWriter);
		String domString = stringWriter.toString();
		return domString;
	}

	public static String toString(String path) throws IOException {

		SVGDocument svgDom = SvgOperations.readSVG(path);

		StringWriter stringWriter = new StringWriter();
		DOMUtilities.writeDocument(svgDom, stringWriter);
		String domString = stringWriter.toString();
		return domString;
	}

    public static IColorScaleHtml createColorScale(Properties scaleProperties) {

        String scaleName = scaleProperties.getProperty("scale-type", "linear");
        if (scaleName.equalsIgnoreCase("linear")) {
            return createLinearColorScale(scaleProperties);
        }

        if (scaleName.equalsIgnoreCase("linear-two-sided")) {
            return createLinearTwoSidedColorScale(scaleProperties);
        }

        if (scaleName.equalsIgnoreCase("pvalue")) {
            return createPValueColorScale(scaleProperties);
        }

        return createLinearColorScale(scaleProperties);
    }

    private static IColorScaleHtml createPValueColorScale(Properties scaleProperties) {
        double significanceLevel = Double.valueOf(scaleProperties.getProperty("significance", "0.05"));
        Color minColor = Color.decode("#" + scaleProperties.getProperty("min-color", "FF0000"));
        Color maxColor = Color.decode("#" + scaleProperties.getProperty("max-color", "FFFF00"));
        Color nonSignificanceColor = Color.decode("#" + scaleProperties.getProperty("non-significant-color", "BBBBBB"));

        return new PValueColorScale(significanceLevel,
                minColor,
                maxColor,
                nonSignificanceColor);
    }

    private static IColorScaleHtml createLinearTwoSidedColorScale(Properties scaleProperties) {

        double minValue = Double.valueOf(scaleProperties.getProperty("min-value", "-2"));
        double midValue = Double.valueOf(scaleProperties.getProperty("mid-value", "0"));
        double maxValue = Double.valueOf(scaleProperties.getProperty("max-value", "2"));
        Color minColor = Color.decode("#" + scaleProperties.getProperty("min-color", "00FF00"));
        Color midColor = Color.decode("#" + scaleProperties.getProperty("mid-color", "000000"));
        Color maxColor = Color.decode("#" + scaleProperties.getProperty("max-color", "FF0000"));

        return new LinearTwoSidedColorScale(
                new ColorScalePoint(minValue, minColor),
                new ColorScalePoint(midValue, midColor),
                new ColorScalePoint(maxValue, maxColor)
        );
    }

    private static IColorScaleHtml createLinearColorScale(Properties scaleProperties) {
        double minValue = Double.valueOf(scaleProperties.getProperty("min-value", "0"));
        double maxValue = Double.valueOf(scaleProperties.getProperty("max-value", "1"));
        Color minColor = Color.decode("#" + scaleProperties.getProperty("min-color", "FFFFFF"));
        Color maxColor = Color.decode("#" + scaleProperties.getProperty("max-color", "000000"));

        return new LinearColorScale(
                new ColorScalePoint(minValue, minColor),
                new ColorScalePoint(maxValue, maxColor)
        );
    }


}
