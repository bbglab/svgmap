/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmap.cli;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * Main class for performing a simple SVGMap image generation
 *
 */
public class Main {

	public static void main(String args[]) {

		SVGMapParams params = parseParams(args);

		SVGMap g2d = new SVGMap(params);

		g2d.execute();

		System.exit(0);

	}

	private static SVGMapParams parseParams(String[] args) {
		SVGMapParams options = new SVGMapParams();
		CmdLineParser parser = new CmdLineParser(options);
		try {

			parser.parseArgument(args);

		} catch( CmdLineException e ) {
			System.err.println(e.getMessage());
			System.err.println("./svgmap.sh [options...]");
			parser.printUsage(System.err);
			System.err.println();
			System.exit(1);
		}
		return options;
	}

}
