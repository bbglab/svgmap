/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmap.cli;

import edu.upf.bg.colorscale.IColorScaleHtml;
import edu.upf.bg.progressmonitor.IProgressMonitor;
import edu.upf.bg.progressmonitor.NullProgressMonitor;
import edu.upf.bg.progressmonitor.StreamProgressMonitor;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.FileUtils;
import org.gitools.matrix.model.MatrixView;
import org.gitools.persistence.PersistenceUtils;
import org.gitools.utils.CSVStrategies;
import org.w3c.dom.svg.SVGDocument;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * This class performs a non-scaled SVGMap image generation.
 * The input parameters are a data file, an SVG control image, a keyword and a column index .
 * The SVGMap image generation consist on :
 * 1. Read a svg file (regions)
 * 2. Read a mapping file (regions-id-value)
 * 3. Write svg doc with marked regions of a gene given a mapping file + svg origin file
 * 4. Write svg doc to a file
 *
 */
public class SVGMap {

	private SVGMapParams params;

	public SVGMap(SVGMapParams params) {
		this.params = params;
	}

	public void execute() {

        IProgressMonitor progressMonitor = new StreamProgressMonitor(System.out, true, true);

		try {

            // Configure file names
            String svgFile;
            String pngFile;
            if (params.getToPNG() == null && !params.getOutputFileName().endsWith(".png")) {
                if (params.getOutputFileName().endsWith(".svg")) {
                    svgFile = params.getOutputFileName();
                } else {
                    svgFile = params.getOutputFileName() + ".svg";
                }
                pngFile = null;
            } else {
                if (params.getOutputFileName().endsWith(".png")) {
                    pngFile = params.getOutputFileName();
                } else {
                    pngFile = params.getOutputFileName() + ".png";
                }
                svgFile = pngFile.substring(0, pngFile.lastIndexOf('.')) + ".svg";
            }

            // Read color scale
            IColorScaleHtml colorScale;
            if (params.getScaleFileName()!=null) {
                Properties scaleProperties = new Properties();
                scaleProperties.load(new FileReader(params.getScaleFileName()));
                colorScale = SvgOperations.createColorScale(scaleProperties);
            } else {
                colorScale = SvgOperations.DEFAULT_SCALE;
            }

            // Read SVG file
            progressMonitor.begin("Reading SVG file", 10);
            SVGDocument doc = SvgOperations.readSVG(params.getPathSVGFile());
            progressMonitor.end();

            // Read data file
            progressMonitor.begin("Reading data file '" + params.getPathMappingFile() + "'", 10);
			MatrixView data = SvgOperations.readMappingFile(params.getPathMappingFile(), new NullProgressMonitor());
            progressMonitor.end();

            // Map SVG regions
            progressMonitor.begin("Mapping SVG regions", 10);
            int columns = params.getColumn() - 3;
            if (columns < 0 || columns >= data.getCellAttributes().size()) {
                String msg = "Invalid column number." + (data.getCellAttributes().size() > 1 ? " It must be between 3 and " + (data.getCellAttributes().size()+2) : " It must be 3.") + " Because the two first are identifiers.";
                progressMonitor.exception(new Exception(msg));
                System.exit(1);
            }

            Reader reader = PersistenceUtils.openReader(new File(params.getPathMappingFile()));
            CSVParser parser = new CSVParser(reader, CSVStrategies.TSV);
            List<String> header = Arrays.asList(parser.getLine());
			doc = SvgOperations.createSVGRegionsFromValue(doc, data, columns, params.getKeyValue(), colorScale, header);

            progressMonitor.end();

            // Write SVG file
            progressMonitor.begin("Writing SVG file to '" + params.getOutputFileName() + "'", 10);
            SvgOperations.writeDomToFile(doc, svgFile);
            progressMonitor.end();

            // write PNG file
            if (pngFile != null) {
                progressMonitor.begin("Converting to PNG format", 10);
				SvgOperations.convertSvg2Png(svgFile);
                FileUtils.forceDelete(new File(svgFile));
                progressMonitor.end();
            }

        } catch (Exception ex) {
			progressMonitor.exception(ex);
		}

        System.out.println();
	}
}
