/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmap.cli;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Class to read the input parameters of the main application class
 *
 */
public class SVGMapParams {

	@Option(name="-S", usage="path svg file", required = true)
	private String pathSVGFile;

	@Option(name="-D", usage="path data file", required = true)
	private String pathMappingFile;

	@Option(name="-K", usage="key to search", required = true)
	private String keyValue;

	@Option(name="-C", usage="index of the value column", required = true)
	private Integer column;

	@Option(name="-P", usage="generate a PNG file", required = false)
	private Boolean toPNG;

	@Option(name="-O", usage="path to output file", required = true)
	private String outputFileName;

    @Option(name="-CS", usage = "color scale config file", required = false)
    private String scaleFileName;

    // receives other command line parameters than options
    @Argument
    private List<String> arguments = new ArrayList();

	public List<String> getArguments() {
		return arguments;
	}

	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

	public String getPathMappingFile() {
		return pathMappingFile;
	}

	public void setPathMappingFile(String pathMappingFile) {
		this.pathMappingFile = pathMappingFile;
	}

	public String getPathSVGFile() {
		return pathSVGFile;
	}

	public void setPathSVGFile(String pathSVGFile) {
		this.pathSVGFile = pathSVGFile;
	}

    public Boolean getToPNG() {
        return toPNG;
    }

    public void setToPNG(Boolean toPNG) {
        this.toPNG = toPNG;
    }

    public String getScaleFileName() {
        return scaleFileName;
    }

    public void setScaleFileName(String scaleFileName) {
        this.scaleFileName = scaleFileName;
    }
}
