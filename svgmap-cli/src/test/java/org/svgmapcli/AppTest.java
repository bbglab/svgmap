/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapcli;

import org.junit.Test;
import org.svgmap.cli.Main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
    public void testApp() {
		ClassLoader classLoader = getClass().getClassLoader();
		String svgPath = classLoader.getResource("example/test.svg").getPath();
		String expPath = classLoader.getResource("example/experiment1.dat").getPath();
		File out = new File(svgPath);
		String outPath = out.getParent() + File.separator + "result.svg";

		String cmd = " -S \"" + svgPath + "\""
				+ " -D \"" + expPath + "\""
				+ " -K \"gene1\""
				+ " -C \"3\""
				+ " -O \"" + outPath + "\"";

		System.out.println(cmd);

		String[] args = cmdLineSplit(cmd);

		Main.main(args);
	}

	@SuppressWarnings("empty-statement")
	private static String[] cmdLineSplit(String cmd) {
		List<String> args = new ArrayList<String>();

		int lastPos = 0;
		int pos = 0;
		while (pos < cmd.length()) {
			char ch = cmd.charAt(pos);
			switch (ch) {
				case ' ':
					args.add(cmd.substring(lastPos, pos));
					while (pos < cmd.length() && cmd.charAt(pos++) != ch);
					lastPos = pos;
					break;
				case '"':
				case '\'':
					lastPos = ++pos;
					while (pos < cmd.length() && cmd.charAt(pos++) != ch);
					args.add(cmd.substring(lastPos, pos - 1));
					while (pos < cmd.length() && cmd.charAt(pos) == ' ') pos++;
					lastPos = pos;
					break;

				default: pos++;	break;
			}
		}

		if (lastPos < cmd.length())
			args.add(cmd.substring(lastPos, pos));

		String[] retArgs = new String[args.size()];
		args.toArray(retArgs);
		return retArgs;
	}
}

