
Usage examples:

1. With the default linear scale

./svgmap.sh -S examples/Drosophila_expression/housefly_anatomy_bnw.svg -D examples/Drosophila_expression/exprRatios.tsv -K "1616608_a_at" -C 3 -P -O test.png

2. If you want to config your own color scale, create a color scale file (see colorscale-example.config for examples, and uncomment an example before running the next command) and then run svgmap-cli like this:

./svgmap.sh -S examples/Drosophila_expression/housefly_anatomy_bnw.svg -D examples/Drosophila_expression/exprRatios.tsv -K "1616608_a_at" -C 3 -P -O test.png -CS colorscale-example.config

On Windows use "svgmap.bat" instead of "svgmap.sh"