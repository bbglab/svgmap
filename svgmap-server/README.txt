Execution of SVGMap

This project provides a Jetty Server to run SVGMap web, which needs Sun Java 1.6 or higher installed on your machine.

If your Operating System is a Linux or Mac OS X, then execute run.sh, in Windows use run.bat. We strongly recommend to run SVGMap from a terminal by the commands listed below!

When you run SVGMap a Webserver is created at the port 8095. Access to it by opening your favourite browser and direct yourself to the address: http://localhost:8095
Keep in mind that you may execute SVGMap only once per machine with the same port, otherwise it may create conflicts. The same applies if you already have another service running on that port. You can change the default port in the run.sh/run.bat on the following lines.

Linux/OS X: run.sh
java -server -Djetty.port=8095 -Dsvgmap.home=data -jar lib/start-6.1.22.jar

Windows: run.bat
java -server -Djetty.port=8095 -jar lib/start-6.1.22.jar



More information at https://bg.upf.edu/forge/projects/svgmap/wiki/User_documentation