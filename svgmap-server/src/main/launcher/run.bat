@echo off

set occupied=no

if not [%1]==[] (
    set port=%1%
) else (
    set port=8095
)


netstat -an | findstr :%port% | findstr LISTENING >nul&& set occupied=yes

if /i %occupied% == yes (
    echo PORT %port% is already in use, please change the port number or release it before to launch the SVGMAP web application
) else (
    java -Djetty.port=%port% -jar lib/start-6.1.22.jar
)

