#!/bin/bash

if [ -n "$1" ]; then
	PORT=$1
else
	PORT="8095"
fi

CHK_PORT=$(nc -z localhost $PORT)

if [ -n "$CHK_PORT" ]; then
    echo "PORT ($PORT) is already in use, please change the port number or release it before to launch the SVGMAP web application"
else
    PRG=`dirname "$0"`
    cd $PRG
    java -Djetty.port=$PORT -Dsvgmap.home=$PRG/data -jar lib/start-6.1.22.jar
fi
