====
    
    Copyright 2011 Universitat Pompeu Fabra.
    
    This software is open source and is licensed under the Open Software License
    version 3.0.
    
    You may obtain a copy of the License at
    http://www.opensource.org/licenses/osl-3.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    implied.
    
    See the License for the specific language governing permissions and
    limitations under the License.
====

This Example visualizes a elaborate meta-analysis of gene expression in different drosophila tissues. We mapped all the original data for the adult fly onto our image.

All the values between are a ratio of the tissue in comparison with the whole body expression mean. So all the values between 0 and 1 are less expressed than in the whole body, all values above 1 are more expressed than in the rest of the body.

This example is best viewed with the Linear Two-Sided scale. The mid-point is 1.

Data:
http://www.flyatlas.org

Original publication:
http://www.ncbi.nlm.nih.gov/pubmed/17534367

Image adapted from:
http://commons.wikimedia.org/wiki/File:Housefly_anatomy-key.svg
http://commons.wikimedia.org/wiki/File:Insect_anatomy_diagram.svg

URL for external linking of the ID in SVGMap:
http://www.google.com/search?q=drosophila+site:www.ebi.ac.uk+1631871_at