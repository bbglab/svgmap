====
    
    Copyright 2011 Universitat Pompeu Fabra.
    
    This software is open source and is licensed under the Open Software License
    version 3.0.
    
    You may obtain a copy of the License at
    http://www.opensource.org/licenses/osl-3.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    implied.
    
    See the License for the specific language governing permissions and
    limitations under the License.
====

This example visualizes the expression levels of yest cell cycle genes in each part of the cell cycle (M,G1,S,G2) for two full cell cycles. The data are taken from a "timecourse experiment of cell-cycle synchronized cells" by Rustici et al. called "elutration 1".

Choose between the values for Cell Cycle 1 (CC1) and Cell Cycle 2 (CC2) for visualization.

Data:
http://www.bahlerlab.info/projects/cellcycle/

Original publication:
http://www.ncbi.nlm.nih.gov/pubmed/15195092

Image adapted from:
http://commons.wikimedia.org/wiki/File:Cell_Cycle_2-2.svg

URL for external linking of the ID in SVGMap:
http://www.bahlerlab.info/cgi-bin/YOGY/yogy-search.pl?gene=%%&amp;species=S._pombe&amp;wild=No&amp;