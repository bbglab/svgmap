====
    
    Copyright 2011 Universitat Pompeu Fabra.
    
    This software is open source and is licensed under the Open Software License
    version 3.0.
    
    You may obtain a copy of the License at
    http://www.opensource.org/licenses/osl-3.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    implied.
    
    See the License for the specific language governing permissions and
    limitations under the License.
====

In this example we show transcriptomic data from IntOGen combination data. IntOGen provides integrated multidimensional OncoGenomics Data for the identification of genes and groups of genes We visualize which cell compartments are affected in a few chosen tissues. The Cell Compartments have been chosen from Gene Ontology CC Terms.

Data: 
http://www.intogen.org

Original Publication:
http://www.ncbi.nlm.nih.gov/pubmed/20111033

Image adapted from: 
http://commons.wikimedia.org/wiki/File:Biological_cell.svg
http://commons.wikimedia.org/wiki/File:Diagram_human_cell_nucleus.svg
