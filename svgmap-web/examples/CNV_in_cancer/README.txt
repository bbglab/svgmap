==== 
    
    Copyright 2011 Universitat Pompeu Fabra.
    
    This software is open source and is licensed under the Open Software License
    version 3.0.
    
    You may obtain a copy of the License at
    http://www.opensource.org/licenses/osl-3.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    implied.
    
    See the License for the specific language governing permissions and
    limitations under the License.
====

In this example we visualized the Copy Number Variation data from IntOGen combination data for ten chosen tissues. IntOGen combinations provides integrated multidimensional OncoGenomics Data for the identification of genes and groups of genes. 
The gain and loss p-values are mapped on human organs.

Data: IntOGen
http://www.intogen.org

Original Publication:
http://www.ncbi.nlm.nih.gov/pubmed/20111033

Image adapted from:
http://commons.wikimedia.org/wiki/File:Human_body_proportions2_svg.svg

URL for external linking of the ID in SVGMap:
http://www.ensembl.org/Homo_sapiens/Gene/Summary?g=%%

