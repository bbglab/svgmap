/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import org.svgmapweb.db.jpa.controlers.exceptions.NonexistentEntityException;
import javax.persistence.Query;
import org.svgmapweb.db.jpa.ExperimentFile;
import org.svgmapweb.ui.WicketApplication;
import org.apache.wicket.Application;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.ExperimentFileJpaController;

/**
 * Class to handle data base operations. 
 * 
 * ToDo: Is Expected to replace SVGMapDBOperations (migrating the functionality of SVGMapDBOperations).
 *
 * If more entities where introduced, this could be used more extensively.
 *
 * By now only for general query purposes.
 * 
 */
public class SVGMapDBOperationsHandle {

	private static final Logger logger = Logger.getLogger(SVGMapDBOperationsHandle.class);

	public static void cleanNotLinkedFiles() {

		EntityManager em = getEntityManager();
		try {

			cleanNotLinkedFiles(em);

		} catch (NonexistentEntityException ex) {

			java.util.logging.Logger.getLogger(SVGMapDBOperationsHandle.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} finally {
			em.close();
		}

		Logger.getLogger(SVGMapDBOperationsHandle.class.getName()).log(Level.DEBUG, "cleanNotLinkedFiles");

	}

	public static EntityManagerFactory getEntityManagerFactory() {
		return ((WicketApplication) Application.get()).getApplicationEntityManagerFactory();
	}

	public static EntityManager getEntityManager() {
		//return Application.get().getMetaData(WicketApplication.EMF).createEntityManager();
		return getEntityManagerFactory().createEntityManager();
	}

	public static void cleanNotLinkedFiles(EntityManager em) throws NonexistentEntityException {
		// Select this ExperimentFile's that don't appear in ExperimentDataType to be deleted
		Query q = em.createQuery(
				"SELECT distinct f FROM ExperimentFile f LEFT JOIN f.experimentdatatypeCollection e "
				+ " WHERE e.experimentdatatypePK.idfile IS NULL");

		List<ExperimentFile> experimentFileList = q.getResultList();
		Integer id = null;
		for (ExperimentFile exp : experimentFileList) {
			id = exp.getIdFile();
			ExperimentFileJpaController.destroy(exp.getIdFile());
			Logger.getLogger(SVGMapDBOperationsHandle.class.getName()).log(Level.DEBUG, "Deleted ExperimentFile with id : " + id.toString());
			logger.info("deleted");
		}
	}

	
	public static ExperimentFile getExperimentFileByType(Integer idData, String type) {

		EntityManager em = getEntityManager();
		try {

			// Select this ExperimentFile's that don't appear in ExperimentDataType to be deleted
			Query q = em.createQuery(
					" SELECT f FROM ExperimentDataType ef JOIN ef.experimentfile f "
					+ " WHERE ef.type = :type and ef.experimentdatatypePK.iddata  = :data  ORDER BY f.name ASC");

			q.setParameter("type", type);
			q.setParameter("data", idData);
			q.setMaxResults(1);

			List<ExperimentFile> res = q.getResultList();

			if (res != null && !res.isEmpty()) {
				return res.get(0);
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
		return null;
	}

	public static List<ExperimentFile> getAllFilesByType(String type) {

		EntityManager em = getEntityManager();
		try {

			// Select this ExperimentFile's that don't appear in ExperimentDataType to be deleted
			Query q = em.createQuery(
					" SELECT distinct f FROM ExperimentDataType ef JOIN ef.experimentfile f "
					+ " WHERE ef.type = :type  ORDER BY f.name ASC");

			q.setParameter("type", type);

			return q.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
		return null;
	}

	/**
	 * Experiment is consistent if this is true.
	 * Sometimes is usual have this un-consistency of databases.
	 */
	public static boolean consistentExperiment(ExperimentDataView newExperiment) {

		List<ExperimentFile> file = ExperimentFileJpaController.findExperimentsFiles(newExperiment.getIdData());
		
		return (file != null && !file.isEmpty()) ? true : false;
	}
}
