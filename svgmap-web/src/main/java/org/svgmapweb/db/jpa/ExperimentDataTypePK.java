/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class ExperimentDataTypePK implements Serializable {
	@Basic(optional = false)
    @Column(name = "idData", nullable = false)
	private int iddata;
	@Basic(optional = false)
    @Column(name = "idFile", nullable = false)
	private int idfile;

	public ExperimentDataTypePK() {
	}

	public ExperimentDataTypePK(int iddata, int idfile) {
		this.iddata = iddata;
		this.idfile = idfile;
	}

	public int getIdData() {
		return iddata;
	}

	public void setIdData(int iddata) {
		this.iddata = iddata;
	}

	public int getIdFile() {
		return idfile;
	}

	public void setIdFile(int idfile) {
		this.idfile = idfile;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (int) iddata;
		hash += (int) idfile;
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ExperimentDataTypePK)) {
			return false;
		}
		ExperimentDataTypePK other = (ExperimentDataTypePK) object;
		if (this.iddata != other.iddata) {
			return false;
		}
		if (this.idfile != other.idfile) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "org.svgmapweb.db.jpa.ExperimentDataTypePK[iddata=" + iddata + ", idfile=" + idfile + "]";
	}

}
