/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa.controlers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.apache.wicket.Application;
import org.svgmapweb.ui.WicketApplication;


public class ExperimentController {

	public static EntityManagerFactory getEntityManagerFactory() {
		return ((WicketApplication)Application.get()).getApplicationEntityManagerFactory();
	}

	public static EntityManager getEntityManager() {
		//return Application.get().getMetaData(WicketApplication.EMF).createEntityManager();
		return getEntityManagerFactory().createEntityManager();
	}

	
}
