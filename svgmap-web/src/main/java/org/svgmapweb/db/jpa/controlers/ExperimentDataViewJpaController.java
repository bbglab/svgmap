/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa.controlers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.exceptions.NonexistentEntityException;


public class ExperimentDataViewJpaController extends ExperimentController{
	
	public static ExperimentDataView create(ExperimentDataView experimentDataView) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			experimentDataView = create(experimentDataView, em);
			em.getTransaction().commit();
		} catch(Exception ex) {
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return experimentDataView;
	}

	public static ExperimentDataView create(ExperimentDataView experimentDataView, EntityManager em) {
			em.persist(experimentDataView);
			return experimentDataView;
	}


	public static void edit(ExperimentDataView experimentDataView) throws NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			experimentDataView = edit(experimentDataView, em);
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				Integer id = experimentDataView.getIdData();
				if (findExperimentDataView(id) == null) {
					throw new NonexistentEntityException("The experimentDataView with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static ExperimentDataView edit(ExperimentDataView experimentDataView, EntityManager em) {
		return em.merge(experimentDataView);
	}

	public static void destroy(Integer id) throws NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			ExperimentDataView experimentDataView;
			try {
				experimentDataView = em.getReference(ExperimentDataView.class, id);
				experimentDataView.getIdData();
			} catch (EntityNotFoundException enfe) {
				em.getTransaction().rollback();
				throw new NonexistentEntityException("The experimentDataView with id " + id + " no longer exists.", enfe);
			}
			em.remove(experimentDataView);
			em.getTransaction().commit();
		}catch(Exception e) {
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static List<ExperimentDataView> findExperimentDataViewEntities() {
		return findExperimentDataViewEntities(true, -1, -1);
	}

	public static List<ExperimentDataView> findExperimentDataViewEntities(int maxResults, int firstResult) {
		return findExperimentDataViewEntities(false, maxResults, firstResult);
	}

	private static List<ExperimentDataView> findExperimentDataViewEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery("select object(o) from ExperimentDataView as o");
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public static ExperimentDataView findExperimentDataView(Integer id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(ExperimentDataView.class, id);
		} finally {
			em.close();
		}
	}

	public static int getExperimentDataViewCount() {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery("select count(o) from ExperimentDataView as o");
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}

}
