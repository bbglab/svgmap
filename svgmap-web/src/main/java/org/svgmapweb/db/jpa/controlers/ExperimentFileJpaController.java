/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa.controlers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.ExperimentFile;
import org.svgmapweb.db.jpa.controlers.exceptions.NonexistentEntityException;

public class ExperimentFileJpaController extends ExperimentController {

	public static void create(ExperimentFile experimentFile) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(experimentFile);
			em.getTransaction().commit();
		} catch(Exception ex) {
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static void create(ExperimentFile experimentFile, EntityManager em) {
		em.persist(experimentFile);		
	}

	public static void edit(ExperimentFile experimentFile) throws NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			experimentFile = em.merge(experimentFile);
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				Integer id = experimentFile.getIdFile();
				if (findExperimentFile(id) == null) {
					throw new NonexistentEntityException("The experimentFile with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static void destroy(Integer id, EntityManager em) throws NonexistentEntityException {
		ExperimentFile experimentFile;
		try {
			experimentFile = em.getReference(ExperimentFile.class, id);
			experimentFile.getIdFile(); // JPA is lazy
		} catch (EntityNotFoundException enfe) {
			throw new NonexistentEntityException("The experimentFile with id " + id + " no longer exists.", enfe);
		}
		em.remove(experimentFile);
	}

	public static void destroy(Integer id) throws NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();

			destroy(id, em);

			em.getTransaction().commit();
		} catch(Exception ex) {
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static List<ExperimentFile> findExperimentFileEntities() {
		return findExperimentFileEntities(true, -1, -1);
	}

	public static List<ExperimentFile> findExperimentFileEntities(int maxResults, int firstResult) {
		return findExperimentFileEntities(false, maxResults, firstResult);
	}

	private static List<ExperimentFile> findExperimentFileEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery("select object(o) from ExperimentFile as o");
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public static ExperimentFile findExperimentFile(Integer id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(ExperimentFile.class, id);
		} finally {
			em.close();
		}
	}

	public static List<ExperimentFile> findExperimentFileFromExperimentDataType(ExperimentDataView dataParam) {

		assert dataParam.getIdData() != null : "ExperimentDataView object with id == null";

		List<ExperimentFile> res = null;
		EntityManager em = getEntityManager();
		try {

			Query q = em.createQuery(" SELECT f FROM ExperimentFile as f LEFT JOIN f.experimentdatatypeCollection ef  "
					+ " WHERE ef.experimentdatatypePK.iddata = :data "
					+ " ORDER BY ef.type DESC"); // Note that is important the order in some cases!

			q.setParameter("data", dataParam.getIdData());

			res = q.getResultList();

		} finally {
			em.close();
		}
		return res;
	}

	public static int getExperimentFileCount() {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery("select count(o) from ExperimentFile as o");
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}

	public static ExperimentFile findExperimentFileByType(Integer idData, String type, EntityManager em) {
		ExperimentFile res = null;
		
		Query q = em.createQuery(" SELECT f FROM ExperimentFile as f LEFT JOIN f.experimentdatatypeCollection ef "
				+ " WHERE ef.type = :type and ef.experimentdatatypePK.iddata  = :data "
				+ " ORDER BY f.name ASC");

		q.setParameter("type", type);
		q.setParameter("data", idData);

		q.setMaxResults(1);

		if (!q.getResultList().isEmpty()) {
			res = (ExperimentFile) (q.getResultList()).get(0);
		}

		return res;
	}

	public static List<ExperimentFile> findExperimentsFiles(Integer idData) {

		List<ExperimentFile> experimentFiles = null;
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery(" SELECT f FROM ExperimentFile as f LEFT JOIN f.experimentdatatypeCollection ef "
				+ " WHERE ef.experimentdatatypePK.iddata  = :data "
				+ " ORDER BY f.name ASC");

			q.setParameter("data", idData);

			experimentFiles = q.getResultList();
		} finally {
			em.close();
		}
		
		return experimentFiles;
	}


	public static List<ExperimentFile> findExperimentFileByTypeCollection(String type, EntityManager em) {

		Query q = em.createQuery(" SELECT f FROM ExperimentFile as f JOIN f.experimentdatatypeCollection ef "
				+ " WHERE ef.type = :type "
				+ " ORDER BY f.name ASC");

		q.setParameter("type", type);

		return q.getResultList();
	}

	public static List<ExperimentFile> findExperimentFileByTypeCollection(String type) {
		List<ExperimentFile> res = null;
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();

			res = findExperimentFileByTypeCollection(type, em);

			em.getTransaction().commit();
		} catch(Exception ex) {
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return res;
	}


	public static ExperimentFile getExperimentFileByType(Integer idData, String type) {
		ExperimentFile res = null;

		EntityManager em = getEntityManager();
		try {
			res = findExperimentFileByType(idData, type, em);
		} finally {
			em.close();
		}

		return res;
	}
}
