/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa.controlers;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import org.svgmapweb.db.jpa.ExperimentDataType;
import org.svgmapweb.db.jpa.ExperimentDataTypePK;
import org.svgmapweb.db.jpa.controlers.exceptions.NonexistentEntityException;
import org.svgmapweb.db.jpa.controlers.exceptions.PreexistingEntityException;


public class ExperimentDataTypeJpaController extends ExperimentController{
	
	public static void create(ExperimentDataType experimentDataType) throws PreexistingEntityException, Exception {
		
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			create(experimentDataType, em);
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			if (findExperimentDataType(experimentDataType.getExperimentDataTypePK()) != null) {
				throw new PreexistingEntityException("ExperimentDataType " + experimentDataType + " already exists.", ex);
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static void create(ExperimentDataType experimentDataType, EntityManager em) throws PreexistingEntityException, Exception {

		if (experimentDataType.getExperimentDataTypePK() == null) {
			experimentDataType.setExperimentDataTypePK(new ExperimentDataTypePK());
		}

		em.persist(experimentDataType);				
	}

	public static void edit(ExperimentDataType experimentDataType) throws NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			edit(experimentDataType, em);
			em.getTransaction().commit();
		} catch (Exception ex) {
			em.getTransaction().rollback();
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				ExperimentDataTypePK id = experimentDataType.getExperimentDataTypePK();
				if (findExperimentDataType(id) == null) {
					throw new NonexistentEntityException("The experimentDataType with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static ExperimentDataType edit(ExperimentDataType experimentDataType, EntityManager em) throws NonexistentEntityException, Exception {
		return experimentDataType = em.merge(experimentDataType);
	}

	public static void destroy(ExperimentDataTypePK id) throws NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			ExperimentDataType experimentDataType;
			try {
				experimentDataType = em.getReference(ExperimentDataType.class, id);
				experimentDataType.getExperimentDataTypePK();
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The experimentDataType with id " + id + " no longer exists.", enfe);
			}
			em.remove(experimentDataType);
			em.getTransaction().commit();
		} catch(Exception ex) {
			em.getTransaction().rollback();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public static void destroy(ExperimentDataTypePK id, EntityManager em) {
		ExperimentDataType experimentDataType = em.getReference(ExperimentDataType.class, id);
		experimentDataType.getExperimentDataTypePK();
		em.remove(experimentDataType);
	}

	public static List<ExperimentDataType> findExperimentDataTypeEntities() {
		return findExperimentDataTypeEntities(true, -1, -1);
	}

	public static List<ExperimentDataType> findExperimentDataTypeEntities(int maxResults, int firstResult) {
		return findExperimentDataTypeEntities(false, maxResults, firstResult);
	}

	private static List<ExperimentDataType> findExperimentDataTypeEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery("select object(o) from ExperimentDataType as o");
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public static ExperimentDataType findExperimentDataType(ExperimentDataTypePK id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(ExperimentDataType.class, id);
		} finally {
			em.close();
		}
	}

	public static int getExperimentDataTypeCount() {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createQuery("select count(o) from ExperimentDataType as o");
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}

}
