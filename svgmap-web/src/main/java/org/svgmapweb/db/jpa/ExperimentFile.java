/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
//import org.svgmapweb.db.jpa.ExperimentEntity;

@Entity
@Table(name = "ExperimentFile" )
@NamedQueries({
	@NamedQuery(name = "ExperimentFile.findAll", query = "SELECT e FROM ExperimentFile e"),
	@NamedQuery(name = "ExperimentFile.findByIdfile", query = "SELECT e FROM ExperimentFile e WHERE e.idfile = :idfile"),
	@NamedQuery(name = "ExperimentFile.findByName", query = "SELECT e FROM ExperimentFile e WHERE e.name = :name"),
	@NamedQuery(name = "ExperimentFile.findByPath", query = "SELECT e FROM ExperimentFile e WHERE e.path = :path")})
public class ExperimentFile implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "idFile", nullable = false)
	private Integer idfile;
	@Basic(optional = false)
	@Column(name = "name", nullable = false, length = 200)
	private String name;
	@Basic(optional = false)
	@Column(name = "path", nullable = false, length = 200)
	private String path;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "experimentfile", fetch = FetchType.LAZY)
	private Collection<ExperimentDataType> experimentdatatypeCollection;

	public ExperimentFile() {
	}

	public ExperimentFile(Integer idfile) {
		this.idfile = idfile;
	}

	public ExperimentFile(Integer idfile, String name, String path) {
		this.idfile = idfile;
		this.name = name;
		this.path = path;
	}

	public Integer getIdFile() {
		return idfile;
	}

	public void setIdFile(Integer idfile) {
		this.idfile = idfile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Collection<ExperimentDataType> getExperimentDataTypeCollection() {
		return experimentdatatypeCollection;
	}

	public void setExperimentDataTypeCollection(Collection<ExperimentDataType> experimentdatatypeCollection) {
		this.experimentdatatypeCollection = experimentdatatypeCollection;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idfile != null ? idfile.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ExperimentFile)) {
			return false;
		}
		ExperimentFile other = (ExperimentFile) object;
		if ((this.idfile == null && other.idfile != null) || (this.idfile != null && !this.idfile.equals(other.idfile))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "org.svgmapweb.db.jpa.ExperimentFile[idfile=" + idfile + "]";
	}
	
	public String getDisplay() {
		return "name";
	}

	public Integer getId() {
		return getIdFile();
	}

	public void setId(Integer id) {
		setIdFile(id);
	}

	
}
