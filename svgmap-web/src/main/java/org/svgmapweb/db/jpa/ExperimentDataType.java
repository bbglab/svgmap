/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ExperimentDataType")
@NamedQueries({
	@NamedQuery(name = "ExperimentDataType.findAll", query = "SELECT e FROM ExperimentDataType e"),
	@NamedQuery(name = "ExperimentDataType.findByIddata", query = "SELECT e FROM ExperimentDataType e WHERE e.experimentdatatypePK.iddata = :iddata"),
	@NamedQuery(name = "ExperimentDataType.findByIdfile", query = "SELECT e FROM ExperimentDataType e WHERE e.experimentdatatypePK.idfile = :idfile"),
	@NamedQuery(name = "ExperimentDataType.findByType", query = "SELECT e FROM ExperimentDataType e WHERE e.type = :type")})
public class ExperimentDataType implements Serializable {
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected ExperimentDataTypePK experimentdatatypePK;
	@Basic(optional = false)
    @Column(name = "type", nullable = false, length = 150)
	private String type;
	@JoinColumn(name = "idFile", referencedColumnName = "idFile", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
	private ExperimentFile experimentfile;
	@JoinColumn(name = "idData", referencedColumnName = "idData", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
	private ExperimentDataView experimentdataview;

	public ExperimentDataType() {
	}

	public ExperimentDataType(ExperimentDataTypePK experimentdatatypePK) {
		this.experimentdatatypePK = experimentdatatypePK;
	}

	public ExperimentDataType(ExperimentDataTypePK experimentdatatypePK, String type) {
		this.experimentdatatypePK = experimentdatatypePK;
		this.type = type;
	}

	public ExperimentDataType(int iddata, int idfile) {
		this.experimentdatatypePK = new ExperimentDataTypePK(iddata, idfile);
	}

	public ExperimentDataTypePK getExperimentDataTypePK() {
		return experimentdatatypePK;
	}

	public void setExperimentDataTypePK(ExperimentDataTypePK experimentdatatypePK) {
		this.experimentdatatypePK = experimentdatatypePK;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ExperimentFile getExperimentFile() {
		return experimentfile;
	}

	public void setExperimentFile(ExperimentFile experimentfile) {
		this.experimentfile = experimentfile;
	}

	public ExperimentDataView getExperimentDataView() {
		return experimentdataview;
	}

	public void setExperimentDataView(ExperimentDataView experimentdataview) {
		this.experimentdataview = experimentdataview;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (experimentdatatypePK != null ? experimentdatatypePK.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ExperimentDataType)) {
			return false;
		}
		ExperimentDataType other = (ExperimentDataType) object;
		if ((this.experimentdatatypePK == null && other.experimentdatatypePK != null) || (this.experimentdatatypePK != null && !this.experimentdatatypePK.equals(other.experimentdatatypePK))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "org.svgmapweb.db.jpa.ExperimentDataType[experimentdatatypePK=" + experimentdatatypePK + "]";
	}

}
