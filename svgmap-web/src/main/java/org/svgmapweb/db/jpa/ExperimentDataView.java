/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.db.jpa;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ExperimentDataView")
@NamedQueries({
	@NamedQuery(name = "ExperimentDataView.findAll", query = "SELECT e FROM ExperimentDataView e"),
	@NamedQuery(name = "ExperimentDataView.findByIddata", query = "SELECT e FROM ExperimentDataView e WHERE e.iddata = :iddata"),
	@NamedQuery(name = "ExperimentDataView.findByName", query = "SELECT e FROM ExperimentDataView e WHERE e.name = :name"),
	@NamedQuery(name = "ExperimentDataView.findByDescription", query = "SELECT e FROM ExperimentDataView e WHERE e.description = :description"),
	@NamedQuery(name = "ExperimentDataView.findBycreationDate", query = "SELECT e FROM ExperimentDataView e WHERE e.creationDate = :creationdate"),
	@NamedQuery(name = "ExperimentDataView.findByModificationdate", query = "SELECT e FROM ExperimentDataView e WHERE e.modificationDate = :modificationdate"),
	@NamedQuery(name = "ExperimentDataView.findByNumimages", query = "SELECT e FROM ExperimentDataView e WHERE e.numImages = :numimages"),
	@NamedQuery(name = "ExperimentDataView.findByPvaluefilter", query = "SELECT e FROM ExperimentDataView e WHERE e.pvalueFilter = :pvaluefilter"),
	@NamedQuery(name = "ExperimentDataView.findByImageoptions", query = "SELECT e FROM ExperimentDataView e WHERE e.imageOptions = :imageoptions"),
	@NamedQuery(name = "ExperimentDataView.findByPvalueoptions", query = "SELECT e FROM ExperimentDataView e WHERE e.pvalueOptions = :pvalueoptions"),
	@NamedQuery(name = "ExperimentDataView.findByScaleoptions", query = "SELECT e FROM ExperimentDataView e WHERE e.scaleOptions = :scaleoptions"),
	@NamedQuery(name = "ExperimentDataView.findByGenelink", query = "SELECT e FROM ExperimentDataView e WHERE e.geneLink = :genelink"),
	@NamedQuery(name = "ExperimentDataView.findByDefaulttabselected", query = "SELECT e FROM ExperimentDataView e WHERE e.defaultTabSelected = :defaulttabselected"),
	@NamedQuery(name = "ExperimentDataView.findByLinearscaledefaults", query = "SELECT e FROM ExperimentDataView e WHERE e.linearScaleDefaults = :linearscaledefaults")})
public class ExperimentDataView implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "IdData", nullable = false)
	private Integer iddata;
	@Basic(optional = false)
	@Column(name = "name", nullable = false, length = 100)
	private String name;
	@Column(name = "description", length = 400)
	private String description;
	@Column(name = "creationDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	@Column(name = "modificationDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;
	@Column(name = "numImages")
	private Integer numImages = 1; // Default value
	@Column(name = "pvalueFilter")
	private Integer pvalueFilter = 0; // Default value
	@Column(name = "imageOptions", length = 4000)
	private String imageOptions;
	@Column(name = "pvalueOptions", length = 4000)
	private String pvalueOptions = "";
	@Column(name = "scaleOptions", length = 4000)
	private String scaleOptions = "";
	@Column(name = "geneLink", length = 400)
	private String geneLink;
	@Column(name = "defaultTabSelected", length = 400)
	private String defaultTabSelected;
	@Column(name = "linearScaleDefaults", length = 400)
	private String linearScaleDefaults = "outlier"; // Default value
	@Column(name = "relevantKeys", length = 400)
	private String relevantKeys;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "experimentdataview", fetch = FetchType.LAZY)
	private Collection<ExperimentDataType> experimentdatatypeCollection;

	public ExperimentDataView() {
	}

	public ExperimentDataView(Integer iddata) {
		this.iddata = iddata;
	}

	public ExperimentDataView(Integer iddata, String name) {
		this.iddata = iddata;
		this.name = name;
	}

	public Integer getIdData() {
		return iddata;
	}

	public void setIdData(Integer iddata) {
		this.iddata = iddata;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationdate) {
		this.creationDate = creationdate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Integer getNumImages() {
		return numImages;
	}

	public void setNumImages(Integer numimages) {
		this.numImages = numimages;
	}

	public Integer getPvalueFilter() {
		return pvalueFilter;
	}

	public void setPvalueFilter(Integer pvaluefilter) {
		this.pvalueFilter = pvaluefilter;
	}

	public String getImageOptions() {
		return imageOptions;
	}

	public void setImageOptions(String imageoptions) {
		this.imageOptions = imageoptions;
	}

	public String getPvalueOptions() {
		return pvalueOptions;
	}

	public void setPvalueOptions(String pvalueoptions) {
		this.pvalueOptions = pvalueoptions;
	}

	public String getScaleOptions() {
		return scaleOptions;
	}

	public void setScaleOptions(String scaleoptions) {
		this.scaleOptions = scaleoptions;
	}

	public String getGeneLink() {
		return geneLink;
	}

	public void setGeneLink(String genelink) {
		this.geneLink = genelink;
	}

	public String getDefaultTabSelected() {
		return defaultTabSelected;
	}

	public void setDefaultTabSelected(String defaulttabselected) {
		this.defaultTabSelected = defaulttabselected;
	}

	public String getLinearScaleDefaults() {
		return linearScaleDefaults;
	}

	public void setLinearScaleDefaults(String linearscaledefaults) {
		this.linearScaleDefaults = linearscaledefaults;
	}

	public Collection<ExperimentDataType> getExperimentDataTypeCollection() {
		return experimentdatatypeCollection;
	}

	public void setExperimentDataTypeCollection(Collection<ExperimentDataType> experimentdatatypeCollection) {
		this.experimentdatatypeCollection = experimentdatatypeCollection;
	}

	public String getRelevantKeys() {
		return relevantKeys;
	}

	public void setRelevantKeys(String relevantKeys) {
		this.relevantKeys = relevantKeys;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (iddata != null ? iddata.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof ExperimentDataView)) {
			return false;
		}
		ExperimentDataView other = (ExperimentDataView) object;
		if ((this.iddata == null && other.iddata != null) || (this.iddata != null && !this.iddata.equals(other.iddata))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "org.svgmapweb.db.jpa.ExperimentDataView[iddata=" + iddata + "]";
	}

	public String[] getKeys() {
		return new String[]{"idData"};
	}

	public Integer getId() {
		return getIdData();
	}

	public void setId(Integer id) {
		setIdData(id);
	}

	/*
	public String getDisplay() {
		return "name";
	}*/
}
