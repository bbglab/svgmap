/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui;

import org.apache.wicket.Application;
import org.apache.wicket.markup.html.basic.Label;
import org.svgmapweb.model.ProjectManager;

public class AboutPage extends SvgmapwebPage {
	
    public AboutPage() {

		ProjectManager projectManager = ((WicketApplication) Application.get()).getProjectManager();

		add(new Label("about", projectManager.getProject().getAbout()).setEscapeModelStrings(false));

    }
}
