/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui;

import org.svgmapweb.ui.browser.BrowserPanel;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.extensions.markup.html.tabs.TabbedPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.browser.image.BrowserSVGPanel;
import org.svgmapweb.ui.browser.table.BrowserTblPanel;

public class BrowsePage extends SvgmapwebPage {

	/*By default constructor. Used after click on header */
	public BrowsePage() {

		setDefaultModel(new Model<String>("tabpanel"));

		IModel<SvgSearch> searchModel = new Model(new SvgSearch());

		add(new BrowserPanel("browse", searchModel));
		
		List<ITab> tabs = new ArrayList<ITab>();

        TabbedPanel tpanel = new TabbedPanel("tabs", tabs);
		
		tpanel.setVisible(false);

		add(tpanel);
	}

	public BrowsePage(final IModel<SvgSearch> searchModel) {

		try {

			SvgSearch search = searchModel.getObject();

			setOutputMarkupId(true);

			setDefaultModel(new Model<String>("tabpanel"));

			add(new BrowserPanel("browse", searchModel));

			List<ITab> tabs = createTabsPanel(search);

			TabbedPanel tpanel = new TabbedPanel("tabs", tabs);
			tpanel.setSelectedTab(getTabSelectedIndex(search));

			add(tpanel);

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new WicketRuntimeException();
		}
	}
	
	private List<ITab> createTabsPanel(final SvgSearch search) {
        List<ITab> tabs = new ArrayList<ITab>();
	
        tabs.add(new AbstractTab(new Model<String>("Image")) {
            @Override
            public Panel getPanel(String panelId) {
				try {
					
					Panel tb = new BrowserSVGPanel(panelId, new Model(search));

					search.setSelectedTab(SvgmapConstants.TAB_IMG);

					return tb;
					
				} catch (Exception ex) {
					throw new WicketRuntimeException(ex);
				}
            }
        });
		
        tabs.add(new AbstractTab(new Model<String>("Table")) {
            @Override
            public Panel getPanel(String panelId) {
				try {
					Panel tb = new BrowserTblPanel(panelId,  new Model(search));
					search.setSelectedTab(SvgmapConstants.TAB_TABLE);
					return tb;

				} catch (Exception ex) {
					ex.printStackTrace();
					throw new WicketRuntimeException(ex);
				}
            }
        });
		return tabs;
	}

	private int getTabSelectedIndex(SvgSearch search) {
		if (search.getSelectedTab() == null) {
			if (search.getExperiment().getDefaultTabSelected() == null)
				throw new WicketRuntimeException("Experiment with wrong or non-defined configuration. Contact administrator to fix this issue.");
			else
				if (search.getExperiment().getDefaultTabSelected().equals(SvgmapConstants.IMAGE))
					return SvgmapConstants.TAB_IMG;
				else
					return SvgmapConstants.TAB_TABLE;

		} else
			return search.getSelectedTab();
	}
}