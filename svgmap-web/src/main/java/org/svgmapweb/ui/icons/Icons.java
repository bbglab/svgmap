/**
 *  Copyright 2011 Universitat Pompeu Fabra.
 *
 *  This software is open source and is licensed under the Open Software License
 *  version 3.0. You may obtain a copy of the License at
 *
 *      http://www.opensource.org/licenses/osl-3.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.svgmapweb.ui.icons;

import org.apache.wicket.ResourceReference;


public final class Icons {
	
	private Icons() {
		super();
	}
	
	/**
     *  Cross icon.
     */
    public static final ResourceReference CROSS = new ResourceReference(Icons.class, "cross.png");
        
    /**
     * Question mark icon.
     */
    public static final ResourceReference HELP = new ResourceReference(Icons.class, "b_help.png");
    
    /**
     * Thin delete icon.
     */
    public static final ResourceReference DELETE = new ResourceReference(Icons.class, "drop.png");
    

}
