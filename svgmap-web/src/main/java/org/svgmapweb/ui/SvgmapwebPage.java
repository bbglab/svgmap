/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.wicket.Application;
import org.svgmapweb.ui.layout.Bottom;
import org.svgmapweb.ui.layout.Header;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.protocol.http.request.WebClientInfo;
import org.svgmapweb.model.ProjectManager;
import org.svgmapweb.ui.layout.Menu;



public class SvgmapwebPage extends WebPage {

	static final int MIN_VERSION_SAFARI = 5;
	static final int MIN_VERSION_FIREFOX = 4;
	static final int MIN_VERSION_CHROME = 10;
	static final int MIN_VERSION_OPERA = 9;
	static final int MIN_VERSION_MSIE = 9;


	public SvgmapwebPage () {

		// Add page title
		add(new Label("title", new Model<String>() {
			public String getObject() {
				return SvgmapwebPage.this.getTitle();
			}
		}));

		// Add headers and menus
		add(new Header("header", getClass()));
		add(new Menu("menu", this.getClass()));
		add(new Bottom("bottom"));
		checkSVGCompatibility();
	}

	private void checkSVGCompatibility() throws NumberFormatException {

		SvgmapSession session = (SvgmapSession) WebSession.get();
		if (session.isSvgSupported() == null) {
			session.setSvgSupported(false);
			// Does browser support svg?
			WebClientInfo w = (WebClientInfo) session.getClientInfo();
			ClientProperties cp = w.getProperties();
			String userAgent = w.getUserAgent();
			
			//Mozilla Firefox 4.0 or higher required
			if (cp.isBrowserMozillaFirefox()) {
				Pattern p = Pattern.compile("Firefox/(\\d+)");
				Integer version = extractVersion(p,userAgent);
				if (version >= MIN_VERSION_FIREFOX) {
					session.setSvgSupported(true);
				}

			} else if (cp.isBrowserSafari()) {
				//check if it is chrome
				Pattern p = Pattern.compile("Chrome/(\\d+)");
				Integer version = extractVersion(p,userAgent);
				if (version >= MIN_VERSION_CHROME) {
					session.setSvgSupported(true);
				} else {
					//it is SAFARI
					p = Pattern.compile("Version/(\\d+)");
					version = extractVersion(p,userAgent);
					if (version >= MIN_VERSION_SAFARI) {
						session.setSvgSupported(true);
					}
				}

			} else if (cp.isBrowserOpera()) {
				Pattern p = Pattern.compile("Opera/(\\d+)");
				Integer version = extractVersion(p,userAgent);
				if (version >= MIN_VERSION_OPERA) {
					session.setSvgSupported(true);
				}
			} else if (cp.isBrowserInternetExplorer()) {
				Pattern p = Pattern.compile("MSIE (\\d+)");
				Integer version = extractVersion(p,userAgent);
				if (version >= MIN_VERSION_MSIE) {
					session.setSvgSupported(true);
				}
			} 
		}
	}

	private Integer extractVersion(Pattern p, String userAgent) throws NumberFormatException {
		Matcher versionMatcher = p.matcher(userAgent);
		Integer version = -1;
		if (versionMatcher.find()) {
			version = Integer.parseInt(versionMatcher.group(1));
		}
		return version;
	}

	public String getTitle() {
		ProjectManager projectManager = ((WicketApplication) Application.get()).getProjectManager();

		if ( projectManager.getProject() == null ||  projectManager.getProject().getTitle().isEmpty())
			return "svgmap";
		else
			 return projectManager.getProject().getTitle();
	}




}
