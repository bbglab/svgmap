/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui;
import org.apache.wicket.Application;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.model.ProjectManager;
import org.svgmapweb.ui.admin.ExperimentConfigPanel;
import org.svgmapweb.ui.admin.LoginPanel;
import org.svgmapweb.ui.admin.ExperimentsPanel;
import org.svgmapweb.ui.admin.ExperimentEditPanel;
import org.svgmapweb.ui.layout.InfoAdminPanel;

// FIXME : Q&D
public class AdminPage extends SvgmapwebPage {

	public AdminPage() {
		try {
			SvgmapSession session = (SvgmapSession) WebSession.get();
			ProjectManager projectManager = ((WicketApplication) Application.get()).getProjectManager();

			//No login panel if user authentified or not provided in project xml
			if (session.isUserValidated() ||
					(projectManager.getProject().getLogin() == null || projectManager.getProject().getLogin().isEmpty()
					&& projectManager.getProject().getPwd() == null || projectManager.getProject().getPwd().isEmpty()))
				completePanel(ExperimentsPanel.class, null);
			else {
				add(new InfoAdminPanel("userInfo", ""));
				add(new LoginPanel("main"));
			}
		} catch (Exception ex) {
			add(new LoginPanel("main"));
			error("Unable load page");
		}
	}

	public AdminPage(Class panelClass) {
		try {
			ProjectManager projManager = ((WicketApplication) Application.get()).getProjectManager();
			add(new InfoAdminPanel("userInfo", "user: " + projManager.getProject().getLogin()));
			
			if (panelClass.equals(ExperimentsPanel.class))
				add(new ExperimentsPanel("main"));

		} catch (Exception ex) {
			add(new LoginPanel("main"));
			error("Unable load page");
		}
	}

	public AdminPage(Class panelClass, Object obj) {
		try {

			completePanel(panelClass, obj);
			
		} catch (Exception ex) {
			add(new LoginPanel("main"));
			error("Unable load page");
		}
	}

	private void completePanel(Class panelClass, Object obj) throws Exception {
		ProjectManager projManager = ((WicketApplication) Application.get()).getProjectManager();

		add(new InfoAdminPanel("userInfo", "user: " + projManager.getProject().getLogin()));

		if (panelClass.equals(ExperimentsPanel.class) && obj == null)
			add(new ExperimentsPanel("main"));

		if (panelClass.equals(ExperimentConfigPanel.class)) {
			add(new ExperimentConfigPanel("main", (ExperimentDataView) obj));
		}
		if (panelClass.equals(ExperimentEditPanel.class)) {
			add(new ExperimentEditPanel("main", (ExperimentDataView) obj));
		}
	}
}
