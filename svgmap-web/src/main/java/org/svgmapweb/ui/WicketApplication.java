/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui;

import cern.colt.matrix.ObjectMatrix1D;
import cern.colt.matrix.ObjectMatrix2D;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.wicket.*;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.file.Folder;
import org.apache.wicket.util.file.WebApplicationPath;
import org.gitools.matrix.model.MatrixView;
import org.gitools.persistence.PersistenceException;
import org.svgmap.cli.SvgOperations;
import org.svgmapweb.db.SVGMapDBOperationsHandle;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.ExperimentFile;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.model.ProjectManager;
import org.svgmapweb.model.scales.ColorScaleRangeExt;
import org.svgmapweb.model.utils.FileUtils;
import org.svgmapweb.model.utils.RangeUtils;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.utils.SvgmapUtils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 *
 */
public class WicketApplication extends WebApplication {


	public static final String SVGMAP_PU = "svgmap_principal_pu";
	private static final String PROJ_FILE = "project.xml";
	private EntityManagerFactory app_emf = null;
	private Folder uploadFolder = null;
	private Folder processFolder = null;
	private ProjectManager projectManager = null;

	private HashMap<String, HashMap<Integer, ExperimentFile>> experimentFiles = new HashMap<String, HashMap<Integer, ExperimentFile>>();
	private Map<Integer, MatrixView> mappingsMatrix = new HashMap<Integer, MatrixView>();
	private Map<Integer, ObjectMatrix2D> annotationsMatrix = new HashMap<Integer, ObjectMatrix2D>();
	private Map<Integer, HashMap<String, ColorScaleRangeExt>> mappingsMatrixRanges = new HashMap<Integer, HashMap<String, ColorScaleRangeExt>>();
	private Map<Integer, HashMap<String, String>> experimentsKeyNameAnnotationsMap = new HashMap<Integer, HashMap<String, String>>();
	private Map<String, String> controlFiles = new HashMap<String, String>();

	/**
	 * Constructor
	 */
	public WicketApplication() {
	}

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	public Class<HomePage> getHomePage() {
		return HomePage.class;
	}

	@Override
	protected void init() {
		super.init();

		/*mount pages*/
		mountBookmarkablePage("admin", AdminPage.class);
		mountBookmarkablePage("browse", BrowsePage.class);
		mountBookmarkablePage("about", AboutPage.class);

		getResourceSettings().setThrowExceptionOnMissingResource(false);

		/*create or load experiments file folders*/

		// check if DATA directory is in web.xml properties
		String pathBase = this.getInitParameter("svgmap.home");

		// check if DATA directory is in user parameters
		if (pathBase == null || pathBase.isEmpty()) {
			pathBase = System.getProperty("svgmap.home");
		}

		// check if a DATA directory exists in the current execution directory
		if (pathBase == null || pathBase.isEmpty()) {
			String curDir = System.getProperty("user.dir") + File.separator + "data";
			File file = new File(curDir);
			if (file.exists() && file.isDirectory()) {
				pathBase = curDir;
			}
		}

		// The last option is to create a hidden folder .svgmap on the user home
		if (pathBase == null || pathBase.isEmpty()) {
			pathBase = System.getProperty("user.home") + File.separator + ".svgmap";
			FileUtils.createLoadFolder(pathBase);
		}

		/* Load project.xml*/
		File projectsFile = null;
		try {
			projectsFile = new File(pathBase + File.separator + PROJ_FILE);

			if (!projectsFile.exists())
				projectsFile = new File(WebApplication.get().getServletContext().getResource(PROJ_FILE).toURI());

			if (projectManager == null)
				projectManager = new ProjectManager(projectsFile);

		} catch (Exception ex) {
			throw new WicketRuntimeException("Error while loading configuration file", ex);
		}

		/* Database */

		// Check if an external database exists by project.xml
		String dbUrl = projectManager.getProject().getUrlDB();
		String dbUser = projectManager.getProject().getUserDB();
		String dbPassword = projectManager.getProject().getPwdDB();

		// Check if an external database exists by parameters
		if (dbUrl == null || dbUrl.isEmpty()) {
			dbUrl = System.getProperty("dbUrl");
			dbUser = System.getProperty("dbUser");
			dbPassword = System.getProperty("dbPassword");
		} else

		System.out.println("System params: " + dbUrl + dbUser + dbPassword);
		
		boolean createTables = false;

		// Create local HSQL db if no db url provided
		if (dbUrl == null || dbUrl.isEmpty()) {
			String hsqlDB = pathBase + File.separator + "svgmap.hsqldb";

			File file = new File(hsqlDB + ".properties");
			if (!file.exists()) {
				createTables = true;
			}

			dbUrl = "jdbc:hsqldb:file:" + hsqlDB;
			dbUser = "SA";
			dbPassword = "";

			try {
				Class.forName("org.hsqldb.jdbcDriver");
			} catch (Exception e) {
				System.out.println("ERROR: failed to load HSQLDB JDBC driver.");
				e.printStackTrace();
				return;
			}
		}

		// Only in case connection to embedded db (HSQL)
		if (createTables) {
			createDefaultTables( createDataSource(dbUrl, dbUser, dbPassword)/*connection.newDataSource()*/);
		}

		initGlobalEntityManagerFactory(dbUrl, dbUser, dbPassword);

		/* Load DATA folders */		
		if (projectManager.getProject().getDataFolder() != null) {
			pathBase = pathBase + File.separator + projectManager.getProject().getDataFolder();
		}

		uploadFolder = FileUtils.createLoadFolder(pathBase + File.separator + "svgmap-uploads");
		processFolder = FileUtils.createLoadFolder(pathBase + File.separator + "svgmap-process");

		WebApplicationPath resourceFinder = (WebApplicationPath) getResourceSettings().getResourceFinder();
		resourceFinder.add(uploadFolder.getPath());
		resourceFinder.add(processFolder.getPath());
		resourceFinder.add(projectsFile.getPath());

		getDebugSettings().setAjaxDebugModeEnabled(false);
		getResourceSettings().setResourceFinder(resourceFinder);

		/*Load experiments*/
		loadMatrixExperiments();

	}

	private void createDefaultTables(DataSource ds) {

		try {
			Connection conn = ds.getConnection();
			Statement st = conn.createStatement();

			// Definition of table `ExperimentDataType`
			st.execute("CREATE TABLE ExperimentDataType ( idData int NOT NULL, idFile int NOT NULL, type varchar(150) NOT NULL, PRIMARY KEY (idData, idFile) );");

			// Definition of table `ExperimentDataView`
			st.execute("CREATE TABLE ExperimentDataView ( idData INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY, name varchar(100) NOT NULL, description varchar(4000) DEFAULT NULL, creationDate datetime DEFAULT NULL, modificationDate datetime DEFAULT NULL, numImages INTEGER DEFAULT 1, pvalueFilter int DEFAULT 0,  imageOptions varchar(4000) DEFAULT NULL,  pvalueOptions varchar(4000) DEFAULT '',  scaleOptions varchar(4000) DEFAULT '', geneLink varchar(4000) DEFAULT NULL, defaultTabSelected varchar(400) DEFAULT 'image', linearScaleDefaults varchar(400) DEFAULT 'outlier', relevantKeys varchar(4000) DEFAULT NULL);"); //, PRIMARY KEY out (idData) in HSQL 2.1.0

			// Definition of table `ExperimentFile`
			st.execute("CREATE TABLE ExperimentFile ( idFile INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY, name varchar(200) NOT NULL,  path varchar(200) NOT NULL);"); //PRIMARY KEY (idFile) out in HSQLDB

		} catch (SQLException e) {
			throw new WicketRuntimeException("Creating tables", e);
		}


	}

	/**
	 * Instantiate new data-source.
	 *
	 * @param dbUrl
	 * @param dbUser
	 * @param dbPassword
	 * @return
	 */
	private DataSource createDataSource(String dbUrl, String dbUser, String dbPassword) {

		DriverManagerConnectionFactory connectionFactory = new DriverManagerConnectionFactory(dbUrl, dbUser, dbPassword);
		GenericObjectPool connectionPool = new GenericObjectPool(null);
		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);

		return new PoolingDataSource(connectionPool);
	}

	private void loadMatrixExperiments() {

		List<ExperimentFile> listExp = SVGMapDBOperationsHandle.getAllFilesByType(SvgmapConstants.TYPE_MAPPING);
		if (listExp != null) {
			for (ExperimentFile f : listExp) {
				mappingsMatrix.put(f.getIdFile(), null);
			}
		}

		List<ExperimentFile> listAnn = SVGMapDBOperationsHandle.getAllFilesByType(SvgmapConstants.TYPE_ANNOTATION);
		if (listAnn != null) {
			for (ExperimentFile f : listAnn) {
				annotationsMatrix.put(f.getIdFile(), null);
			}
		}
	}

	public ExperimentFile getExperimentFile(Integer idData, String type) {
		ExperimentFile exp = null;

		HashMap<Integer, ExperimentFile> hashExp = experimentFiles.get(type);

		if (hashExp != null) {
			exp = experimentFiles.get(type).get(idData);
		}

		if (exp == null) {
			exp = SVGMapDBOperationsHandle.getExperimentFileByType(idData, type);

			hashExp = new HashMap<Integer, ExperimentFile>();
			hashExp.put(idData, exp);
			experimentFiles.put(type, hashExp);
		}

		return exp;
	}

	public MatrixView getMappingMatrix(Integer idData) throws Exception {
		ExperimentFile mappingFile = getExperimentFile(idData, SvgmapConstants.TYPE_MAPPING);

		if (mappingFile == null)
			return null;

		MatrixView mappingMatrix = mappingsMatrix.get(mappingFile.getIdFile());

		/* Load mapping file into matrix and set ranges*/
		if (mappingMatrix == null) {
			String basePath = FileUtils.getUploadFolder() + File.separator;
			mappingMatrix = SvgOperations.readMappingFile(basePath + mappingFile.getPath());
			mappingsMatrix.put(mappingFile.getIdFile(), mappingMatrix);

			setMappingMatrixRanges(idData);
		}

		return mappingMatrix;
	}

	public ObjectMatrix2D getAnnotationMatrix(Integer idData) throws IOException, PersistenceException {
		ExperimentFile annotationFile = getExperimentFile(idData, SvgmapConstants.TYPE_ANNOTATION);

		if (annotationFile == null)
			return null;

		ObjectMatrix2D annotationMatrix  = annotationsMatrix.get(annotationFile.getIdFile());

		/* Load mapping file into matrix */
		if (annotationMatrix == null) {
			String basePath = FileUtils.getUploadFolder() + File.separator;
			annotationMatrix = SvgmapUtils.parseTextFile2MatrixHeader(new File(basePath + annotationFile.getPath()), true);
			annotationsMatrix.put(annotationFile.getIdFile(), annotationMatrix);
		}

		return annotationMatrix;
	}

	public void setMappingMatrixRanges(Integer idData) throws Exception {

		ExperimentFile mappingFile = getExperimentFile(idData, SvgmapConstants.TYPE_MAPPING);

		if (mappingFile == null)
			throw new WicketRuntimeException("Unnable to load experiment mapping file");

		MatrixView mappingMatrix = mappingsMatrix.get(mappingFile.getIdFile());

		/* Load mapping file into matrix */
		if (mappingMatrix == null) {
			String basePath = FileUtils.getUploadFolder() + File.separator;
			mappingMatrix = SvgOperations.readMappingFile(basePath + mappingFile.getPath());
			mappingsMatrix.put(mappingFile.getIdFile(), mappingMatrix);
		}

		HashMap<String, ColorScaleRangeExt> rangeColumnsData = new HashMap<String, ColorScaleRangeExt>();

		ExperimentDataView data = ExperimentDataViewJpaController.findExperimentDataView(idData);
		List<String> dataColumnsRange = Arrays.asList(data.getImageOptions().split(";"));

		ColorScaleRangeExt rangeColumns = null;
		rangeColumns = RangeUtils.computeRangeWithoutOutliers(mappingMatrix, dataColumnsRange);
		rangeColumnsData.put(SvgmapConstants.SCALE_RANGES_WITHOUT_OUTLIER, rangeColumns);

		rangeColumns = RangeUtils.computeRangeColumns(mappingMatrix, dataColumnsRange);
		rangeColumnsData.put(SvgmapConstants.SCALE_RANGES_ALL_DATA, rangeColumns);
		mappingsMatrixRanges.put(idData, rangeColumnsData);
	}

	public HashMap<String, ColorScaleRangeExt> getMappingsMatrixRanges(Integer idData) throws Exception {
		if (mappingsMatrixRanges.get(idData) == null)
			setMappingMatrixRanges(idData);

		return mappingsMatrixRanges.get(idData);
	}
	
	public HashMap<String, String> getKeyNameAnnotationsMap(Integer idData) throws IOException, PersistenceException {

		if (experimentsKeyNameAnnotationsMap.get(idData) != null)
			return experimentsKeyNameAnnotationsMap.get(idData);

		ObjectMatrix2D annotationMatrix = getAnnotationMatrix(idData);
		HashMap<String, String> expData = null;

		if (annotationMatrix != null) {
			expData = new HashMap<String, String>();
			ObjectMatrix1D colIndex = annotationMatrix.viewColumn(0);
			for (int j = 0; j < colIndex.toArray().length; j++)
				expData.put((String) annotationMatrix.viewColumn(0).get(j), (String) annotationMatrix.viewColumn(1).get(j));

			experimentsKeyNameAnnotationsMap.put(idData, expData);
		}

		return expData;
	}

	public String getControlImage(Integer idData) throws Exception {
		String path = null;

		String basePath = FileUtils.getUploadFolder() + File.separator;

		String svg = ((WicketApplication) Application.get()).getExperimentFile(idData, SvgmapConstants.TYPE_SVG).getPath();

		path = controlFiles.get(svg);

		if (path == null) {
			path = SvgOperations.convertSvg2Png(basePath + svg);
			path = path.replace(basePath, "");
			controlFiles.put(svg, path);
		}

		return path;
	}

	public void resetExperimentCache(ExperimentDataView data) {
		String[] types = new String[]{SvgmapConstants.TYPE_MAPPING, SvgmapConstants.TYPE_SVG, SvgmapConstants.TYPE_ANNOTATION};
		ExperimentFile expFile = null;

		for (String type : types) {
			HashMap<Integer, ExperimentFile> hashExp = experimentFiles.get(type);

			if (hashExp != null) {
				for (Integer idData : hashExp.keySet()) {
					if (idData == data.getIdData()) {
						expFile = experimentFiles.get(type).get(idData);

						if (expFile != null) {
							if (type.equals(SvgmapConstants.TYPE_MAPPING)) {
								resetMappingsMatrix(expFile);
							}
							if (type.equals(SvgmapConstants.TYPE_ANNOTATION)) {
								resetAnnotationsMatrix(expFile);
								resetKeyNameAnnotationsMap(idData);
							}
							if (type.equals(SvgmapConstants.TYPE_SVG)) {
								resetControlFiles(expFile);
							}
						}
					}
				}
				experimentFiles.get(type).put(data.getIdData(), null);
			}
		}
	}

	public void resetMappingsMatrix(ExperimentFile exp) {
		this.mappingsMatrix.put(exp.getIdFile(), null);
	}

	public void resetAnnotationsMatrix(ExperimentFile exp) {
		this.annotationsMatrix.put(exp.getIdFile(), null);
	}

	public void resetControlFiles(ExperimentFile exp) {
		this.controlFiles.put(exp.getPath(), null);
	}

	private void resetKeyNameAnnotationsMap(Integer idData) {
		this.experimentsKeyNameAnnotationsMap.put(idData, null);
	}

	public EntityManagerFactory getApplicationEntityManagerFactory() {
		return app_emf;
	}

	public void setApplicationEntityManagerFactory(EntityManagerFactory app_emf) {
		this.app_emf = app_emf;
	}

	private void initGlobalEntityManagerFactory(String dbUrl, String dbUser, String dbPassword) {

		assert dbUrl != null && dbUser != null && dbPassword != null : "Database connection must be specified (dbUrl + dbUser + dbPassword).";

		Properties prop = new Properties();
		prop.put("hibernate.connection.url", dbUrl);
		prop.put("hibernate.connection.driver_class", getProperURLDriverClassName(dbUrl));
		prop.put("hibernate.connection.username", dbUser);
		prop.put("hibernate.connection.password", dbPassword);
		setApplicationEntityManagerFactory(Persistence.createEntityManagerFactory(SVGMAP_PU, prop));
	}

	private String getProperURLDriverClassName(String dbUrl) {

		if (dbUrl.indexOf("jdbc:mysql") > -1) {

			return "com.mysql.jdbc.Driver";

		} else if (dbUrl.indexOf("jdbc:hsqldb") > -1) {

			return "org.hsqldb.jdbcDriver";

		} else {
			// By now only we support HSQLDB and MySQL drivers
			throw new UnsupportedOperationException("Drive not found by given uri : " + dbUrl);
		}
	}
	
	@Override
	public Session newSession(Request request, Response response) {
		return new SvgmapSession(request);
	}

	public Folder getUploadFolder() {
		return uploadFolder;
	}

	public Folder getProcessFolder() {
		return processFolder;
	}

	public ProjectManager getProjectManager() {
		return projectManager;
	}

}
