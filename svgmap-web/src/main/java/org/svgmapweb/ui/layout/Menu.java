/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.layout;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.svgmapweb.ui.AboutPage;
import org.svgmapweb.ui.BrowsePage;
import org.svgmapweb.ui.HomePage;
import org.svgmapweb.ui.SvgmapwebPage;

public class Menu extends Panel {

	private static final long serialVersionUID = -297484477734645307L;

    public Menu(String id, Class<? extends SvgmapwebPage> currentPage) {

		super(id);

		addLink("home", HomePage.class, currentPage);
		addLink("browse", BrowsePage.class, currentPage);
		addLink("about", AboutPage.class, currentPage);

	}

	private void addLink(String componentId, Class<? extends SvgmapwebPage> linkPage, Class<? extends SvgmapwebPage> currentPage) {
		WebMarkupContainer item = new WebMarkupContainer(componentId);
		item.add(new BookmarkablePageLink<SvgmapwebPage>("link", linkPage));
		if (linkPage.equals(currentPage)) {
			item.add(new AttributeModifier("class", true, new Model<String>("current")));
		}
		add(item);
	}	
}
