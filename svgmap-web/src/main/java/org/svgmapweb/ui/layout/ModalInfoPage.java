/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.svgmapweb.ui.layout;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;


public class ModalInfoPage extends WebPage
{

    public ModalInfoPage(final ModalWindow window, final String title, final String txt)
    {
		try {						
			window.setTitle(title);
			window.setInitialHeight(200);
			window.setInitialWidth(150);

			Label txtField = new Label("txt", txt);
			txtField.setEscapeModelStrings(false);
			txtField.setVisible(true);
			add(txtField);

		} catch (Exception ex) {
			throw new WicketRuntimeException(ex);
		}

	}
}
