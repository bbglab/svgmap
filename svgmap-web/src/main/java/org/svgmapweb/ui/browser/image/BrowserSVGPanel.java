/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.image;

import edu.upf.bg.colorscale.ColorScalePoint;
import edu.upf.bg.colorscale.impl.LinearColorScale;
import edu.upf.bg.colorscale.impl.LinearTwoSidedColorScale;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteSettings;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.string.Strings;
import org.gitools.matrix.model.MatrixView;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.model.wrappers.ColumnWrapper;
import org.svgmapweb.model.wrappers.RepeatingViewerModel;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.filters.FilterFactory;
import org.svgmapweb.model.scales.ScalePanelFactory;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.scales.ColorScaleRangeExt;
import org.svgmapweb.model.utils.RangeUtils;
import org.svgmapweb.ui.BrowsePage;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.model.wrappers.ScaleWrapper;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.browser.image.scale.Linear2SidedScalePanel;
import org.svgmapweb.ui.browser.image.scale.LinearScalePanel;
import org.svgmapweb.ui.browser.image.scale.ModalScalePanel;

public class BrowserSVGPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private AutoCompleteTextField<String> keyWordTextField;
	private ExperimentDataView currentExperiment;
	private DropDownChoice<ScaleWrapper> optionScale;
	private RepeatingView imgContainer;
	private List<String> KeyNamesExperiment;
	private Label lblValueScale;
	private IModel<String> textToSearchModel;
	private List<String> dataColumnsRange;
	private Panel resultSVG;

	private SvgSearch search;

	public BrowserSVGPanel(String id, final IModel<SvgSearch> searchModel) {
		super(id);

		setOutputMarkupId(true);

		try {
			/* Init local variables */
			search = searchModel.getObject();
			currentExperiment = search.getExperiment();

			List<ColumnWrapper> imgColumnOptionsSelected = search.getImgColumnOptionsSelected();
			ScaleWrapper scaleSelected = search.getScale();

			add(new FeedbackPanel("feedback"));

			/* Init form */
			final Form form = new Form("form");
			add(form);

			form.add(new Label("msgScale", "Scale"));
			form.add(new Label("msgValescale", " "));

			form.add(new MultiLineLabel("relevantKeys", createRelevantExampleText().toString())
					.setEscapeModelStrings(false));

			form.add(new Link("tblLink") {
				@Override
				public void onClick() {
					search.setSelectedTab(SvgmapConstants.TAB_TABLE);
					setResponsePage(new BrowsePage(new Model(search)));
				}
			});

			form.add(new Label("msgKey", SvgmapUtils.createTextForExperimentSearch(currentExperiment.getIdData(), SvgmapConstants.TAB_IMG).toString()));

			/*Load full key + names */
			KeyNamesExperiment = SvgmapUtils.loadKeyNamesExperiment(currentExperiment.getIdData());

			/*Get columns data for key ranges*/
			dataColumnsRange = Arrays.asList(currentExperiment.getImageOptions().split(";"));

			/* Add image columns */
			RepeatingViewerModel rvm = new RepeatingViewerModel();
			List<IModel<ColumnWrapper>> listOptions = new ArrayList<IModel<ColumnWrapper>>(0);
			ChoiceRenderer choiceRenderer = new ChoiceRenderer("columnName", "index");

			imgContainer = new RepeatingView("imgColumns", rvm);
			IModel<ColumnWrapper> modelImageData = null;
			DropDownChoice<ColumnWrapper> optionColumn;

			List<ColumnWrapper> imageOptions = createColumns(currentExperiment.getImageOptions(), "image", true);
			for (int i = 0; i < currentExperiment.getNumImages(); i++) {
				if (imgColumnOptionsSelected == null || imgColumnOptionsSelected.isEmpty()) {
					if (imageOptions.size() > i)
						modelImageData = new Model<ColumnWrapper>(imageOptions.get(i));
					else
						modelImageData = new Model<ColumnWrapper>(null);
				}
				else {
					if (i < imgColumnOptionsSelected.size())
						modelImageData = new Model<ColumnWrapper>(imgColumnOptionsSelected.get(i));
					else
						modelImageData = new Model<ColumnWrapper>(null);
				}

				listOptions.add(modelImageData);
				optionColumn = new DropDownChoice<ColumnWrapper>("column", modelImageData, imageOptions, choiceRenderer);

				WebMarkupContainer container = new WebMarkupContainer(imgContainer.newChildId());
				container.add(new Label("msgColumn", "Data for image " + (i + 1)));
				container.add(optionColumn);
				imgContainer.add(container);
			}
			rvm.setObject(listOptions);
			form.add(imgContainer);

			/*Add Scales*/
			final List<ScaleWrapper> scales = createScales();
			if (scales != null && !scales.isEmpty() && scaleSelected == null)
				scaleSelected = scales.get(0);

			final IModel<ScaleWrapper> scaleValueModel = new Model<ScaleWrapper>(scaleSelected);
			optionScale = new DropDownChoice<ScaleWrapper>("scale", scaleValueModel, scales) {
				@Override public boolean isNullValid() {
					return false;
				}
			};

			optionScale.setRequired(true);
			optionScale.setOutputMarkupId(true);
			optionScale.add(new OnChangeAjaxBehavior() {
				@Override protected void onUpdate(AjaxRequestTarget target) {
					target.addComponent(BrowserSVGPanel.this);
				}
			});

			lblValueScale = new Label("scaleValues", "");
			if (scaleSelected != null) {
				lblValueScale = new Label("scaleValues", new PropertyModel(scaleValueModel, "panel.scaleValues"));
				lblValueScale.setOutputMarkupId(true);
			}

			form.add(optionScale);
			form.add(lblValueScale);

			// Add modal window
			final ModalWindow modal1 = new ModalWindow("modal1") ;
			modal1.setPageMapName("modal-1");
			modal1.setCookieName("modal-1");

			modal1.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
				public void onClose(AjaxRequestTarget target) {
					if (lblValueScale != null) {
						recomputeScaleRangeFromKeyWord();
						target.addComponent(lblValueScale);
					}
				}
			});
			form.add(modal1);

			/* Add modal button */
			AjaxLink modalBtn = new AjaxLink("showModal1") {
				@Override public void onClick(AjaxRequestTarget target) {
					if (optionScale.getModelObject() != null) {
						modal1.setContent(new ModalScalePanel(modal1.getContentId(), modal1, optionScale.getModel()));
						modal1.show(target);
					}
				}
			};
			form.add(modalBtn);

			//Add Gene field
			AutoCompleteSettings settings = new AutoCompleteSettings();
			settings.setCssClassName("scroll");

			//Add searchText
			List<String> keyList = SvgmapUtils.parseKeywordsToList(search.getSearchText());
			if (keyList != null && keyList.size() > 1)
				search.setSearchText("");
			textToSearchModel = new PropertyModel(search, "searchText");

			keyWordTextField = new AutoCompleteTextField<String>("keyword", textToSearchModel, settings) {
				@Override protected Iterator<String> getChoices(String input) {

					if (Strings.isEmpty(input))
						return KeyNamesExperiment.iterator();

					List<String> choices = new ArrayList<String>(10);
					input = input.toLowerCase();
					for (String column : KeyNamesExperiment)
						if (column.toLowerCase().startsWith(input))
							choices.add(column);

					return choices.iterator();
				}
			};
			keyWordTextField.setRequired(true);
			keyWordTextField.setOutputMarkupId(true);
			keyWordTextField.add(new AjaxFormComponentUpdatingBehavior("onblur") {
				@Override
				protected void onUpdate(AjaxRequestTarget target) {
					recomputeScaleRangeFromKeyWord();
					target.addComponent(lblValueScale);
				}
			});
			form.add(keyWordTextField);
			recomputeScaleRangeFromKeyWord();

			/* search button*/
			Button searchBttn = new Button("searchBtn") {
				@Override
				public void onSubmit() {
					try {
						updateSearchObject();
						setResponsePage(new BrowsePage(new Model(search)));

					} catch (Exception e) {
						e.printStackTrace();
						error("Error: Non properly image generated. Desciption:" + e.getMessage());
					}
				}
			};
			form.add(searchBttn);
			Button resetBttn = new Button("resetBtn") {
				@Override
				public void onSubmit() {
					try {
						// Update model before to show results
						resetSearchObject();
						setResponsePage(new BrowsePage(new Model(search)));

					} catch (Exception e) {
						e.printStackTrace();
						error("Error: Non properly image generation process. Desciption:" + e.getMessage());
					}
				}
			};
			form.add(resetBttn);

			resultSVG = new EmptyPanel("resultSVG");
			if (search.getExperiment().getName() != null) {

				// Update model before to show results
				updateSearchObject();
				resultSVG = new ResultSVGPanel("resultSVG", new Model(search));
			}

			resultSVG.setOutputMarkupPlaceholderTag(true);
			resultSVG.setOutputMarkupId(true);
			add(resultSVG);

		} catch (Exception e) {
			e.printStackTrace();
			error("Error: Non terminated browsing process. Desciption:" + e.getMessage());
		}
	}

	private void updateSearchObject() {
		List<ColumnWrapper> imgColumnOptionsSelected = getColumnValuesImage();
		if (imgColumnOptionsSelected.size() < 1) {
			warn("At least one value must be selected in the image data columns");
		}

		search.setImgColumnOptionsSelected(imgColumnOptionsSelected);
		search.setScale(optionScale.getModelObject());
		search.setSearchText(keyWordTextField.getModelObject());
	}

	private void resetSearchObject() {
		search = new SvgSearch();
		search.setExperiment(currentExperiment);
		search.setSelectedTab(SvgmapConstants.TAB_IMG);

	}

	private StringBuilder createRelevantExampleText() {
		StringBuilder relevantExamplesTxt = new StringBuilder();

		if (currentExperiment.getRelevantKeys() != null && !currentExperiment.getRelevantKeys().isEmpty()) {
			relevantExamplesTxt.append(currentExperiment.getRelevantKeys());
			relevantExamplesTxt.append("\n");
		}

		return relevantExamplesTxt;
	}

	private List<ScaleWrapper> createScales() throws Exception {
		List<String> scaleTypes = new ArrayList();

		if (currentExperiment.getScaleOptions() != null) {
			if (currentExperiment.getScaleOptions().contains(";")) {
				scaleTypes = Arrays.asList(currentExperiment.getScaleOptions().split(";"));
			} else {
				scaleTypes.add(currentExperiment.getScaleOptions());
			}
		}

		int i = 0;
		List<ScaleWrapper> scalesLst = new ArrayList<ScaleWrapper>();
		for (String type : scaleTypes) {
			ScaleWrapper scale = new ScaleWrapper();
			scale.setIndex(i);
			scale.setPanel(ScalePanelFactory.createScalePanel(type, currentExperiment));
			scalesLst.add(scale);
			i++;
		}
		return scalesLst;
	}

	private List<ColumnWrapper> createColumns(String options, String type, boolean defaultOption) {
		List<ColumnWrapper> cols = new ArrayList<ColumnWrapper>();
		ColumnWrapper col = null;

		if (options == null)
			return cols;
		else {
			int i = 0;
			for (String pval : Arrays.asList(options.split(";"))) {
				col = new ColumnWrapper();
				col.setIndex(i);
				col.setColumnName(pval);

				cols.add(col);
				i++;
			}
		}
		if (defaultOption) {
			col = new ColumnWrapper();
			col.setColumnName("-- No " + type + "--");
			col.setIndex(-1);
			cols.add(col);
		}
		return cols;
	}

	private ArrayList<ColumnWrapper> getColumnValuesImage() {
		ArrayList<ColumnWrapper> columnValues = new ArrayList<ColumnWrapper>(0);
		for (IModel<ColumnWrapper> c : (List<IModel<ColumnWrapper>>) imgContainer.getDefaultModelObject()) {
			if (c.getObject() != null && c.getObject().getIndex() >= 0)
				columnValues.add(c.getObject());
		}
		return columnValues;
	}

	private void recomputeScaleRangeFromKeyWord() throws WicketRuntimeException {
		ScaleWrapper scaleSelected = optionScale.getModelObject();
		try {
			if (scaleSelected != null && scaleSelected.getPanel() != null) {
				if (scaleSelected.getPanel() instanceof LinearScalePanel) {
						String scaleValuesType = ((LinearScalePanel) scaleSelected.getPanel()).getDefaultValueType();

						if (scaleValuesType.equals(SvgmapConstants.SCALE_RANGES_KEY) && keyWordTextField.getModelObject() != null) {
							List<String> keyList = SvgmapUtils.convertKeywordsToKeys(currentExperiment.getIdData(), keyWordTextField.getModelObject());

							if (keyList != null && !keyList.isEmpty()) {
								MatrixView mappingMatrix = ((WicketApplication) Application.get()).getMappingMatrix(currentExperiment.getIdData());
								ColorScaleRangeExt range = RangeUtils.computeRangeColumnsKey(mappingMatrix, keyList.get(0), dataColumnsRange);
								LinearColorScale scaleOld = (LinearColorScale) scaleSelected.getPanel().getScale();
								LinearColorScale scale = new LinearColorScale(
										new ColorScalePoint(range.getMin(), scaleOld.getMin().getColor()),
										new ColorScalePoint(range.getMax(), scaleOld.getMax().getColor()));

								scaleSelected.getPanel().resetScale(scale);
							}
						}
					}
					if (scaleSelected.getPanel() instanceof Linear2SidedScalePanel) {
						String scaleValuesType = ((Linear2SidedScalePanel) scaleSelected.getPanel()).getDefaultValueType();

						if (scaleValuesType.equals(SvgmapConstants.SCALE_RANGES_KEY) && keyWordTextField.getModelObject() != null) {
							List<String> keyList = SvgmapUtils.convertKeywordsToKeys(currentExperiment.getIdData(), keyWordTextField.getModelObject());

							if (keyList != null && !keyList.isEmpty()) {
								MatrixView mappingMatrix = ((WicketApplication) Application.get()).getMappingMatrix(currentExperiment.getIdData());
								ColorScaleRangeExt range = RangeUtils.computeRangeColumnsKey(mappingMatrix, keyList.get(0), dataColumnsRange);
								LinearTwoSidedColorScale scaleOld = (LinearTwoSidedColorScale) scaleSelected.getPanel().getScale();

								LinearTwoSidedColorScale scale = new LinearTwoSidedColorScale(
								new ColorScalePoint(range.getMin(), scaleOld.getMin().getColor()),
								scaleOld.getMid(),
								new ColorScalePoint(range.getMax(), scaleOld.getMax().getColor())
								);
								scaleSelected.getPanel().resetScale(scale);
							}
						}
				}
			}

		} catch (Exception ex) {
			throw new WicketRuntimeException(ex);
		}
	}

}
