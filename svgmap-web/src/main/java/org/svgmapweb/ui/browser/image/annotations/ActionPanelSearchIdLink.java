/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.image.annotations;

import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.svgmapweb.model.utils.SvgmapConstants;

public class ActionPanelSearchIdLink extends Panel {

	public ActionPanelSearchIdLink(String id, String key, String link) {
		super(id);

		add(new ExternalLink("link", link.replace(SvgmapConstants.CHR_LNK, key), key));

	}
}
