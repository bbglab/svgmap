/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.table;

import java.io.IOException;
import org.apache.wicket.Application;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.svgmapweb.model.ProjectManager;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.ui.BrowsePage;
import org.svgmapweb.model.utils.FileUtils;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.WicketApplication;

public class BrowserTblPanel extends Panel {

    private static final long serialVersionUID = 1L;

	private TextArea nameGene;
	private FileUploadField fileUpload;

	private SvgSearch search;

    public BrowserTblPanel(String id, final IModel<SvgSearch> searchModel) {

		super(id);

		try {
			search = searchModel.getObject();
			search.setSelectedTab(SvgmapConstants.TAB_TABLE);

			//Add modal window
			final ModalWindow modal  = new ModalWindow("modal1");
			modal.setPageMapName("modal-1");
			modal.setCookieName("modal-1");
			modal.setInitialHeight(200);
			ProjectManager projectManager = ((WicketApplication) Application.get()).getProjectManager();
			Label txtField = new Label(modal.getContentId(), projectManager.getProject().getHelp());
			txtField.setEscapeModelStrings(false);
			txtField.setVisible(true);
			modal.setTitle("Help");
			modal.setContent(txtField);
			add(modal);

			// Add form
			Form form = new Form("form");
			Label lbl = new Label("msgKey", SvgmapUtils.createTextForExperimentSearch(search.getExperiment().getIdData(), SvgmapConstants.TAB_TABLE).toString());
			form.add(lbl);
			
			AjaxLink lnk = new AjaxLink("link"){
				@Override
				public void onClick(AjaxRequestTarget target) {
					modal.show(target);
				}
			};
			form.add(lnk);
			
			form.add(new Label("file", "Upload file"));

			nameGene = new TextArea("gene", new PropertyModel(search, "searchText"));
			form.add(nameGene);

			//Genes FILE
			form.add(fileUpload = new FileUploadField("geneFile"));

			Button searchBttn = new Button("searchBtn") {
				@Override
				public void onSubmit() {
					try {
						//updateModel();
						setResponsePage(new BrowsePage(new Model(search)));

					} catch (Exception e) {
						e.printStackTrace();
							error("Error: Non properly table generation process. Desciption:" + e.getMessage());
					}
				}
			};
			form.add(searchBttn);

			Button resetBttn = new Button("resetBtn") {
				@Override
				public void onSubmit() {
					try {

						search.setSearchText(null);
						setResponsePage(new BrowsePage(new Model(search)));

					} catch (Exception e) {
						e.printStackTrace();
						error("Error: Non properly table generation process. Desciption:" + e.getMessage());
					}
				}
			};
			form.add(resetBttn);
			add(form);
			
			/* Update model before to show results*/
			updateSearchObject();
			
			add(new ResultTblPanel("tableResults", new Model(search)));
			
		} catch (Exception e) {
			e.printStackTrace();
			error("Error: Non terminated browsing process. Desciption:" + e.getMessage());
		}
    }
	
	private void updateSearchObject() throws IOException {
		FileUpload upload = fileUpload.getFileUpload();

		if (upload == null)
			search.setKeyword(nameGene.getValue());
		else 
			search.setKeyword(FileUtils.convertStreamToString(upload.getInputStream()));
	}
}
