/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.table;

import org.svgmapweb.ui.browser.toolbar.ToolbarPanel;
import cern.colt.matrix.ObjectMatrix2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.wicket.Application;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.gitools.matrix.model.MatrixView;
import org.gitools.persistence.PersistenceException;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.model.filters.FilterByKey;
import org.svgmapweb.model.filters.FilterFactory;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.filters.FilterByUser;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.ui.browser.image.annotations.ActionPanelSearchIdLink;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.browser.image.annotations.ActionPanelSearchIdModal;
import org.svgmapweb.model.filters.Filter;

public class ResultTblPanel extends Panel {

	private SvgSearch search;
	private ExperimentDataView experiment = null;

	private List<IColumn<?>> columnsTbl = new ArrayList<IColumn<?>>();
	private List<String> hdrMapp;
	private List<String> hdrAnn;
	private DefaultDataTable tblResults;

	private IModel<List<RegionDetails>> dataModel = new Model();

	public ResultTblPanel(String id, final IModel<SvgSearch> searchModel) throws Exception {
		super(id);

		try {
			//Init local variables
			search = searchModel.getObject();
			experiment = search.getExperiment();
			hdrMapp = SvgmapUtils.readHeader(experiment.getIdData(), SvgmapConstants.TYPE_MAPPING);
			hdrAnn = SvgmapUtils.readHeader(experiment.getIdData(), SvgmapConstants.TYPE_ANNOTATION);

			// Convert searched keywords(names or keys) into keys
			List<String> keyList = SvgmapUtils.convertKeywordsToKeys(experiment.getIdData(), search.getSearchText());

			/* Apply keywords filters */
			final MatrixView mappingMatrix = ((WicketApplication) Application.get()).getMappingMatrix(experiment.getIdData());
			FilterByKey f = (FilterByKey) FilterFactory.createFilterByKey(experiment.getIdData(), mappingMatrix);
			Map<String, List<RegionDetails>> mapRows = f.filter(keyList);

			/*Apply user-defined filters*/
			List<Filter> userFilters = search.getExperimentFilters();
			if (userFilters != null && !userFilters.isEmpty()) {
				final ObjectMatrix2D annotationMatrix = ((WicketApplication) Application.get()).getAnnotationMatrix(experiment.getIdData());
				FilterByUser fUser = (FilterByUser) FilterFactory.createUserFilter(annotationMatrix);
				for (Filter flt : userFilters) {
					mapRows = fUser.filter(mapRows, flt, hdrMapp, hdrAnn);
				}
			}

			//Create data for the table
			List<RegionDetails> lDetails = SvgmapUtils.mergeLists(mapRows.values());
			dataModel.setObject(lDetails);

			/* Make columns */
			makeColumnsTbl(search);

			//Add Table panel
			tblResults = new DefaultDataTable("tableSummary", columnsTbl, new SortableSummaryDataProvider(dataModel), 20);
			add(tblResults);

			//Add toolbar panel
			add(new ToolbarPanel("toolbar", searchModel, dataModel));
		}
		catch (Exception ex) {
			ex.printStackTrace();
			error("Error while showing results");
		}
	}

	private void makeColumnsTbl(final SvgSearch search) throws IOException, PersistenceException {

		//Add key
		if (experiment.getGeneLink() == null || experiment.getGeneLink().isEmpty()) {
			columnsTbl.add(new PropertyColumn(new Model<String>(hdrMapp.get(1)), "id", "id"));
		} else {
			columnsTbl.add(new AbstractColumn<RegionDetails>(new Model<String>(hdrMapp.get(1))) {

				public void populateItem(Item<ICellPopulator<RegionDetails>> cellItem, String type, IModel<RegionDetails> model) {
					cellItem.add(new ActionPanelSearchIdLink(type, model.getObject().getId(), experiment.getGeneLink()));
				}
			});
		}
		//Add name if possible
		if (((WicketApplication) Application.get()).getAnnotationMatrix(experiment.getIdData()) != null)
			columnsTbl.add(new PropertyColumn(new Model<String>(hdrAnn.get(1)), "name", "name"));

		//Add region
		columnsTbl.add(new PropertyColumn(new Model<String>(hdrMapp.get(0)), "region", "region"));

		//Add mapping value columns
		for (int i = 2; i < hdrMapp.size(); i++) {
				String col = hdrMapp.get(i);
				columnsTbl.add(new PropertyColumn(new Model<String>(col), "values[" + (i-2) + "]", "values[" + (i-2) + "]"));
		}

		columnsTbl.add(new AbstractColumn<RegionDetails>(new Model<String>("Image")) {
			public void populateItem(Item<ICellPopulator<RegionDetails>> cellItem, String type,
					IModel<RegionDetails> model) {
				cellItem.add(new ActionPanelSearchIdImg(type, model.getObject().getId(), search));
			}
		});

		//Annotations as link
		if (((WicketApplication) Application.get()).getAnnotationMatrix(experiment.getIdData()) != null) {
			columnsTbl.add(new AbstractColumn<RegionDetails>(new Model<String>("Annotations")) {

				public void populateItem(Item<ICellPopulator<RegionDetails>> cellItem, String type, IModel<RegionDetails> model) {
					cellItem.add(new ActionPanelSearchIdModal(type, experiment.getIdData(), model.getObject().getId()));
				}
			});
		}
	}
}
