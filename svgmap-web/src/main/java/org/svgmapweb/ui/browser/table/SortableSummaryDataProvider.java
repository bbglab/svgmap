/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.table;

import cern.colt.function.DoubleComparator;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.svgmapweb.model.RegionDetails;


public class SortableSummaryDataProvider extends SortableDataProvider<RegionDetails> {

	IModel<List<RegionDetails>> data;
	Comparator<RegionDetails> comparator; 

	public SortableSummaryDataProvider(IModel<List<RegionDetails>> rows) {
		data = rows;
		comparator = new GenericSortableComparator();
	}

    /**
     * @see org.apache.wicket.markup.repeater.data.IDataProvider#size()
     */
    public int size()
    {
        return data.getObject().size();
    }

	public Iterator<RegionDetails> iterator(int first, int count) {

		Iterator<RegionDetails> it = null;
		if (data != null) {
			if (getSort() != null) {
              Collections.sort(data.getObject(), comparator);
            }
			it = data.getObject().subList(first,first+count).iterator();
		}
		return it;
	}



	public IModel<RegionDetails> model(RegionDetails object) {
		return new Model(object);
	}


	// INNER CLASS (Default GenericSortableComparator)
    class GenericSortableComparator implements Comparator<RegionDetails>, Serializable {

        public int compare(final RegionDetails o1, final RegionDetails o2) {

            PropertyModel<Comparable> model1 = new PropertyModel<Comparable>(o1, getSort().getProperty());
            PropertyModel<Comparable> model2 = new PropertyModel<Comparable>(o2, getSort().getProperty());

			if (model1 == null || model1.getObject() == null || ((String) model1.getObject()).isEmpty())
				return -1;
			
			if (model2 == null || model2.getObject() == null || ((String) model2.getObject()).isEmpty())
				return 1;
			
			//TODO: improve this with formatters
			int result;
			if (getSort().getProperty().contains("values")) {

				Double value1 = Double.valueOf((String) model1.getObject());
				Double value2 = Double.valueOf((String) model2.getObject());
				result = value1.compareTo(value2);

			} else
				result = model1.getObject().compareTo(model2.getObject());				

            if (!getSort().isAscending()) {
                result = -result;
            }

            return result;
        }
    }
}
