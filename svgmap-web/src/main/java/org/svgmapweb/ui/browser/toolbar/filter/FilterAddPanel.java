/**
 *  Copyright 2011 Universitat Pompeu Fabra.
 *
 *  This software is open source and is licensed under the Open Software License
 *  version 3.0. You may obtain a copy of the License at
 *
 *      http://www.opensource.org/licenses/osl-3.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.svgmapweb.ui.browser.toolbar.filter;

import java.util.ArrayList;
import org.svgmapweb.model.filters.Filter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.ChoiceRenderer;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.ui.SvgmapSession;

/**
 * Simple lightweight form panel.
 * @author armand
 */
public class FilterAddPanel extends Panel {

	private Filter filterPanelObj = new Filter();
	
	private List<String> fieldSelectionList;
	private TextArea value;

	private static List<FilterOperator> OpSelectionList = Arrays.asList(
				new FilterOperator('c',"contains"),
				new FilterOperator('=',"equals to"),
				new FilterOperator('i',"equals to (ignoring case)"),
				new FilterOperator('g',"greater than"),
				new FilterOperator('G',"greater or equals than"),
				new FilterOperator('m',"minor than"),
				new FilterOperator('M',"minor or equals than"),
				new FilterOperator('e',"is empty"),
				new FilterOperator('n',"is not empty"),
				new FilterOperator('s',"starts with"));

	public List<FilterOperator> getOpSelectionList() {
		return OpSelectionList;
	}

	public List<String> getFieldSelectionList() {
		return fieldSelectionList;
	}
	
	public FilterAddPanel(String id, List<String> filterableFields, final Integer idData, final ModalWindow window) {
		super(id);

		setOutputMarkupId(true);

		window.setInitialHeight(200);
		this.filterPanelObj =  new Filter();
		this.fieldSelectionList = filterableFields;

		//Add form
		final Form form = new Form("form");

		// Create feedback panels
		final FeedbackPanel feedback = new FeedbackPanel("uploadFeedback");
		feedback.setOutputMarkupId(true);
		form.add(feedback);

		//Add Components
		TextField<String> filterName = new TextField<String>("name", new PropertyModel<String>(filterPanelObj, "name"));
		filterName.setRequired(true);
		form.add(filterName);

		form.add(new DropDownChoice<String>("column", new PropertyModel<String>(filterPanelObj, "column"), getFieldSelectionList()).setRequired(true));

		DropDownChoice operator = new DropDownChoice<FilterOperator>(
			"operation",
			new PropertyModel<FilterOperator>(filterPanelObj, "operation"),
			getOpSelectionList(), new ChoiceRenderer("operatorName", "operatorIndex")
		);
		operator.setRequired(true);
		operator.setOutputMarkupId(true);
		form.add(operator);

		value = new TextArea<String>("value", new PropertyModel<String>(filterPanelObj, "values"));
		value.setRequired(false);
		value.setOutputMarkupId(true);
		form.add(value);

		AjaxSubmitLink yesButton = new AjaxSubmitLink("okBtn") {
			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				super.onError(target, form);
				error("Error processing the form");
				target.addComponent(feedback);
			}

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				if (filterPanelObj.getOperation().getOperatorIndex() != 'e'
						&& filterPanelObj.getOperation().getOperatorIndex() != 'n'
						&& (value.getModelObject() == null || value.getModelObject().toString().isEmpty()))
					error("The field value is empty");

				else {
					//Update list of filters in session 
					SvgmapSession session = (SvgmapSession) WebSession.get();
					HashMap<String, List<Filter>> userFilters = session.getUserFilters().get(idData);
					if (userFilters == null) {
						userFilters = new HashMap<String, List<Filter>>();
						session.getUserFilters().put(idData, userFilters);
					}

					List<Filter> filters = (userFilters.get("table") == null ? new ArrayList() : userFilters.get("table"));
					if (filters == null || filters.isEmpty())
						filters = new ArrayList<Filter>();

					filters.add(filterPanelObj);
					userFilters.put("table", filters);

					window.close(target);
				}
			}
		};

		form.add(yesButton);

		AjaxSubmitLink cancelButton = new AjaxSubmitLink("cancelBtn") {
			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				if (target != null) {
					window.close(target);
				}
			}

			@Override
			public void onSubmit(AjaxRequestTarget target, Form form) {
				if (target != null) {
					window.close(target);
				}
			}
		};
		form.add(cancelButton);

		add(form);
	}

	/**
	 * Validates the form data.
	 * @return
	 */
	public boolean isDataFormOk() {

		if (filterPanelObj.getName() == null || "".equals(filterPanelObj.getName())) {
			error("Filter name must be specified");
			return false;

		} else {
			return  filterPanelObj != null && filterPanelObj.getName() != null && !"".equals(filterPanelObj.getName())
					&& ((filterPanelObj.getValues() != null && filterPanelObj.getValues().length() > 0));
		}
	}
}
