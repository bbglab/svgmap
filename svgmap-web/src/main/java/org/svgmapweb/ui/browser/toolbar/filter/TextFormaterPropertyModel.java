/**
 *  Copyright 2011 Universitat Pompeu Fabra.
 *
 *  This software is open source and is licensed under the Open Software License
 *  version 3.0. You may obtain a copy of the License at
 *
 *      http://www.opensource.org/licenses/osl-3.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.svgmapweb.ui.browser.toolbar.filter;

import org.apache.wicket.model.PropertyModel;
import org.svgmapweb.model.utils.formaters.ITextFormater;

public class TextFormaterPropertyModel extends PropertyModel<String> {
	
	private ITextFormater formater;

	public TextFormaterPropertyModel(Object modelObject, String expression, ITextFormater formater) {
		super(modelObject, expression);
		this.formater = formater;
	}

	@Override
	public String getObject() {
		Object value = super.getObject();
		return (value==null?null:formater.format(value));
	}
	
	

	
}
