/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svgmapweb.ui.browser.image.scale;

import edu.upf.bg.color.utils.ColorUtils;
import edu.upf.bg.colorscale.AbstractColorScale;
import edu.upf.bg.colorscale.ColorScalePoint;
import edu.upf.bg.colorscale.impl.LinearTwoSidedColorScale;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.model.scales.ColorScaleRangeExt;
import org.svgmapweb.model.scales.IScalePanel;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.model.wrappers.SelectOption;
import org.svgmapweb.ui.SvgmapSession;
import org.svgmapweb.ui.WicketApplication;

import java.awt.*;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Linear2SidedScalePanel extends Panel implements IScalePanel {

	private LinearTwoSidedColorScale scale;
	private IModel<SelectOption> selectedOption = new Model<SelectOption>();
	private RadioChoice<SelectOption> linearScaleDefaults;
	private Integer idData;
	private TextField max;
	private IModel<String> maxColorModel;
	private TextField mid;
	private IModel<String> midColorModel;
	private TextField min;
	private IModel<String> minColorModel;
	private static List<SelectOption> linearValueTypes = Arrays.asList(
			new SelectOption(SvgmapConstants.SCALE_RANGES_ALL_DATA, "Min and Max values according to all data"),
			new SelectOption(SvgmapConstants.SCALE_RANGES_WITHOUT_OUTLIER, "Min and Max values according to all data without outliers"),
			new SelectOption(SvgmapConstants.SCALE_RANGES_KEY, "Min and Max values according to key selected"),
			new SelectOption(SvgmapConstants.SCALE_RANGES_USER, "Min and Max values according to user selection"));

	public Linear2SidedScalePanel(String id, LinearTwoSidedColorScale scale) {

		super(id);

		/*Add default value scale */
		add(new Label("msg8", "Default linear two-sided scale values"));

		ChoiceRenderer choiceRenderer = new ChoiceRenderer("value", "key");
		linearScaleDefaults = new RadioChoice("scaleDefaults", selectedOption, linearValueTypes, choiceRenderer);
		linearScaleDefaults.setOutputMarkupId(true);
		add(linearScaleDefaults);

		/* Max min options*/
		this.scale = scale;

		max = new TextField("max", new Model(scale.getMax().getValue()));
		max.setEnabled(true);
		max.setOutputMarkupId(true);
		max.setMarkupId("linear-max");

		maxColorModel = new Model(ColorUtils.colorToHexHtml(scale.getMax().getColor()));
		ColorPickerPanel maxColor = new ColorPickerPanel("maxColorCPicker", "maxColor", maxColorModel);

		min = new TextField("min", new Model(scale.getMin().getValue()));
		min.setEnabled(true);
		min.setOutputMarkupId(true);
		min.setMarkupId("linear-min");

		minColorModel = new Model(ColorUtils.colorToHexHtml(scale.getMin().getColor()));
		ColorPickerPanel minColor = new ColorPickerPanel("minColorCPicker", "minColor", minColorModel);

		mid = new TextField("mid", new Model(scale.getMid().getValue()));
		mid.setEnabled(true);

		midColorModel = new Model(ColorUtils.colorToHexHtml(scale.getMid().getColor()));
		ColorPickerPanel midColor = new ColorPickerPanel("midColorCPicker", "midColor", midColorModel);

		add(max);
		add(maxColor);
		add(mid);
		add(midColor);
		add(min);
		add(minColor);
	}

	public String getPanelName() {
		return "Linear two sided scale";
	}

	public AbstractColorScale getScale() {
		return scale;
	}

	public TextField getMax() {
		return max;
	}

	public void setMax(TextField max) {
		this.max = max;
	}

	public TextField getMin() {
		return min;
	}

	public void setMin(TextField min) {
		this.min = min;
	}

	// FIXME : Q&D
	/* Set old values */
	public void resetScale(AbstractColorScale scale) throws Exception {

		this.scale = (LinearTwoSidedColorScale) scale;
		max.setModelObject(this.scale.getMax().getValue());
		maxColorModel.setObject(ColorUtils.colorToHexHtml(this.scale.getMax().getColor()));
		min.setModelObject(this.scale.getMin().getValue());
		minColorModel.setObject(ColorUtils.colorToHexHtml(this.scale.getMin().getColor()));
		mid.setModelObject(this.scale.getMid().getValue());
		midColorModel.setObject(ColorUtils.colorToHexHtml(this.scale.getMid().getColor()));

		//Set old selected option
		String defaultLinearScaleValues = null;

		SvgmapSession session = (SvgmapSession) WebSession.get();
		HashMap<String, String> scaleRangesExp = session.getScaleDefaultRangeType().get(idData);

		if (scaleRangesExp == null || scaleRangesExp.get(SvgmapConstants.LINEAR2SIDED) == null) {
			//ExperimentDataView experiment = SVGmapDBOperations.getBeanById("idData", idData, "ExperimentDataView", ExperimentDataView.class, null);
			ExperimentDataView experiment = ExperimentDataViewJpaController.findExperimentDataView(idData);
			defaultLinearScaleValues = experiment.getLinearScaleDefaults();
		} else
			defaultLinearScaleValues = session.getScaleDefaultRangeType().get(idData).get(SvgmapConstants.LINEAR2SIDED);


		this.setDefaultValueType(defaultLinearScaleValues);
	}

	/*Set new values*/
	public void updateScale() throws Exception {
		//Update session
		SvgmapSession session = (SvgmapSession) WebSession.get();
		HashMap<String, String> scaleDefault = new HashMap();
		scaleDefault.put(SvgmapConstants.LINEAR2SIDED, selectedOption.getObject().getKey());
		session.getScaleDefaultRangeType().put(idData, scaleDefault);

		if (mid.getModelObject() == null)
			mid.setDefaultModelObject(0);

		//Update values
		if (selectedOption.getObject().getKey().equals(SvgmapConstants.SCALE_RANGES_USER)) {

			updateScaleForUserValues(session);

		} else {

			if (selectedOption.getObject().getKey().equals(SvgmapConstants.SCALE_RANGES_ALL_DATA)
					|| selectedOption.getObject().getKey().equals(SvgmapConstants.SCALE_RANGES_WITHOUT_OUTLIER)) {

				updateScaleForWholeDataValues(session);

			} else {

				UpdateScaleForKeyWord(session);
			}
		}
	}

	private void UpdateScaleForKeyWord(SvgmapSession session) throws NumberFormatException, ParseException {
		String midValue = mid.getValue();

		if (midValue == null || midValue.isEmpty())
			midValue = "0";

		this.scale = new LinearTwoSidedColorScale(
				new ColorScalePoint(0,  Color.decode(minColorModel.getObject())),
				new ColorScalePoint(Double.parseDouble(midValue), Color.decode(midColorModel.getObject())),
				new ColorScalePoint(0, Color.decode(maxColorModel.getObject())));

		HashMap<String, ColorScaleRangeExt> scaleUserVal = new HashMap();

		scaleUserVal.put(SvgmapConstants.LINEAR2SIDED,
				new ColorScaleRangeExt(0, Double.parseDouble(midValue), 0));

		session.getScaleUserRange().put(idData, scaleUserVal);
	}

	private void updateScaleForWholeDataValues(SvgmapSession session) throws Exception {
		HashMap<String, ColorScaleRangeExt> ScaleRangeData = ((WicketApplication) Application.get()).getMappingsMatrixRanges(idData);
		ColorScaleRangeExt range = ScaleRangeData.get(selectedOption.getObject().getKey());
		this.scale = new LinearTwoSidedColorScale(
				new ColorScalePoint(range.getMin(), Color.decode(minColorModel.getObject())),
				new ColorScalePoint(Double.parseDouble(mid.getValue()), Color.decode(midColorModel.getObject())),
				new ColorScalePoint(range.getMax(), Color.decode(maxColorModel.getObject())));

		HashMap<String, ColorScaleRangeExt> scaleUserVal = new HashMap();
		scaleUserVal.put(SvgmapConstants.LINEAR2SIDED,
				new ColorScaleRangeExt(
				range.getMax(),
				Double.parseDouble(mid.getValue()),
				range.getMin()));
		session.getScaleUserRange().put(idData, scaleUserVal);
	}

	private void updateScaleForUserValues(SvgmapSession session) throws NumberFormatException, ParseException {

		if (min.getModelObject() == null
				|| max.getModelObject() == null
				|| (Double.parseDouble(min.getValue()) >Double.parseDouble(max.getValue()))) {
			throw new WicketRuntimeException("Max and Min values are not correct");
		} else {

			if (Double.parseDouble(mid.getValue()) > Double.parseDouble(max.getValue()))
				mid.setDefaultModelObject(Double.parseDouble(max.getValue()));

			if (Double.parseDouble(mid.getValue()) < Double.parseDouble(min.getValue()))
				mid.setDefaultModelObject(Double.parseDouble(min.getValue()));

			this.scale = new LinearTwoSidedColorScale(
					new ColorScalePoint(Double.parseDouble(min.getValue()), Color.decode(minColorModel.getObject())),
					new ColorScalePoint(Double.parseDouble(mid.getValue()), Color.decode(midColorModel.getObject())),
					new ColorScalePoint(Double.parseDouble(max.getValue()), Color.decode(maxColorModel.getObject())));
		}
		HashMap<String, ColorScaleRangeExt> scaleUserVal = new HashMap();
		new ColorScaleRangeExt(scale.getRange().getMax(), scale.getRange().getMin());
		scaleUserVal.put(SvgmapConstants.LINEAR2SIDED,
				new ColorScaleRangeExt(
				scale.getMin().getValue(),
				scale.getMid().getValue(),
				scale.getMax().getValue()));
		session.getScaleUserRange().put(idData, scaleUserVal);
	}

	public String getScaleValues() {
		StringBuilder str = new StringBuilder();

		str.append("Min: " + SvgmapUtils.roundToDecimals(scale.getMin().getValue(), 3));
		str.append(", Mid: " + SvgmapUtils.roundToDecimals(scale.getMid().getValue(),3));
		str.append(", Max: " + SvgmapUtils.roundToDecimals(scale.getMax().getValue(), 3));

		return str.toString();
	}

	public String getDefaultValueType() {
		return ((SelectOption) linearScaleDefaults.getDefaultModelObject()).getKey();
	}

	public void setDefaultValueType(String defaultValueType) {
		for (SelectOption opt : linearValueTypes) {
			if (opt.getKey().equals(defaultValueType)) {
				linearScaleDefaults.setModelObject(opt);
				break;
			}
		}
	}

	public void setExperimentId(Integer idData) {
		this.idData = idData;
	}

}
