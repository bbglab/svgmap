/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.ui.browser.image.scale;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;


/**
 * This color picker is related with js_color_picker_v2.js, color_functions.js and js_color_picker_v2.css files.
 * 
 * @author armand
 */
public class ColorPickerPanel extends Panel {

	IModel<String> colorModel;
			
	public ColorPickerPanel(String id, final String javascriptId, final IModel<String> colorModel) {
		super(id);

		setOutputMarkupId(true);
		
		this.colorModel = colorModel;

		TextField colorName = new TextField("colorName",colorModel) {
			@Override
			protected void onComponentTag(ComponentTag tag) {
				tag.put("id", javascriptId);
				if (colorModel.getObject() != null && !"".equals(colorModel.getObject()) ) {
					tag.put("style","background-color:" + colorModel.getObject());
				} else {
					tag.put("style","");
				}
				super.onComponentTag(tag);
			}
		};
		/*colorName.add(new AjaxEventBehavior("onchange") {
			@Override
			protected void onEvent(AjaxRequestTarget target) {
				updateColorLookLike();
			}
		});*/
		add( colorName);
		 AjaxLink buttonColor = new AjaxLink("buttonColor") {

			@Override
			protected void onComponentTag(ComponentTag tag) {
				tag.put("id", getButtonId(javascriptId)); // Just to put an id
				super.onComponentTag(tag);				
			}
		
			@Override
			public void onClick(AjaxRequestTarget target) {				
				String jsFunction = "showColorPicker(document.getElementById('" + getButtonId(javascriptId) + "'), document.forms[0]." + javascriptId + ")";
				//updateColorLookLike();
				target.appendJavascript(jsFunction);
				target.addComponent(ColorPickerPanel.this);
			}
		};
		buttonColor.setMarkupId(getButtonId(javascriptId)); // Force to use this id
		add(buttonColor);		
		//updateColorLookLike();
	}

	/*
	private void updateColorLookLike() {
		Label colorLookLike = new Label("colorLookLike") {
			@Override
			protected void onComponentTag(ComponentTag tag) {
				if (colorModel.getObject() != null && !"".equals(colorModel.getObject())) {
					tag.put("style", "width:25px; background-color:" + colorModel.getObject());
				} else {
					tag.put("style", "width:25px");
				}
				super.onComponentTag(tag);
			}

		};
		colorLookLike.setOutputMarkupId(true);
		addOrReplace(colorLookLike);
	}*/

	private String getButtonId(String javascriptId) {
		return "bc" + javascriptId;
	}
	
}
