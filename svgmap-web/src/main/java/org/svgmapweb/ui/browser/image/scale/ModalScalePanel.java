/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svgmapweb.ui.browser.image.scale;

import edu.upf.bg.colorscale.AbstractColorScale;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.svgmapweb.model.scales.IScalePanel;
import org.svgmapweb.model.wrappers.ScaleWrapper;

public class ModalScalePanel extends Panel {
	/*
	private LinearScalePanel linearPanel;
	private PvalueScalePanel PvaluePanel;
	 */

	private AbstractColorScale origScale;
	private Form form;
	private IModel<ScaleWrapper> scalePanelModel;
	private FeedbackPanel feedback;

	public ModalScalePanel(String id, final ModalWindow window, IModel<ScaleWrapper> panelModel/*final IScalePanel panel*/) {

		super(id);

		window.setInitialHeight(200);
		setOutputMarkupPlaceholderTag(true);

		this.scalePanelModel = panelModel;
		final IScalePanel panel = (panelModel.getObject() != null) ? (IScalePanel) panelModel.getObject().getPanel() : null;

		origScale = panel.getScale();

		feedback = new FeedbackPanel("feedInnerPanel");
		feedback.setOutputMarkupId(true);
		add(feedback);
		
		this.form = new Form("form");
		add(form);

		// Panels (only we display one)		
		displaySelectedScalePanel();

		window.setTitle("Scale properties");
		window.setInitialWidth(250);

		AjaxSubmitLink yesButton = new AjaxSubmitLink("okBtn") {

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				super.onError(target, form);
				error("Error processing the form");
				target.addComponent(feedback);
			}

			@Override
			public void onSubmit(AjaxRequestTarget target, Form form) {
				try {
					if (panel != null) {
						panel.updateScale();
						if (target != null) {
							window.close(target);
						}
					}
				} catch (Exception e) {
					error(e.getLocalizedMessage());
				}
			}
		};
		form.add(yesButton);

		AjaxSubmitLink noButton = new AjaxSubmitLink("cancelBtn") {
			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				super.onError(target, form);
				window.close(target);
			}

			@Override
			public void onSubmit(AjaxRequestTarget target, Form form) {
				try {
					if (panel != null) {
						panel.resetScale(origScale);
						if (target != null) {
							window.close(target);
						}
					}
				} catch (Exception e) {
					window.close(target);
				}
			}
		};
		form.add(noButton);
	}

	private void displaySelectedScalePanel() {
		IScalePanel panel = (IScalePanel) scalePanelModel.getObject().getPanel();
		if (panel != null) {
			if (panel.getPanelName().equals("Linear scale")) {
				// Put blank
				form.addOrReplace((new EmptyPanel("pvalue")).setVisible(false));
				form.addOrReplace((new EmptyPanel("linear2sided")).setVisible(false));
			} else {
				if (panel.getPanelName().equals("P-value scale")) {
					form.addOrReplace((new EmptyPanel("linear")).setVisible(false));
					form.addOrReplace((new EmptyPanel("linear2sided")).setVisible(false));
				} else {
					form.addOrReplace((new EmptyPanel("linear")).setVisible(false));
					form.addOrReplace((new EmptyPanel("pvalue")).setVisible(false));
				}
			}
			form.addOrReplace((Panel) panel);
		} else {
			// panel is null
			form.addOrReplace((new EmptyPanel("pvalue")).setVisible(false));
			form.addOrReplace((new EmptyPanel("linear")).setVisible(false));
			form.addOrReplace((new EmptyPanel("linear2sided")).setVisible(false));
		}

	}

	@Override
	protected void onBeforeRender() {

		displaySelectedScalePanel();
		super.onBeforeRender();
	}
}
