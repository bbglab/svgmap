package org.svgmapweb.ui.browser.image.modal;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.batik.dom.util.DOMUtilities;
import org.apache.wicket.Page;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.svgmap.cli.SvgOperations;
import org.svgmapweb.model.utils.FileUtils;
import org.svgmapweb.ui.browser.image.ResultSVGPanel;
import org.w3c.dom.Element;
import org.w3c.dom.svg.SVGDocument;


/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class ModalSVGPage extends Page {

	private String SVGPath;

    @Override
    public String getMarkupType() {
        return "image/svg+xml";
    }

    @Override
    protected void onRender(MarkupStream markupStream) {
		try {
			this.setEscapeModelStrings(false);
			this.getResponse().setContentType("image/svg+xml");

			PrintWriter writer = new PrintWriter(this.getResponse().getOutputStream());
			SVGDocument domSVG = SvgOperations.readSVG(SVGPath);
			domSVG = SvgOperations.makeScalable(domSVG);

			writer.write(SvgOperations.toString(domSVG));
			writer.flush();
			writer.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			error("unnable to render the svg image");
		}
    }

	public ModalSVGPage(final ResultSVGPanel resultPanel, final ModalWindow window, String SVGPath, String nameLbl) {

		this.SVGPath = SVGPath;
		
		window.setTitle(nameLbl);
		window.setInitialHeight(600);
		window.setInitialWidth(480);
	}
}

