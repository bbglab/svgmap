/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.ui.browser.image.annotations;

import cern.colt.matrix.ObjectMatrix2D;
import java.io.File;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.svgmapweb.db.jpa.ExperimentFile;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.utils.FileUtils;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.WicketApplication;


public class ModalAnnotationPanel extends Panel {

    public ModalAnnotationPanel(String id, final ModalWindow window, final Integer idData, final String idKey)
    {
		super(id);
		try {
			final WebMarkupContainer parent = new WebMarkupContainer("wrapper");
			parent.setOutputMarkupId(true);
			add(parent);

			window.setTitle("Annotations details");
			window.setInitialHeight(200);
			window.setInitialWidth(250);

			final Form form = new Form("form");
			final ObjectMatrix2D annotationMatrix = ((WicketApplication) Application.get()).getAnnotationMatrix(idData);

			// Repeating view
			boolean showLbl = true;
			RepeatingView rv = new RepeatingView("repeatedView");
			parent.add(rv);
			if (annotationMatrix != null ) {
				for (int i = 0; i < annotationMatrix.rows(); i++) {

					if (annotationMatrix.viewRow(i).get(0).equals(idKey)) {

						//Get annotation headers
						ExperimentFile annotationFile = ((WicketApplication) Application.get()).getExperimentFile(idData, SvgmapConstants.TYPE_ANNOTATION);
						String hdrAnnotations[] = SvgmapUtils.readHeaderCSVfile(new File(FileUtils.getUploadFolder() + File.separator + annotationFile.getPath()));

						for (int j = 0; j < annotationMatrix.viewRow(i).cardinality(); j++) {
							WebMarkupContainer wmc = new WebMarkupContainer(rv.newChildId());
							rv.add(wmc);

							Label name = new Label("name", hdrAnnotations[j]);
							Label value = new Label("value", annotationMatrix.viewRow(i).get(j).toString());
							wmc.add(name);
							wmc.add(value);
						}
						showLbl = false;

					}
				}
			}

			Label txtNotFound = new Label("txt", "No annotations found for the key: " + idKey);
			txtNotFound.setVisible(showLbl);
			parent.add(txtNotFound);

		} catch (Exception ex) {
			throw new WicketRuntimeException(ex);
		}

	}
}
