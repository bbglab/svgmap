/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser;

import org.apache.wicket.markup.ComponentTag;
import org.svgmapweb.ui.layout.InfoSearchPanel;
import java.util.List;
import org.apache.wicket.Application;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.OnChangeAjaxBehavior;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.ui.BrowsePage;
import org.svgmapweb.ui.WicketApplication;
//import org.svgmapweb.model.ExperimentDataView;
//import org.svgmapweb.db.deprecated.SVGmapDBOperations;

public class BrowserPanel extends Panel {

    private static final long serialVersionUID = 1L;

    private DropDownChoice option;
	private ExperimentDataView currentExperiment;
	private MultiLineLabel description = null;
	private Image imgSvg = null;
	private Label lblDetails = null;

    public BrowserPanel(String id, final IModel<SvgSearch> searchModel) {
		super(id);
		try {

			SvgSearch search = searchModel.getObject();

			setOutputMarkupId(true);

			currentExperiment = search.getExperiment();

			add(new InfoSearchPanel("infoText"));

			final Form form = new Form("form");
			form.add(new Label("msg", "Experiment "));

			//Get all experiments
			List<ExperimentDataView> searchList = ExperimentDataViewJpaController.findExperimentDataViewEntities();

			// Add experiment selector
			ChoiceRenderer choiceRenderer = new ChoiceRenderer("name"/*data.getDisplay()*/, SvgmapConstants.keysExperimentDataView[0] /*data.getKeys()[0]*/);
			IModel<ExperimentDataView> selectedValue = new Model<ExperimentDataView>(currentExperiment);

			option = new DropDownChoice("combo", selectedValue, searchList, choiceRenderer);
			option.setOutputMarkupPlaceholderTag(true);
			option.setOutputMarkupId(true);			
			option.add(new OnChangeAjaxBehavior() {				
				@Override
				protected void onUpdate(AjaxRequestTarget target) {

					if (lblDetails.getDefaultModelObject().toString().toLowerCase().contains("hide")) {
						
						target.addComponent(description);
						String js = "$('div[name=msgDesc]').autolink(); "
									+ "$('div[name=msgDesc]').mailto();";
						target.appendJavascript(js);

						try {
							ExperimentDataView d = (ExperimentDataView) this.getComponent().getDefaultModelObject();
							target.addComponent(imgSvg);
							String pathControlImg = "";
							if (d != null && d.getIdData() != null) {
								pathControlImg = (((WicketApplication) Application.get())).getControlImage(d.getIdData());
							}

							imgSvg.setDefaultModelObject("/" + pathControlImg);
						} catch (Exception ex) {
							error("Unable to preview the experiment image");
						}
					}
				}
			}
			);
			form.add(option);

			//Add description
			description = new MultiLineLabel("msgDesc", new PropertyModel(option.getModel(),"description"));
			description.setEscapeModelStrings(false);
			description.setOutputMarkupId(true);

			/* View Image */
			String pathControlImg = "";
			if (currentExperiment != null && currentExperiment.getIdData() != null)
				pathControlImg = (((WicketApplication) Application.get())).getControlImage(currentExperiment.getIdData());

			imgSvg = new Image("svgImg", new Model(pathControlImg)) {
				@Override
				protected void onComponentTag(ComponentTag tag) {
					super.onComponentTag(tag);
					if (this.getDefaultModelObjectAsString().isEmpty() || this.getDefaultModelObjectAsString().equals("/"))
						tag.put("width", "0");
					else {
						tag.put("width", "250");
						tag.put("style","border-style: dotted;border-width: 0px");
						tag.put("src", "resources/org.svgmapweb.ui.browser.image.ResultSVGPanel//" + this.getDefaultModelObjectAsString());
					}
				}
			};
			imgSvg.setOutputMarkupId(true);
			
			form.add( new AjaxButton("viewBtn") {
				@Override
				protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
					SvgSearch search = new SvgSearch();

					if (option.getModelObject() == null)
						error("No value selected");
					else {
						try {
							search.setExperiment((ExperimentDataView) option.getModelObject());
							setResponsePage(new BrowsePage(new Model(search)));
						} catch(Exception ex) {
							ex.printStackTrace();
							error("Error loading the selected experiment.");
						}
					}
				}
			});

			final WebMarkupContainer group = new WebMarkupContainer("groupLog");
			group.setOutputMarkupPlaceholderTag(true);
			group.setVisible(false);
			group.add(new Label("msg1", "Experiment description: "));
			group.add(description);
			group.add(imgSvg);
			form.add(group);

			lblDetails = new Label("lblDetails", new Model<String>("Show details  >>"));
			lblDetails.setOutputMarkupPlaceholderTag(true);

			AjaxFallbackLink detailsLink = new AjaxFallbackLink("detailsLnk") {
				@Override
				public void onClick(AjaxRequestTarget target) {
					group.setVisible(!group.isVisible());
					if (group.isVisible()) {
						lblDetails.setDefaultModelObject("Hide details  <<");
						
						target.addComponent(description);
						String js = "$('div[name=msgDesc]').autolink(); "
									+ "$('div[name=msgDesc]').mailto();";
						target.appendJavascript(js);
						try {

							target.addComponent(imgSvg);
							String pathControlImg = "";
							ExperimentDataView d = (ExperimentDataView) option.getModelObject();
							if (d != null && d.getIdData() != null) {
								pathControlImg = (((WicketApplication) Application.get())).getControlImage(d.getIdData());
							}

							imgSvg.setDefaultModelObject("/" + pathControlImg);
						} catch (Exception ex) {
							error("Unable to preview the experiment image");
						}
					} else
						lblDetails.setDefaultModelObject("Show details  >>");
					
					if (target != null) {
						target.addComponent(lblDetails);
						target.addComponent(group);
					}					
				}
			};
			detailsLink.add(lblDetails);
			form.add(detailsLink);

			add(form);

		} catch (Exception ex) {
			error("Unable to load the experiment");
		}
    }
}
