/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.ui.browser.toolbar.export;
import java.util.List;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.WebResource;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.protocol.http.WebResponse;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.resource.StringResourceStream;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.utils.SvgmapUtils;

public class ExportsPanel extends Panel {

	public ExportsPanel(String id, final IModel<SvgSearch> searchModel, final IModel<List<RegionDetails>> details) {
		super(id);

		final Integer idData = searchModel.getObject().getExperiment().getIdData();
		final String name = searchModel.getObject().getExperiment().getName().trim().replace(" ", "_");

		WebResource export = new WebResource() {
			@Override
			public IResourceStream getResourceStream() {
				try {
					List<String> header = SvgmapUtils.readHeader(idData, SvgmapConstants.TYPE_MAPPING);
					return new StringResourceStream(SvgmapUtils.getDetailsAsString(header, details.getObject()), "text/plain");
				} catch (Exception ex) {
					throw new WicketRuntimeException(ex);
				}
			}

			@Override
			protected void setHeaders(WebResponse response) {
				super.setHeaders(response);
				response.setAttachmentHeader(name + "_data.tsv");
			}
		};

		export.setCacheable(false);
		ResourceLink exportLink = new ResourceLink("exportCsvLink", export);
		exportLink.setEnabled(true);
		add(exportLink);

	}
}
