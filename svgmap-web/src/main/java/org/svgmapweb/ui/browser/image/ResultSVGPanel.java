/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svgmapweb.ui.browser.image;

import org.svgmapweb.ui.browser.image.modal.ModalSVGPage;
import org.svgmapweb.ui.browser.image.modal.ModalPNGPage;
import edu.upf.bg.colorscale.IColorScaleHtml;
import edu.upf.bg.colorscale.drawer.ColorScaleDrawer;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import org.apache.wicket.Application;
import org.apache.wicket.Page;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebResource;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.ResourceLink;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import org.apache.wicket.protocol.http.WebResponse;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;
import org.gitools.label.MatrixRowsLabelProvider;
import org.gitools.matrix.filter.MatrixViewLabelFilter;
import org.gitools.matrix.model.MatrixView;

import org.gitools.persistence.PersistenceException;
import org.svgmap.cli.SvgOperations;

import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.SVGMapDBOperationsHandle;

import org.svgmapweb.model.filters.FilterFactory;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.filters.FilterByKey;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.ui.browser.image.annotations.ActionPanelSearchIdLink;
import org.svgmapweb.model.wrappers.ColumnWrapper;
import org.svgmapweb.model.utils.FileUtils;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.model.wrappers.ScaleWrapper;
import org.svgmapweb.ui.SvgmapSession;
import org.svgmapweb.ui.browser.image.annotations.ActionPanelSearchIdModal;
import org.svgmapweb.ui.browser.table.SortableSummaryDataProvider;
import org.w3c.dom.svg.SVGDocument;

public class ResultSVGPanel extends Panel {

	private RepeatingView imgContainer = new RepeatingView("imgColumns");
	private List<String> newSVG = null;
	private String scaleImgPath = null;
	private String currentPNGPath;
	private String currentSVGPath;
	private String currentLblImage;
	private ExperimentDataView experiment;
	private IModel<List<RegionDetails>> dataModel = new Model();

	private SvgSearch search;

	public ResultSVGPanel(String id, final IModel<SvgSearch> searchModel) throws Exception {
		super(id);

		try {
		//Init local variables
		search = searchModel.getObject();

		//Experiment data
		experiment = search.getExperiment();
		List<ColumnWrapper> columns = search.getImgColumnOptionsSelected();
		String searchText = search.getSearchText();
		ScaleWrapper scaleSelected = search.getScale();

		// Convert searched keyword into key
		List<String> keyList = SvgmapUtils.convertKeywordsToKeys(experiment.getIdData(), searchText);

		//Add image modal panel
		final SvgmapSession session = (SvgmapSession) WebSession.get();
		final ModalWindow modal1 = new ModalWindow("modal1") ;
		modal1.setPageMapName("modal-2");
		modal1.setCookieName("modal-2");
		modal1.setPageCreator(new ModalWindow.PageCreator() {
			public Page createPage() {
				if (session.isSvgSupported())
					return new ModalSVGPage(ResultSVGPanel.this, modal1, currentSVGPath, currentLblImage);
				else
					return new ModalPNGPage(ResultSVGPanel.this, modal1, currentPNGPath, currentLblImage);
			}
		});
		add(modal1);

		// Load values for components
		if (keyList == null || keyList.isEmpty() || searchText == null || searchText.isEmpty() ||
				columns == null || columns.isEmpty() ||	scaleSelected == null)

			loadDefaultComponents(modal1);
		else
			loadComponents(keyList.get(0), scaleSelected, columns, modal1);


		} catch (Exception ex) {
			error("Example unnable to load, check if it is correctly configured");
		}
	}

	private String filterByKey(String key, final MatrixView mappingMatrix) {

		int[] rows = MatrixViewLabelFilter.filterLabels(new MatrixRowsLabelProvider(mappingMatrix), Arrays.asList(new String[]{search.getSearchText()}), false, mappingMatrix.getVisibleRows());
		if (rows.length == 0) {
			warn("Keyword not found in data or annotation files");
		} else {
			key = mappingMatrix.getRowLabel(rows[0]);
		}
		return key;
	}

	private List<IColumn<?>> makeTblColumns() throws PersistenceException, IOException {
		List<IColumn<?>> columnsTbl = new ArrayList(0);

		List<String> hdrMapp = SvgmapUtils.readHeader(experiment.getIdData(), SvgmapConstants.TYPE_MAPPING);
		List<String> hdrAnn = SvgmapUtils.readHeader(experiment.getIdData(), SvgmapConstants.TYPE_ANNOTATION);

		if (experiment.getGeneLink() == null || experiment.getGeneLink().isEmpty()) {
			columnsTbl.add(new PropertyColumn(new Model<String>(hdrMapp.get(1)), "id", "id"));
		} else {
			columnsTbl.add(new AbstractColumn<RegionDetails>(new Model<String>(hdrMapp.get(1))) {
				public void populateItem(Item<ICellPopulator<RegionDetails>> cellItem, String type, IModel<RegionDetails> model) {
					cellItem.add(new ActionPanelSearchIdLink(type, model.getObject().getId(), experiment.getGeneLink()));
				}
			});
		}

		// Adding name column
		if (((WicketApplication) Application.get()).getAnnotationMatrix(experiment.getIdData()) != null) {
			columnsTbl.add(new PropertyColumn(new Model<String>(hdrAnn.get(1)), "name", "name"));
		}

		columnsTbl.add(new PropertyColumn(new Model<String>(hdrMapp.get(0)), "region", "region"));

		for (int i = 2; i < hdrMapp.size(); i++) {
				String col = hdrMapp.get(i);
				columnsTbl.add(new PropertyColumn(new Model<String>(col), "values[" + (i-2) + "]", "values[" + (i-2) + "]"));
		}

		// Adding annotation column
		if (((WicketApplication) Application.get()).getAnnotationMatrix(experiment.getIdData()) != null) {
			columnsTbl.add(new AbstractColumn<RegionDetails>(new Model<String>("Annotations")) {

				public void populateItem(Item<ICellPopulator<RegionDetails>> cellItem, String type, IModel<RegionDetails> model) {
					cellItem.add(new ActionPanelSearchIdModal(type, experiment.getIdData(), model.getObject().getId()));
				}
			});
		}
		return columnsTbl;
	}

	private File createImageScale(ScaleWrapper scaleSelected, String fileName) throws IOException {
		Integer width = 800, height = 60;
		File scaleImg = new File(fileName);
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		ColorScaleDrawer d = new ColorScaleDrawer(scaleSelected.getPanel().getScale());
		Rectangle box = new Rectangle(0, 0, width, height);
		Rectangle clip = g.getClipBounds();
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
		d.draw(g, box, clip);
		bi.flush();
		ImageIO.write(bi, "jpg", scaleImg);

		return scaleImg;
	}

	private void loadDefaultComponents(final ModalWindow modal1) throws Exception {
		Label lbl = new Label("message", "Control experiment image : ");

		String pathControlImg = (((WicketApplication) Application.get())).getControlImage(experiment.getIdData());
		Image imgSvg = new Image("svgImg", "/" + pathControlImg);

		String pathControlImgSvg  = FileUtils.getUploadFolder().getPath() + File.separator + ((WicketApplication) Application.get()).getExperimentFile(experiment.getIdData(), SvgmapConstants.TYPE_SVG).getPath();
		AjaxLink imgLink = new AjaxLink("showModal1", new Model<String>(lbl.getDefaultModelObjectAsString() + "#" + pathControlImg + "#" + pathControlImgSvg)) {
			@Override
			public void onClick(AjaxRequestTarget target) {
				String[] params = this.getDefaultModelObjectAsString().split("#");
				currentLblImage = params[0];
				currentPNGPath = params[1];
				currentSVGPath = params[2];
				modal1.show(target);
			}
		};

		WebMarkupContainer container = new WebMarkupContainer(imgContainer.newChildId());
		container.add(imgSvg);
		container.add(lbl);
		container.add(imgLink);
		imgContainer.add(container);
		add(imgContainer);

		//add scale
		Image imgScale = new Image("scaleImg") {
			@Override
			public boolean isVisible() {
				return false;
			}
		};
		add(imgScale);

		//Add label
		add(new Label("tableName", "Summary table") {
			@Override public boolean isVisible() {
				return false;
		}});

		add(new EmptyPanel("tableSummary"));

		//Download results
		WebResource export = createExport(null);
		export.setCacheable(false);
		ResourceLink resultsLink = new ResourceLink("downloadResults", export);
		add(resultsLink);
	}

	private void loadComponents(String key, ScaleWrapper scaleSelected, Iterable<ColumnWrapper> columns, final ModalWindow modal1) throws Exception {

		MatrixView mappingMatrix = ((WicketApplication) Application.get()).getMappingMatrix(experiment.getIdData());
		String basePath = FileUtils.getProcessFolder() + File.separator;

		//Create path files
		newSVG = new ArrayList<String>();
		String svgPath = SVGMapDBOperationsHandle.getExperimentFileByType(experiment.getIdData(), "svg").getPath();
		String initSvg = FileUtils.getUploadFolder() + File.separator + svgPath;

		//Create process files (svg, jpg's)
		Iterable<ColumnWrapper> imgColumnOptionsSelected = search.getImgColumnOptionsSelected();
		String randomName;
		String tmpFile;

		for (ColumnWrapper c : imgColumnOptionsSelected) {

			if (c.getIndex() >= 0) {
				randomName = SvgmapUtils.generateUniqueName(new String[] {key, c.getColumnName()});
				tmpFile = FileUtils.getProcessFolder().getPath() + File.separator + experiment.getName() + "_" + randomName;
				SVGDocument svgDom = SvgOperations.readSVG(initSvg);
				svgDom = SvgOperations.createSVGRegionsFromValue(svgDom, mappingMatrix, c.getIndex(), key, (IColorScaleHtml) scaleSelected.getPanel().getScale(), SvgmapUtils.readHeader(experiment.getIdData(), SvgmapConstants.TYPE_MAPPING));
				newSVG.add(SvgOperations.writeDomToFile(svgDom, tmpFile + ".svg", true).getName());
			}
		}

		//Add Images
		int i = 0; String svg; String path;
		Label lbl;
		Image imgSvg;
		AjaxLink imgLink;

		for (ColumnWrapper c : columns) {
			if (c.getIndex() >= 0) {
				svg = newSVG.get(i);
				WebMarkupContainer container = new WebMarkupContainer(imgContainer.newChildId());

				lbl = new Label("message", "Image [ "+ c.getColumnName() +" ]");

				SvgOperations.convertSvg2Png(basePath + svg);
				path = svg.substring(0, svg.lastIndexOf(".")) + ".png";
				imgSvg = new Image("svgImg", "/" + path);
				imgLink = new AjaxLink("showModal1", new Model<String>(lbl.getDefaultModelObjectAsString() + "#" + path + "#" + basePath + svg)) {
					@Override public void onClick(AjaxRequestTarget target) {
						String[] params = this.getDefaultModelObjectAsString().split("#");
						currentLblImage = params[0];
						currentPNGPath = params[1];
						currentSVGPath = params[2];
						modal1.show(target);
					}
				};

				container.add(lbl);
				container.add(imgSvg);
				container.add(imgLink);
				imgContainer.add(container);
				i++;
			}
		}
		add(imgContainer);

		//Add Scale
		randomName = SvgmapUtils.generateUniqueName(new String[] {key, "scale"});
		tmpFile = FileUtils.getProcessFolder().getPath() + File.separator + experiment.getName() + "_" + randomName;
		File scaleImg = createImageScale(scaleSelected, tmpFile + ".jpg");
		scaleImgPath = scaleImg.getName();
		Image imgScale = new Image("scaleImg", File.separator + scaleImgPath) {
			@Override
			public boolean isVisible() {
				return true;
			}
		};
		add(imgScale);

		//Add label
		add(new Label("tableName", "Summary table") {
			@Override public boolean isVisible() {
				return (newSVG != null && !newSVG.isEmpty());
		}});

		// Add Table results
		FilterByKey f = (FilterByKey) FilterFactory.createFilterByKey(experiment.getIdData(), mappingMatrix);
		final Map<String, List<RegionDetails>> mapRows = f.filter(Arrays.asList(new String[]{key}));

		//Columns
		List<IColumn<?>> columnsTbl = makeTblColumns();
		List<RegionDetails> lDetails = SvgmapUtils.mergeLists(mapRows.values());
		dataModel.setObject(lDetails);

		add(new DefaultDataTable("tableSummary", columnsTbl, new SortableSummaryDataProvider(dataModel), 10) {
			@Override public boolean isVisible() {
				return (newSVG != null && !newSVG.isEmpty());
			}});

		//Download results
		WebResource export = createExport(lDetails);
		export.setCacheable(false);
		ResourceLink resultsLink = new ResourceLink("downloadResults", export);
		add(resultsLink);
	}

	private WebResource createExport(final List<RegionDetails> lDetails) {

		WebResource export = new WebResource() {
			@Override
			public IResourceStream getResourceStream() {
					try {
						// Zip file
						List<String> filesToZip = new ArrayList<String>();
						File zipFile = new File(FileUtils.getProcessFolder().getPath() + File.separator + experiment.getName() + "_" + SvgmapUtils.generateUniqueName() + ".zip");

						//add control image
						String pathControlImg = ((WicketApplication) Application.get()).getExperimentFile(experiment.getIdData(), SvgmapConstants.TYPE_SVG).getPath();
						filesToZip.add(FileUtils.getUploadFolder().getPath() + File.separator + pathControlImg);

						if (scaleImgPath != null) {

							//add svg images
							for (String expFile : newSVG) {
								filesToZip.add(FileUtils.getProcessFolder().getPath() + File.separator + expFile);
							}

							//add scale
							filesToZip.add(FileUtils.getProcessFolder().getPath() + File.separator + scaleImgPath);

							//add csv summary table
							File csv = new File(
									FileUtils.getProcessFolder().getPath()
									+ File.separator
									+ search.getExperiment().getName().trim().replace(" ", "_")
									+ "_"
									+ (search.getSearchText() == null ? search.getSearchText() + "_" : "" )
									+ SvgmapUtils.generateUniqueName()
									+ ".tsv");

							BufferedWriter out = new BufferedWriter(new FileWriter(csv));
							out.write(SvgmapUtils.getDetailsAsString(SvgmapUtils.readHeader(experiment.getIdData(), SvgmapConstants.TYPE_MAPPING), lDetails));
							out.flush();
							out.close();
							filesToZip.add(csv.getAbsolutePath());
						}

						//make zip
						FileUtils.makeZipFile(filesToZip, zipFile.getAbsolutePath());

						return new FileResourceStream(zipFile);

					} catch (Exception ex) {
						throw new WicketRuntimeException(ex);
					}
			}

			@Override
			protected void setHeaders(WebResponse response) {
				super.setHeaders(response);

				StringBuilder fileName = new StringBuilder();
				fileName.append(search.getExperiment().getName().trim().replace(" ", "_"));
				fileName.append("_");
				fileName.append((search.getSearchText() == null || search.getSearchText().isEmpty() ? "" : search.getSearchText() + "_"));
				fileName.append("download.zip");

				response.setAttachmentHeader(fileName.toString());
			}
		};
		return export;
	}
}
