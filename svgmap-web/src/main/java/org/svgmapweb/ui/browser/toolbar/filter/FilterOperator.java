/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.ui.browser.toolbar.filter;

import java.io.Serializable;

public class FilterOperator implements Serializable{

	private char operatorIndex;
	private String operatorName;

	public FilterOperator(char operatorIndex, String operatorName) {
		this.operatorIndex = operatorIndex;
		this.operatorName = operatorName;
	}

	public char getOperatorIndex() {
		return operatorIndex;
	}

	public void setOperatorIndex(char operatorIndex) {
		this.operatorIndex = operatorIndex;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

}
