package org.svgmapweb.ui.browser.toolbar.filter;

import org.svgmapweb.model.filters.Filter;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.svgmapweb.model.utils.formaters.StringFormater;
import org.svgmapweb.ui.icons.Icons;


/**
 * It incorporate the checkbox, label and remove action if it is necessary.
 *
 * If it is "deletable" it gives the possibility to delete item.
 *
 * @author armand
 */
public abstract class CheckBoxItem extends Panel {

	
	public CheckBoxItem(String id, final ListItem<Filter> item) {
		super(id);
		
		// Add check box
		add( new AjaxCheckBox("active", new PropertyModel<Boolean>(item.getModel(), "active")) {

			@Override
			protected void onUpdate(AjaxRequestTarget target) {
				onItemSelected(target, item.getModelObject());
			}
		});
		
		// Add
		add(new Label("name", new TextFormaterPropertyModel(item.getModel(), "name", new StringFormater(18, true))));
		add(new AttributeModifier("title", true, new PropertyModel<String>(item.getModel(), "name")));

		Image removeImg = new Image("removeImg", Icons.DELETE);
		add(removeImg);
		if (item.getModelObject().getDeletable()) {
			removeImg.add(new AjaxEventBehavior("onclick") {
				@Override
				protected void onEvent(AjaxRequestTarget target) {
					onItemDeleted(target, item.getModelObject());
				}
			});

		} else {
			removeImg.setVisible(false);
		}
	}

	protected abstract void onItemSelected(AjaxRequestTarget target, Filter filter);
	
	protected abstract void onItemDeleted(AjaxRequestTarget target, Filter fitler);
}

