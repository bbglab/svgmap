/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.image.annotations;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.panel.Panel;

public class ActionPanelSearchIdModal extends Panel {

	public ActionPanelSearchIdModal(String id, final Integer idData, final String key) {
		super(id);

		//Add modal window
		final ModalWindow modal  = new ModalWindow("modal2");
		modal.setPageMapName("modal-1");
		modal.setCookieName("modal-1");
		modal.setInitialHeight(200);
		add(modal);
		
		AjaxLink lnk = new AjaxLink("link"){
			@Override
			public void onClick(AjaxRequestTarget target) {
				modal.setContent(new ModalAnnotationPanel(modal.getContentId(), modal, idData, key));
				modal.show(target);
			}
		};
		add(lnk);

	}
}
