package org.svgmapweb.ui.browser.toolbar.filter;

import org.svgmapweb.model.filters.Filter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.apache.wicket.Application;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.model.ProjectManager;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.BrowsePage;
import org.svgmapweb.ui.SvgmapSession;
import org.svgmapweb.ui.WicketApplication;

public class FiltersPanel extends Panel {

	private IModel listFilterModel;
	private SvgSearch search;

	public FiltersPanel(String id, final IModel<SvgSearch> searchModel) {

		super(id);

		try {

			setOutputMarkupId(true);

			search = searchModel.getObject();
			listFilterModel = new ListModel<Filter>(getListFilterSession());

			/* Add form*/
			Form form = new Form("form");
			add(form);

			//List of filters
			final ListView lFilters = new ListView<Filter>("filters", listFilterModel) {
				@Override
				protected void populateItem(final ListItem<Filter> item) {

					item.add(new CheckBoxItem("checkboxItem", item) {
						@Override
						public void onItemSelected(AjaxRequestTarget target, Filter filter) {
							if (filter.getActive())
								search.getExperimentFilters().add(filter);
							else
								search.getExperimentFilters().remove(filter);

							setResponsePage(new BrowsePage(new Model(search)));
						}

						@Override
						protected void onItemDeleted(AjaxRequestTarget target, Filter filter) {
							search.getExperimentFilters().remove(filter);
							deleteFilterFromSessionList(filter);

							setResponsePage(new BrowsePage(new Model(search)));
						}
					});
				}
			};
			lFilters.setOutputMarkupId(true);
			lFilters.setOutputMarkupPlaceholderTag(true);
			form.add(lFilters);

			// Add modal window
			final ModalWindow modal1 = new ModalWindow("modalWindowAddFilter") ;
			modal1.setOutputMarkupId(true);
			modal1.setOutputMarkupPlaceholderTag(true);
			modal1.setPageMapName("modal-1");
			modal1.setCookieName("modal-1");
			modal1.setTitle("Add filter");
			
			//Get list of unique headers
			List<String> tmpList = new ArrayList();
			tmpList.addAll(SvgmapUtils.readHeader(search.getExperiment().getIdData(), SvgmapConstants.TYPE_MAPPING));
			tmpList.addAll(SvgmapUtils.readHeader(search.getExperiment().getIdData(), SvgmapConstants.TYPE_ANNOTATION));
			final List<String> headers = new ArrayList(new HashSet(tmpList));

			modal1.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
				public void onClose(AjaxRequestTarget target) {

					//Update model object
					listFilterModel.setObject(getListFilterSession());

					target.addComponent(FiltersPanel.this);
				}
			});
			add(modal1);

			// Add modal button
			AjaxLink modalBtn = new AjaxLink("addFilter") {
				@Override public void onClick(AjaxRequestTarget target) {
					if (target == null)
						return;

					Integer idData = search.getExperiment().getIdData();				
					FilterAddPanel fAddPanel = new FilterAddPanel(modal1.getContentId(), headers, idData, modal1);
					modal1.setContent(fAddPanel);
					modal1.show(target);
				}
			};
			form.add(modalBtn);

			//Add modal window
			final ModalWindow modal  = new ModalWindow("modalHelp");
			modal.setPageMapName("modal-2");
			modal.setCookieName("modal-2");
			modal.setInitialHeight(200);
			ProjectManager projectManager = ((WicketApplication) Application.get()).getProjectManager();
			Label txtField = new Label(modal.getContentId(), projectManager.getProject().getHelp());
			txtField.setEscapeModelStrings(false);
			txtField.setVisible(true);
			modal.setTitle("Help");
			modal.setContent(txtField);
			add(modal);

			//Info link
			AjaxLink lnk = new AjaxLink("linkHelp"){
				@Override
				public void onClick(AjaxRequestTarget target) {
					modal.show(target);
				}
			};
			add(lnk);

		} catch (Exception ex) {
			error("Unable create panel");
		}
	}

	private void deleteFilterFromSessionList(Filter filter) {
		Integer idData = search.getExperiment().getIdData();

		SvgmapSession session = (SvgmapSession) WebSession.get();
		HashMap<String, List<Filter>> userFilters = session.getUserFilters().get(idData);

		if (userFilters != null) {
			List<Filter> filters = (userFilters.get("table") == null ? new ArrayList() : userFilters.get("table"));
			if (filters != null && !filters.isEmpty()) {
				filters.remove(filter);
				listFilterModel.setObject(filters);
			}
		}
	}

	private List<Filter> getListFilterSession() {
		List<Filter> filters = null;
		Integer idData = search.getExperiment().getIdData();

		SvgmapSession session = (SvgmapSession) WebSession.get();
		HashMap<String, List<Filter>> userFilters = session.getUserFilters().get(idData);

		if (userFilters != null)
			filters = (userFilters.get("table") == null ? new ArrayList() : userFilters.get("table"));

		return filters;
	}
}