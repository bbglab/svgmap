/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.ui.browser.image.scale;

import edu.upf.bg.color.utils.ColorUtils;
import edu.upf.bg.colorscale.AbstractColorScale;
import edu.upf.bg.colorscale.impl.PValueColorScale;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.svgmapweb.model.scales.IScalePanel;
import org.svgmapweb.model.utils.SvgmapUtils;

import java.awt.*;


public class PvalueScalePanel extends Panel implements IScalePanel{

	PValueColorScale scale;

	TextField significanceLevel;
	IModel<String> maxColorModel;
	IModel<String> minColorModel;
	IModel<String> nonSignificantColorModel;	

	public PvalueScalePanel(String id, PValueColorScale scale) {
		super(id);

		this.scale = scale;

		significanceLevel = new TextField("significanceLevel",  new Model(scale.getSignificanceLevel()));
		add(significanceLevel);
		
		maxColorModel = new Model(ColorUtils.colorToHexHtml(Color.YELLOW));
		add( new ColorPickerPanel("maxColorCPicker", "maxColor", maxColorModel) );

		nonSignificantColorModel = new Model(ColorUtils.colorToHexHtml(Color.GRAY));
		add(new ColorPickerPanel("nsigColorCPicker", "nonSignificantColor", nonSignificantColorModel));

		minColorModel = new Model(ColorUtils.colorToHexHtml(scale.getMin().getColor()));
		add(new ColorPickerPanel("minColorCPicker", "minColor", minColorModel));
		
	}

	public String getPanelName() {
		return "P-value scale";
	}

	public AbstractColorScale getScale() {
		return scale;
	}

	public TextField getSignificanceLevel() {
		return significanceLevel;
	}

	public void setSignificanceLevel(TextField significanceLevel) {
		this.significanceLevel = significanceLevel;
	}

	public void resetScale(AbstractColorScale scale) throws Exception {
		this.scale = (PValueColorScale) scale;

		significanceLevel.setModelObject(this.scale.getSignificanceLevel());		
		maxColorModel.setObject(ColorUtils.colorToHexHtml(this.scale.getSigLevelPoint().getColor()));
		minColorModel.setObject(ColorUtils.colorToHexHtml(this.scale.getMin().getColor()));
		nonSignificantColorModel.setObject(ColorUtils.colorToHexHtml(this.scale.getNonSignificantColor()));
	}


	public void setScale(PValueColorScale scale) {
		this.scale = scale;
	}

	public void updateScale() throws Exception {
		Double nsig = Double.parseDouble(significanceLevel.getValue());

		if (nsig == null || nsig == Double.NaN)
			throw new WicketRuntimeException("Non significant value is not correctly defined");

		scale = new PValueColorScale(
				/*fmt.parse((String) significanceLevel.getModelObject()).doubleValue()*/
				nsig,
				Color.decode((String) minColorModel.getObject()),
				Color.decode((String) maxColorModel.getObject()),
				Color.decode((String) nonSignificantColorModel.getObject()));

	}

	public String getScaleValues() {
		StringBuilder str = new StringBuilder();

		str.append("Min: " +  scale.getMin().getValue());
		str.append(", Sig-level: " + SvgmapUtils.roundToDecimals(scale.getSignificanceLevel(), 3));
		str.append(", Max: " + scale.getMax().getValue());

		return str.toString();
	}
}
