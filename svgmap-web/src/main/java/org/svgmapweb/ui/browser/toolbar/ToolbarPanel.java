/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.toolbar;

import java.util.List;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.ui.browser.toolbar.export.ExportsPanel;
import org.svgmapweb.ui.browser.toolbar.filter.FiltersPanel;

public class ToolbarPanel extends Panel {

	public ToolbarPanel(String id, final IModel<SvgSearch> searchModel, final IModel<List<RegionDetails>> dataModel) {
		super(id);

		/*Filters*/
		FiltersPanel filters = new FiltersPanel("filters", searchModel);
		add(filters);
		
		/*Download data*/
		ExportsPanel exports = new ExportsPanel("exports", searchModel, dataModel);
		add(exports);

	}
}
