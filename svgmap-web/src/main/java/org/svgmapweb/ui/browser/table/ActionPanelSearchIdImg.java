/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.browser.table;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.SvgSearch;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.ui.BrowsePage;

public class ActionPanelSearchIdImg extends Panel {

	public ActionPanelSearchIdImg(String id,final String key, final SvgSearch search) {
		super(id);

		add(new Link<RegionDetails>("view") {
			@Override
			public void onClick() {

				search.setSelectedTab(SvgmapConstants.TAB_IMG);
				search.setSearchText(key);

				try {
					setResponsePage(new BrowsePage(new Model(search)));
				} catch (Exception ex) {
					throw new WicketRuntimeException("Error in link of gene table results", ex);
				}
			}
		});
	}
}
