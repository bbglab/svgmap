/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svgmapweb.ui.browser.image.modal;

import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.image.Image;
import org.svgmapweb.ui.browser.image.ResultSVGPanel;

public class ModalPNGPage extends WebPage {

	public ModalPNGPage(final ResultSVGPanel resultPanel, final ModalWindow window, String PNGPath, String nameLbl) {

		//super(id);

		setOutputMarkupPlaceholderTag(true);

		window.setTitle(nameLbl);
		window.setInitialHeight(600);
		window.setInitialWidth(480);

		/* View Image */
		Image imgSvg = new Image("svgImg", "/" + PNGPath);
		add(imgSvg);

	}

	@Override
	protected void onBeforeRender() {
		super.onBeforeRender();
	}
}