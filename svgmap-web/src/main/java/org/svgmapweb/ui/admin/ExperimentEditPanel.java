/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.admin;

import java.io.File;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.lang.Bytes;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.svgmapweb.db.SVGMapDBOperationsHandle;
import org.svgmapweb.model.wrappers.ArrayModel;
import org.svgmapweb.db.jpa.ExperimentDataType;
import org.svgmapweb.db.jpa.ExperimentDataTypePK;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.ExperimentFile;
import org.svgmapweb.db.jpa.controlers.ExperimentDataTypeJpaController;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.db.jpa.controlers.ExperimentFileJpaController;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.ui.AdminPage;
import org.svgmapweb.model.utils.FileUtils;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.ui.layout.InfoExpPanel;

/**
 * Form for EDIT / INSERT  Experiments
 */
public class ExperimentEditPanel extends Panel {

	public static final String DISPLAY_NAME = "name";
	private final String[] typeFile = {"svg", "mapping", "annotation"};
	private static final long serialVersionUID = 1L;
	private final Integer NUM_TYPE_FILES = 3;
	private final Double MAX_MB = 50.0;
	private DropDownChoice<ExperimentFile> nameFileCombo[] = new DropDownChoice[NUM_TYPE_FILES];
	private FileUploadField fileUpload[] = new FileUploadField[NUM_TYPE_FILES];
	private ExperimentFile fileNames[] = new ExperimentFile[NUM_TYPE_FILES];
	private TextField nameExp;
	private TextArea description;
	private String formType;

	public ExperimentEditPanel(final String id, final ExperimentDataView dataParam) {

		super(id);

		// Initialisation form variables
		add(new FeedbackPanel("feedbackBrowse"));

		if (dataParam.getIdData() == null) {
			formType = "insert";
		} else {
			formType = "edit";

			//fileNames = SVGMapDBOperationsHandle.getExperimentsFile(dataParam).toArray(fileNames);
			List<ExperimentFile> listExperimentFiles = ExperimentFileJpaController.findExperimentFileFromExperimentDataType(dataParam);
			if (listExperimentFiles != null) {
				fileNames = listExperimentFiles.toArray(fileNames);
				if (fileNames.length < NUM_TYPE_FILES) {
					ExperimentFile[] auxColumn = new ExperimentFile[NUM_TYPE_FILES];
					auxColumn = fileNames;
					fileNames = new ExperimentFile[NUM_TYPE_FILES];
					fileNames[0] = auxColumn[0];
					fileNames[1] = auxColumn[1];
				}
			}
		}

		//Form initialisation
		Form form = new Form("form");
		form.setMultiPart(true);
		form.setMaxSize(Bytes.megabytes(SvgmapConstants.MAX_MB));
		form.add(new Label("msg1", "Name"));
		form.add(new Label("msg2", "Description"));
		form.add(new Label("msg3", "SVG file"));
		form.add(new Label("msg4", "Search SVG file"));
		form.add(new Label("msg5", "Data file"));
		form.add(new Label("msg6", "Search data file"));
		form.add(new Label("msg7", "Annotation file"));
		form.add(new Label("msg8", "Search annotation file"));

		add(new InfoExpPanel("expInfo", (formType.equals("insert") ? "Add new experiment" : "Edit Experiment")));

		description = new TextArea("description", new PropertyModel(dataParam, "description"));
		nameExp = new TextField("name", new PropertyModel(dataParam, "name"), String.class);
		nameExp.setRequired(true);

		form.add(nameExp);
		form.add(description);

		//CLEAN FILES NOT LINKED TO EXPERIMENTS
		SVGMapDBOperationsHandle.cleanNotLinkedFiles();

		//SVG FILE
		ExperimentFile aux = new ExperimentFile();

		List<ExperimentFile> listFile = SVGMapDBOperationsHandle.getAllFilesByType("svg");
		ChoiceRenderer choiceRenderer = new ChoiceRenderer(DISPLAY_NAME, SvgmapConstants.keysExperimentFile[0]/*aux.getKeys()[0]*/);
		if (listFile != null) {
			nameFileCombo[0] = new DropDownChoice("svgName", new ArrayModel<ExperimentFile>(fileNames, 0), listFile, choiceRenderer);
		}

		//MAPPING FILE
		listFile = SVGMapDBOperationsHandle.getAllFilesByType("mapping");
		choiceRenderer = new ChoiceRenderer(DISPLAY_NAME/*aux.getDisplay()*/, SvgmapConstants.keysExperimentFile[0]);
		if (listFile != null) {
			nameFileCombo[1] = new DropDownChoice("mappingName", new ArrayModel<ExperimentFile>(fileNames, 1), listFile, choiceRenderer);
		}

		//ANNOTATION FILE
		listFile = SVGMapDBOperationsHandle.getAllFilesByType("annotation");
		choiceRenderer = new ChoiceRenderer(DISPLAY_NAME, SvgmapConstants.keysExperimentFile[0]);
		if (listFile != null) {
			nameFileCombo[2] = new DropDownChoice("annotationName", new ArrayModel<ExperimentFile>(fileNames, 2), listFile, choiceRenderer);
		}

		form.add(nameFileCombo[0]);
		form.add(fileUpload[0] = new FileUploadField("svgFile"));
		form.add(nameFileCombo[1]);
		form.add(fileUpload[1] = new FileUploadField("mappingFile"));
		form.add(nameFileCombo[2]);
		form.add(fileUpload[2] = new FileUploadField("annotationFile"));



		form.add(new Button("uploadBtn") {

			@Override
			public void onSubmit() {
				try {

					if (missingFilesInForm(typeFile) || existFilesInDB(typeFile)) {
						error("Error: Missing input files");
						return;
					}

					saveExperimentAndInitComponents(dataParam);

					//Clean experimentFiles no linked with experiments
					if (!formType.equals("insert")) {
						SVGMapDBOperationsHandle.cleanNotLinkedFiles();
						((WicketApplication) Application.get()).resetExperimentCache(dataParam);
					}

					setResponsePage(new AdminPage(ExperimentsPanel.class));
				}
				catch(Exception ex) {
					error("Error: while creating experiment: " + ex.getMessage());
					return;
				}
			}
		});

		form.add(new Button("uploadConfigBtn") {

			@Override
			public void onSubmit() {
				try {
					if (missingFilesInForm() || existFilesInDB(typeFile)) {
						error("Error: Missing input files");
						return;
					}

					//Save the current experiment
					ExperimentDataView exp = saveExperimentAndInitComponents(dataParam);

					//Clean experimentFiles no linked with experiments
					SVGMapDBOperationsHandle.cleanNotLinkedFiles();

					setResponsePage(new AdminPage(ExperimentConfigPanel.class, exp));
				}
				catch(Exception ex) {
					error("Error: while creating experiment" + ex.getMessage());
					return;
				}
			}
		});


		form.add(new Button("cancelBtn") {
			@Override
			public void onSubmit() {
				setResponsePage(new AdminPage(ExperimentsPanel.class));
			}
			}.setDefaultFormProcessing(false)
		);

		add(form);
	}

	/**
	 * Desirable to split into a part to SVGMapDBOperationsHandle.
	 * @param dataParam
	 * @return
	 */
	private ExperimentDataView saveExperimentAndInitComponents(ExperimentDataView dataParam) throws Exception {
		File newFile = null;
		ExperimentFile idFile[] = new ExperimentFile[NUM_TYPE_FILES];
		EntityManager em = null;
		try {

			// ADD TRANSACTIONS!
			em = SVGMapDBOperationsHandle.getEntityManager();

			// Start Transaction
			em.getTransaction().begin();

			//Get date
			Date dateNow = new Date();
			//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			//StringBuilder dateStr = new StringBuilder(format.format(dateNow));
			//Insert Experiment View
			ExperimentDataView newExperiment = dataParam;
			newExperiment.setName((String) nameExp.getModelObject());
			newExperiment.setDescription((String) description.getModelObject());

			newExperiment.setModificationDate(dateNow);
			if (dataParam.getCreationDate() == null) {
				newExperiment.setCreationDate(dateNow);
			} else {

				newExperiment.setCreationDate(dataParam.getCreationDate());
			}


			if (formType.equals("insert")) {
				//newExperiment = (ExperimentDataView) SVGMapDBOperationsHandle.insertBeanDB((IExperimentSimpleEntity) newExperiment, em);
				newExperiment = ExperimentDataViewJpaController.create(newExperiment, em);
				newExperiment.setModificationDate(dateNow);
				newExperiment.setDefaultTabSelected("image");
				newExperiment.setPvalueFilter(0);
				newExperiment.setNumImages(1);
				newExperiment.setLinearScaleDefaults("outlier");
				newExperiment.setImageOptions("");
				newExperiment.setPvalueOptions("");
				newExperiment.setRelevantKeys("");

			} else {
				// new aprox
				ExperimentDataViewJpaController.edit(newExperiment, em);
				((WicketApplication) Application.get()).resetExperimentCache(dataParam);

			}

			//Insert Experiment files
			String ExperimentFolder = "temp" + newExperiment.getIdData();
			String ExperimentFolderShort = FileUtils.getUploadFolder() + File.separator + ExperimentFolder;
			String ExperimentFolderFull = FileUtils.createLoadFolder(ExperimentFolderShort).getPath();

			for (int i = 0; i < SvgmapConstants.NUM_TYPE_FILES; i++) {
				idFile[i] = new ExperimentFile();
				ExperimentFile exp = (ExperimentFile) nameFileCombo[i].getModelObject();
				FileUpload upload = fileUpload[i].getFileUpload();

				if (exp == null && upload == null) {
					//Allow create experiments without annotations
					if (!typeFile[i].equals("annotation")) {
						error("Upload failed, missing file/s");
					}
				} else {
					if (exp != null && upload == null) {
						idFile[i].setName(exp.getName());
						idFile[i].setIdFile(exp.getIdFile());
					} else {
						// Create a new file
						newFile = new File(ExperimentFolderFull, upload.getClientFileName());

						// Check if file already exists
						FileUtils.checkFileExists(newFile);

						// Save to new file
						newFile.createNewFile();
						upload.writeTo(newFile);

						idFile[i].setName(upload.getClientFileName());
						idFile[i].setPath(ExperimentFolder + File.separator + newFile.getName());

						//Insert path file DB
						//idFile[i] = (ExperimentFile) SVGMapDBOperationsHandle.insertBeanDB(idFile[i], em);
						ExperimentFileJpaController.create(idFile[i], em);
					}

					//Insert - Edit ExperimentDataTypes
					ExperimentDataType newExperimentDataTypes = new ExperimentDataType();
					newExperimentDataTypes.setExperimentDataTypePK(new ExperimentDataTypePK());
					newExperimentDataTypes.getExperimentDataTypePK().setIdData(newExperiment.getIdData());
					newExperimentDataTypes.getExperimentDataTypePK().setIdFile(idFile[i].getIdFile());
					newExperimentDataTypes.setType(typeFile[i]);

					//Get old idFile
					ExperimentDataView d = new ExperimentDataView();
					d.setIdData(dataParam.getIdData());

					ExperimentFile oldFile = ExperimentFileJpaController.findExperimentFileByType(d.getIdData(), typeFile[i], em);
					if (oldFile == null) {

						ExperimentDataTypeJpaController.create(newExperimentDataTypes, em);

					} else {
						if (!oldFile.getIdFile().equals(idFile[i].getIdFile())) {

							throw new Exception ("It is not possible to modify the selected files of the experiment. Please add a new experiment or delete this experiment");
							
							/* test1
							ExperimentDataType oldExperimentDataTypes = ExperimentDataTypeJpaController
									.findExperimentDataType(new ExperimentDataTypePK(
																	newExperiment.getIdData(),
																	oldFile.getIdFile()));
							ExperimentDataTypePK pk = oldExperimentDataTypes.getExperimentDataTypePK();
							pk.setIdFile(idFile[i].getIdFile());
							em.merge(pk);
							*/


							//oldExperimentDataTypes.getExperimentDataTypePK().setIdFile(idFile[i].getIdFile());
							//oldExperimentDataTypes.setExperimentFile(idFile[i]);
							//em.persist(oldExperimentDataTypes);
							//ExperimentDataTypeJpaController.edit(oldExperimentDataTypes, em);
							/* test 2
							//Find old data type
							ExperimentDataType oldExperimentDataTypes = null;
							oldExperimentDataTypes = ExperimentDataTypeJpaController.findExperimentDataType(new ExperimentDataTypePK(newExperiment.getIdData(),oldFile.getIdFile()));
							oldExperimentDataTypes = em.getReference(ExperimentDataType.class, oldExperimentDataTypes.getExperimentDataTypePK());
							oldExperimentDataTypes.getExperimentDataTypePK();


							//ExperimentDataType oldExperimentDataTypes = new ExperimentDataType();
							//oldExperimentDataTypes = new ExperimentDataType();
							oldExperimentDataTypes.setExperimentDataTypePK(new ExperimentDataTypePK());
							oldExperimentDataTypes.getExperimentDataTypePK().setIdData(newExperiment.getIdData());
							//old - oldExperimentDataTypes.getExperimentDataTypePK().setIdFile(oldFile.getIdFile());
							oldExperimentDataTypes.getExperimentDataTypePK().setIdFile(idFile[i].getIdFile());
							oldExperimentDataTypes.setType(typeFile[i]);
							oldExperimentDataTypes.setExperimentFile(null);
							oldExperimentDataTypes.setExperimentDataView(null);

							ExperimentDataTypeJpaController.edit(oldExperimentDataTypes, em);
							*/
						}
					}
				}

			}

			em.getTransaction().commit();
			return newExperiment;
			
		} catch(Exception ex) {
			em.getTransaction().rollback();
			ex.printStackTrace();
			throw new WicketRuntimeException(ex.getMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	/**
	 * Method for checking if there are input files missing in the form
	 * @return Boolean
	 */
	private boolean missingFilesInForm(String[] typeFile) {

		for (int i = 0; i < NUM_TYPE_FILES - 1; i++) {
			if (nameFileCombo[i].getModelObject() == null
					&& (fileUpload[i].getFileUpload() == null || fileUpload[i].getFileUpload().equals(""))) {
				error("File '" + typeFile[i] + "' is missing");
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to check if a file doesn't exist in DB already
	 * @param typeFile
	 * @return
	 */
	private boolean existFilesInDB(String[] typeFile) {

		for (int i = 0; i < NUM_TYPE_FILES; i++) {
			if (fileUpload[i].getFileUpload() != null && !fileUpload[i].getFileUpload().equals("")) {

				for (ExperimentFile e : ExperimentFileJpaController.findExperimentFileByTypeCollection(typeFile[i])) {
					if (fileUpload[i].getFileUpload().getClientFileName().equals(e.getName())) {
						error("File " + fileUpload[i].getFileUpload().getClientFileName()
								+ " already exist in DB. "
								+ " Select this file from the " + typeFile[i] + " list or rename it in local disk.");
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Method for checking if there are input files missing in the form
	 * @return Boolean
	 */
	private boolean missingFilesInForm() {

		for (int i = 0; i < NUM_TYPE_FILES - 1; i++) {
			if (nameFileCombo[i].getModelObject() == null
					&& (fileUpload[i].getFileUpload() == null || fileUpload[i].getFileUpload().equals(""))) {
				error("File '" + typeFile[i] + "' is missing");
				return true;
			}
		}
		return false;
	}
}
