/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.wicket.Application;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.NumberValidator;
import org.gitools.matrix.model.MatrixView;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.db.SVGMapDBOperationsHandle;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.model.wrappers.SelectOption;
import org.svgmapweb.model.wrappers.ListOptionModel;
import org.svgmapweb.ui.AdminPage;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.ui.layout.InfoExpPanel;

/**
 * Form for EDIT / INSERT  Experiments
 */

public class ExperimentConfigPanel extends Panel {

	private static final long serialVersionUID = 1L;

	private class choiceModel implements IModel<String> {

		private IModel<Integer> model;

		public choiceModel(IModel<Integer> m) {
			this.model = m;
		}

		public String getObject() {
			if (model.getObject() == 1)
				return "yes";
			else
				return "no";
		}

		public void setObject(String object) {
			if (object.equals("yes"))
				model.setObject(1);
			else
				model.setObject(0);
		}

		public void detach() {
		}
	}

	private TextField nameExp;
	private TextField<Integer> numImg;
	private TextField<String> keyExternalLink;
	private RadioChoice<String> defaultTabSelected;
	private ListMultipleChoice imgOptions;
	private ListMultipleChoice scaleOption;
	private TextArea examples;

	public ExperimentConfigPanel(final String id, final ExperimentDataView dataParam) {
		super(id);

		try {
			//Form initialisation
			Form form = new Form("form");
			form.add(new Label("msg1", "Experiment name"));
			form.add(new Label("msg2", "Num images"));
			form.add(new Label("msg3", "External link for each key"));
			form.add(new Label("msg4", "Image option values"));
			form.add(new Label("msg7", "Scale options"));
			form.add(new Label("msg9", "Tab selected by default"));
			form.add(new Label("msg10", "Text for relevant examples in the browser"));

			add(new InfoExpPanel("expInfo", "Edit experiment configuration"));

			nameExp = new TextField("name", new PropertyModel(dataParam, "name"));
			nameExp.setEnabled(false);
			numImg = new TextField("numImg", new PropertyModel(dataParam, "numImages"));
			numImg.setRequired(true);
			numImg.add(NumberValidator.minimum(1));

			keyExternalLink = new TextField("geneLink", new PropertyModel(dataParam, "geneLink"));
			examples = new TextArea("examples", new PropertyModel(dataParam, "relevantKeys"));

			defaultTabSelected	= new RadioChoice("tabSelected",
					new PropertyModel(dataParam, "defaultTabSelected"),
					Arrays.asList("table", "image"));
			defaultTabSelected.setRequired(true);

			//Options
			MatrixView mappingMatrix = ((WicketApplication) Application.get()).getMappingMatrix(dataParam.getIdData());

			ChoiceRenderer choiceRenderer = new ChoiceRenderer("value", "key");
			List<SelectOption> allPvalueOptions = new ArrayList();
			List<SelectOption> allImageOptions = new ArrayList();
			SvgmapUtils.loadOptionsFromMappingFile(mappingMatrix, allImageOptions, allPvalueOptions, true);

			imgOptions = new ListMultipleChoice("imageOptions",
					new ListOptionModel(new PropertyModel(dataParam, "imageOptions")),
					allImageOptions, choiceRenderer);
			imgOptions.setRequired(true);

			/* Add scale field */
			SelectOption[] scales = new SelectOption[] {
				new SelectOption("linear", "linear"),
				new SelectOption("linear2sided", "linear two sided"),
				new SelectOption("pvalue", "pvalue")};

			scaleOption = new ListMultipleChoice("scaleOptions",
					new ListOptionModel(new PropertyModel(dataParam, "scaleOptions")),
					Arrays.asList(scales), choiceRenderer);

			form.add(new Button("uploadBtn") {
				@Override public void onSubmit() {
					try {
						if (! validForm())
							error ("Review input form data");

						ExperimentDataView oldExperiment = new ExperimentDataView();
						oldExperiment.setIdData(dataParam.getIdData());
						oldExperiment.setName(dataParam.getName());

						Date dateNow = new Date();
						//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						//StringBuilder dateStr = new StringBuilder(format.format(dateNow));
						ExperimentDataView newExperiment = new ExperimentDataView();
						newExperiment.setIdData(dataParam.getIdData());
						newExperiment.setName(nameExp.getDefaultModelObjectAsString());
						newExperiment.setDescription(dataParam.getDescription());
						newExperiment.setModificationDate(dateNow);
						newExperiment.setRelevantKeys(examples.getDefaultModelObjectAsString());
						//newExperiment.setModificationDate(dateStr + "");

						if (dataParam.getCreationDate() == null) {
							//newExperiment.setCreationDate(dateStr + "");
							newExperiment.setCreationDate(dateNow);
						}
						else newExperiment.setCreationDate(dataParam.getCreationDate());

						newExperiment.setNumImages(numImg.getModelObject());
						newExperiment.setGeneLink(keyExternalLink.getDefaultModelObjectAsString());
						newExperiment.setDefaultTabSelected(defaultTabSelected.getDefaultModelObjectAsString());

						//FIXME: Q&D
						newExperiment.setImageOptions(optionsToString((List<SelectOption>) imgOptions.getModelObject()));
						newExperiment.setScaleOptions(optionsToString((List<SelectOption>) scaleOption.getModelObject()));

						// To edit this experiment the newExperiment must have a files svg, mapping and annotation selected.
						if (!SVGMapDBOperationsHandle.consistentExperiment(newExperiment)) {
							error("This experiment is deleted. Is no possible to edit state.");
						} else {
							ExperimentDataViewJpaController.edit(newExperiment);
						}

						//Recomputing the scale ranges
						((WicketApplication) Application.get()).setMappingMatrixRanges(newExperiment.getIdData());
						
						setResponsePage(new AdminPage(ExperimentsPanel.class));
					} catch (Exception e) {
						e.printStackTrace();
						error("Error while updating experiment configuration. Description:" + e.getMessage());
					}
					}
				});

			form.add(new Button("cancelBtn") {

				@Override
				public void onSubmit() {
					setResponsePage(new AdminPage(ExperimentsPanel.class));
				}
			}.setDefaultFormProcessing(false));

			form.add(keyExternalLink);
			form.add(imgOptions);
			form.add(scaleOption);
			form.add(nameExp);
			form.add(numImg);
			form.add(defaultTabSelected);
			form.add(examples);

			add(form);
			add(new FeedbackPanel("feedback"));

		} catch (Exception e) {
			e.printStackTrace();
			error("Error while updating experiment configuration. Description:" + e.getMessage());
		}
	}

	private String optionsToString(List<SelectOption> modelObject) {
		String res = "";

		for (SelectOption s : modelObject)
			res += ";" + s.getKey();

		if (res.isEmpty())
			return res;
		else
			return res.substring(1);
	}

	private boolean validForm() {
		Integer n = numImg.getModelObject();

		if (n == null || n < 1) {
			error("Number of images can not be empty or less than 0");
			return false;
		}
		String link = keyExternalLink.getModelObject();
		if (link != null && !link.contains(SvgmapConstants.CHR_LNK)) {
			error("Link does not contain the symbol where to add the id of the keyword");
			return false;
		}

		return true;
	}
}

