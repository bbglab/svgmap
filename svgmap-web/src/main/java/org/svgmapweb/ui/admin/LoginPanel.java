/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.admin;

import org.apache.wicket.Application;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.model.ProjectManager;
import org.svgmapweb.ui.AdminPage;
import org.svgmapweb.ui.SvgmapSession;
import org.svgmapweb.ui.WicketApplication;

public class LoginPanel extends Panel {

    private static final long serialVersionUID = 1L;

	protected TextField login;
	protected PasswordTextField pwd;

    public LoginPanel(String id) {

		super(id);

		final Form form = new Form("form");

		form.add(new Label("msg1", "Login"));
		form.add(new Label("msg2", "Password"));

		login = new TextField("login", new Model(""));
		pwd = new PasswordTextField("pwd", new Model(""));

		form.add(login);
		form.add(pwd);

		form.add( new Button("acceptBtn") {
			@Override
			public void onSubmit()
			{
				try {
					ProjectManager projManager =((WicketApplication) Application.get()).getProjectManager();

					if (projManager.getProject().getLogin().equalsIgnoreCase(login.getModel().getObject().toString())
							&& projManager.getProject().getPwd().equalsIgnoreCase(pwd.getModel().getObject().toString())) {
						SvgmapSession session = (SvgmapSession) WebSession.get();
						session.setUserValidated(true);
						setResponsePage(new AdminPage(ExperimentsPanel.class));

					} else {
						error("Incorrect user access");
						setResponsePage(AdminPage.class);
					}

				} catch (Exception ex) {
					error("Problems during experiments loading");
				}
			}
		 });

		form.add( new Button("cancelBtn") {
			@Override
			public void onSubmit()
			{
				setResponsePage(AdminPage.class);
			}
		 });
		add(form);
		add(new FeedbackPanel("feedbackBrowse"));

    }
}
