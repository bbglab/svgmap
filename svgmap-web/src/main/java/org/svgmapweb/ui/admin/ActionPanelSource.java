/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.admin;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.apache.wicket.Application;
import org.apache.wicket.behavior.SimpleAttributeModifier;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.db.SVGMapDBOperationsHandle;
import org.svgmapweb.db.jpa.controlers.exceptions.NonexistentEntityException;
import org.svgmapweb.ui.AdminPage;
import org.svgmapweb.ui.WicketApplication;

public class ActionPanelSource extends Panel {

	public ActionPanelSource(String id, final IModel<ExperimentDataView> model) {
		super(id, model);

		add(new Link<ExperimentDataView>("configure", model) {

			@Override
			public void onClick() {
				setResponsePage(new AdminPage(ExperimentConfigPanel.class, model.getObject()));
			}
		});

		Link lnk = new Link<ExperimentDataView>("delete", model) {

			@Override
			public void onClick() {

				// Transaction
				EntityManager em = SVGMapDBOperationsHandle.getEntityManager();
				try {
					em.getTransaction().begin();

					//SVGMapDBOperationsHandle.deleteExperiment(model.getObject(), em);
					ExperimentDataViewJpaController.destroy(model.getObject().getIdData());

					SVGMapDBOperationsHandle.cleanNotLinkedFiles();
					((WicketApplication) Application.get()).resetExperimentCache(model.getObject());

					em.getTransaction().commit();

				} catch (NonexistentEntityException ex) {
					em.getTransaction().rollback();
					ex.printStackTrace();
					Logger.getLogger(ActionPanelSource.class.getName()).log(Level.SEVERE, null, ex);

				} finally {					
					if (em != null) {
						em.close();
					}
				}
				setResponsePage(new AdminPage(ExperimentsPanel.class));
			}
		};
		add(lnk);
		lnk.add(new SimpleAttributeModifier("onclick", "return confirm('Are you sure to delete the selected experiment?');"));

		add(new Link<ExperimentDataView>("edit", model) {

			@Override
			public void onClick() {
				setResponsePage(new AdminPage(ExperimentEditPanel.class, model.getObject()));
			}
		});
	}
}
