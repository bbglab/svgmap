/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui.admin;

import java.util.Iterator;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.svgmapweb.db.jpa.ExperimentDataView;

public class SortableExperimentsDataProvider extends SortableDataProvider<ExperimentDataView> {

	List<ExperimentDataView> data;

	public SortableExperimentsDataProvider(List<ExperimentDataView> experiments) {
		data = experiments;
	}

    /**
     * @see org.apache.wicket.markup.repeater.data.IDataProvider#size()
     */
    public int size() {
        if (data == null)
			return 0;
		
		return data.size();
    }


	public Iterator<ExperimentDataView> iterator(int first, int count) {
		return data.listIterator(first);
	}

	public IModel<ExperimentDataView> model(ExperimentDataView object) {
		return new Model(object);
	}

}
