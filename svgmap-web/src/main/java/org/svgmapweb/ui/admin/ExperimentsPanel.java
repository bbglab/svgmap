/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svgmapweb.ui.admin;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DefaultDataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.ui.AdminPage;


public class ExperimentsPanel extends Panel {

	public ExperimentsPanel(String id) throws Exception {
		super(id);

		/* add new experiment */

		//List<ExperimentDataView> experiments = SVGmapDBOperations.getAllBeans("ExperimentDataView", ExperimentDataView.class);
		List<ExperimentDataView> experiments = ExperimentDataViewJpaController.findExperimentDataViewEntities();
		add(new Link<ExperimentDataView>("new") {
			@Override
			public void onClick() {
				setResponsePage(new AdminPage(ExperimentEditPanel.class, new ExperimentDataView()));
			}
		});

		/* Add columns in summary table */
		List<IColumn<?>> columnsTbl = new ArrayList<IColumn<?>>();
		columnsTbl.add(new PropertyColumn(new Model<String>("Name"), "name"));
		columnsTbl.add(new PropertyColumn(new Model("Description"),"description") {
            @Override
            protected IModel createLabelModel(final IModel embeddedModel) {
                return new Model() {
                    @Override
                    public Serializable getObject() {
                        PropertyModel pm = new PropertyModel(embeddedModel,
															getPropertyExpression());
                        return mask((String) pm.getObject());
                    }

                    private Serializable mask(String desc) {
						if (desc != null && !desc.isEmpty()) {
							int end = desc.length() > 30 ? 30 : desc.length();
							String extension = desc.length() > 30 ? "..." : "";
							return desc.substring(0,end) + extension;
						} else
							return desc;
                    }
                };
            }
        });
		columnsTbl.add(new PropertyColumn(new Model<String>("Creation date"), "creationDate"){
			@Override
            protected IModel createLabelModel(final IModel embeddedModel) {
                return new Model() {
                    @Override
                    public Serializable getObject() {
                        PropertyModel pm = new PropertyModel(embeddedModel,
															getPropertyExpression());

                        return mask((Date) pm.getObject());
                    }

					@Override
					public void setObject(Serializable object) {
						super.setObject(object);
					}
                    private Serializable mask(Date date) {

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						StringBuilder dateStr = new StringBuilder(format.format(date));
                        return dateStr.substring(0,10); // yyyy-MM-dd
                    }
                };
            }
		});
		columnsTbl.add(new PropertyColumn(new Model<String>("Last modification"), "modificationDate"){
			@Override
            protected IModel createLabelModel(final IModel embeddedModel) {
                return new Model() {
                    @Override
                    public Serializable getObject() {
                        PropertyModel pm = new PropertyModel(embeddedModel,
															getPropertyExpression());
                        return mask((Date) pm.getObject());
                    }

					@Override
					public void setObject(Serializable object) {
						super.setObject(object);
					}


                    private Serializable mask(Date date) {

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						StringBuilder dateStr = new StringBuilder(format.format(date));
                        return dateStr.substring(0,10); // yyyy-MM-dd
                    }
                };


            }
		});
		columnsTbl.add(new AbstractColumn<ExperimentDataView>(new Model<String>("Operations")) {
			public void populateItem(Item<ICellPopulator<ExperimentDataView>> cellItem, String type, IModel<ExperimentDataView> model) {
				cellItem.add(new ActionPanelSource(type, model));
			}
		});

		add(new DefaultDataTable("tableSummary", columnsTbl, new SortableExperimentsDataProvider(experiments), 10));
		add(new Label("tableName", "Experiments"));
	}
}
