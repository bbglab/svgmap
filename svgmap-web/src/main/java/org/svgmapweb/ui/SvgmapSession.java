/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.Request;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.model.scales.ColorScaleRangeExt;
import org.svgmapweb.model.filters.Filter;

public class SvgmapSession extends WebSession {

	private boolean userValidated = false;
	private Boolean svgSupported = null;

	//idData + scale + range type
	private Map<Integer, HashMap<String, String>> userScaleRangeType = new HashMap<Integer, HashMap<String, String>>();

	//idData + scale + range value
	private Map<Integer, HashMap<String, ColorScaleRangeExt>> userScaleRange = new HashMap<Integer, HashMap<String, ColorScaleRangeExt>>();

	//idData + typeTab + filters
	private Map<Integer, HashMap<String, List<Filter>>> userFilters = new HashMap<Integer, HashMap<String, List<Filter>>>();

	public SvgmapSession(Request request) {
		super(request);
	}

	public boolean isUserValidated() {
		return userValidated;
	}

	public void setUserValidated(boolean userValidated) {
		this.userValidated = userValidated;
	}

	public Map<Integer, HashMap<String, String>> getScaleDefaultRangeType() {
		return userScaleRangeType;
	}

	public void setScaleDefaultRangeType(Map<Integer, HashMap<String, String>> LinearScaleDefaultValues) {
		this.userScaleRangeType = LinearScaleDefaultValues;
	}

	public Map<Integer, HashMap<String, ColorScaleRangeExt>> getScaleUserRange() {
		return userScaleRange;
	}

	public void setScaleUserRange(Map<Integer, HashMap<String, ColorScaleRangeExt>> mappingsMatrixUserRanges) {
		this.userScaleRange = mappingsMatrixUserRanges;
	}

	public Map<Integer, HashMap<String, List<Filter>>> getUserFilters() {
		return userFilters;
	}

	public void setUserFilters(Map<Integer, HashMap<String, List<Filter>>> userFilters) {
		this.userFilters = userFilters;
	}

	public Boolean isSvgSupported() {
		return svgSupported;
	}

	public void setSvgSupported(Boolean svgSupported) {
		this.svgSupported = svgSupported;
	}
}
