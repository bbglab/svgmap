/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that wraps mapping and annotation files
 * 
 * @author xavier
 */
public class RegionDetails implements java.io.Serializable {

	String region;
	String id;
	String name;
	String link;
	List<String> values = new ArrayList<String>(); // mapping file only value fields, without : region, id

	public String getId() {
		return id;
	}

	public void setId(String genes) {
		this.id = genes;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String regions) {
		this.region = regions;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String geneLink) {
		this.link = geneLink;
	}

	public String getName() {
		return name;
	}

	public void setName(String geneName) {
		this.name = geneName;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final RegionDetails other = (RegionDetails) obj;
		if ((this.region == null) ? (other.region != null) : !this.region.equals(other.region)) {
			return false;
		}
		if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
			return false;
		}
		if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
			return false;
		}
		if ((this.link == null) ? (other.link != null) : !this.link.equals(other.link)) {
			return false;
		}
		if (this.values != other.values && (this.values == null || !this.values.equals(other.values))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 71 * hash + (this.region != null ? this.region.hashCode() : 0);
		hash = 71 * hash + (this.id != null ? this.id.hashCode() : 0);
		hash = 71 * hash + (this.name != null ? this.name.hashCode() : 0);
		hash = 71 * hash + (this.link != null ? this.link.hashCode() : 0);
		hash = 71 * hash + (this.values != null ? this.values.hashCode() : 0);
		return hash;
	}
	
}
