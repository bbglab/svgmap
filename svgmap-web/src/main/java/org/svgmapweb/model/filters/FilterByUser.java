/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.filters;

import cern.colt.matrix.ObjectMatrix2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.browser.toolbar.filter.FilterOperator;


public class FilterByUser extends AbstractFilter {

	private final ObjectMatrix2D annotationMatrix;

	public FilterByUser(ObjectMatrix2D annotationMatrix) {
		this.annotationMatrix = annotationMatrix;
	}

	/**
	 * Search regions where exists the keyword and their values
	 * @param columnIndex : pvalue column
	 * @param key : gene to search
	 * @param data : matrix
	 * @return
	 */
	public Map<String, List<RegionDetails>> filter(Map<String, List<RegionDetails>> mapDetails, Filter flt, List<String> hdrMapp, List<String> hdrAnn) throws Exception {
		try {

			int columnIndex = 0;
			boolean found = false;
			String value = "";

			//Get filter values
			List<String> keyList = SvgmapUtils.parseKeywordsToList(flt.getValues());
			FilterOperator operator = flt.getOperation();

			//Search in Mapping columns
			for (String col : hdrMapp) {
				if (!found && col.equals(flt.getColumn())) {
					found = true;
					List<RegionDetails> lDetails = SvgmapUtils.mergeLists(mapDetails.values());

					for (RegionDetails row : lDetails) {
						//For hdrMapp
						switch (columnIndex) {
							case 0: value = row.getRegion(); break;
							case 1: value = row.getId(); break;
							default:
								value = row.getValues().get(columnIndex-2);
						}
						if (!checkRow(operator, keyList, value)) {

							//Add new details object
							List<RegionDetails> listRows = mapDetails.get(row.getId());
							if (listRows != null && !listRows.isEmpty())
								listRows.remove(row);
						}
					}
				}
				columnIndex++;
			}

			//Search in Annotation columns
			if (!found && hdrAnn != null && !hdrAnn.isEmpty()
				&& annotationMatrix != null && annotationMatrix.size() > 0) {

				columnIndex = 0;
				String idAnn = "";

				//Remove rows without annotations in case operation is not : empty
				if (operator.getOperatorIndex() != 'e')
					mapDetails = removeRowsWithoutAnn(mapDetails);

				for (String col : hdrAnn) {
					if (columnIndex > 0 && !found && col.equals(flt.getColumn())) {
						found = true;
						for (int j = 0; j < annotationMatrix.rows(); j++) {
							idAnn = (String) annotationMatrix.viewRow(j).get(0);
							value = (String) annotationMatrix.viewRow(j).get(columnIndex);
							if (mapDetails.get(idAnn) == null || mapDetails.get(idAnn).isEmpty()
									|| !checkRow(operator, keyList, value))
								mapDetails.remove(idAnn);
						}
					}
					columnIndex++;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error in the format of the filter " + flt.getName());
		}
		return mapDetails;
	}

	private boolean checkRow(FilterOperator operator, List<String> filterValues, String currentValue) throws NumberFormatException {
		try {

			if (currentValue == null || currentValue.isEmpty())
				if (operator.getOperatorIndex() == 'e')
					return true;
				else
					return false;
			else {
				switch (operator.getOperatorIndex()) {
					case 'n': {
								return true;
					}
					case 'g': {
							if (new Double(currentValue) > new Double(filterValues.get(0)))
								return true;
							break;
					}
					case 'G': {
							if (new Double(currentValue) >= new Double(filterValues.get(0)))
								return true;
							break;
					}
					case 'm': {
							if (new Double(currentValue) < new Double(filterValues.get(0)))
								return true;
							break;
					}
					case 'M': {
							if (new Double(currentValue) <= new Double(filterValues.get(0)))
								return true;
							break;
					}
					case '=': {
							for (String val : filterValues) {
								if (currentValue.equals(val))
									return true;
							}
							break;
					}
					case 'i': {
							for (String val : filterValues) {
								if (currentValue.equalsIgnoreCase(val))
									return true;
							}
							break;
					}
					case 'c': {
							for (String val : filterValues) {
								if (currentValue.contains(val))
									return true;
							}
					}
					case 's': {
							for (String val : filterValues) {
								if (currentValue.startsWith(val))
									return true;
							}
							break;
					}
				}
			}

		} catch (Exception ex) {}

		return false;
	}

	private Map<String, List<RegionDetails>> removeRowsWithoutAnn(Map<String, List<RegionDetails>> mapDetails) {

		String[] annIds = new String[annotationMatrix.viewColumn(0).size()];
		annotationMatrix.viewColumn(0).toArray(annIds);

		List<String> annListIds = Arrays.asList(annIds);
		List<String> keysToRemove = new ArrayList();

		for (String key : mapDetails.keySet()) {
			if (!annListIds.contains(key))
				keysToRemove.add(key);
		}

		for (String key : keysToRemove)
			mapDetails.remove(key);

		return mapDetails;
	}
}
