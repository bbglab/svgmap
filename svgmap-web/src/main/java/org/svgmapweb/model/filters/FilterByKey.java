/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.filters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.Application;
import org.gitools.matrix.model.MatrixView;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.model.utils.SvgmapUtils;
import org.svgmapweb.ui.WicketApplication;


public class FilterByKey extends AbstractFilter {

	private Integer idData;
	private MatrixView mappingMatrix;

	public FilterByKey(Integer idData, MatrixView mappingMatrix) {
		this.idData = idData;
		this.mappingMatrix = mappingMatrix;
	}

	/**
	 * Search regions where exists the key and their values
	 * @param keyList : keys to search
	 * @return
	 */
	public Map<String, List<RegionDetails>> filter(List<String> keyList) throws Exception {

		String key;
		RegionDetails details;
		Map<String, List<RegionDetails>> map = new HashMap<String, List<RegionDetails>>();
		Map<String, String> expData = ((WicketApplication) Application.get()).getKeyNameAnnotationsMap(idData);

		try {

			for (int i = 0; i < mappingMatrix.getRowCount(); i++) {
				key = (String) mappingMatrix.getRowLabel(i);

				//get key row
				if (keyList == null || keyList.isEmpty() || SvgmapUtils.containsIgnoreCase(keyList, key)) {

					for (int j = 0; j < mappingMatrix.getColumnCount(); j++) {

						//Get region where exist key gene
						if (mappingMatrix.getCell(i, j) != null) {

							details = new RegionDetails();
							details.setId(key);
							details.setRegion((String) mappingMatrix.getColumnLabel(j));

							if (expData != null && ! expData.isEmpty())
								details.setName(expData.get(key));

							//Add mapping values to details
							addMappingValuesFields(i, j, details);

							//Add new details object
							List<RegionDetails> listRows = map.get(key);
							if (listRows == null) {
								listRows = new ArrayList<RegionDetails>();
								map.put(key, listRows);
							}
							listRows.add(details);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new Exception("Error building table results");
		}
		return map;
	}

	private void addMappingValuesFields(int i, int j, RegionDetails details) {

		double[] data = (double[]) mappingMatrix.getCell(i, j);
		List<String> values = new ArrayList<String>(0);
		for (Double val : data) {
			if (val != null && !val.isNaN()) {
				values.add(val + "");
			} else {
				values.add("");
			}
		}
		details.setValues(values);
	}

}
