/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.svgmapweb.model.filters;

import java.io.Serializable;
import java.util.UUID;
import org.svgmapweb.ui.browser.toolbar.filter.FilterOperator;

/**
 *
 * @author xavi
 */
public class Filter implements Serializable {

	private String id;
	private String name = "filter_1";
	private String column = "";
	private FilterOperator operation;
	private String values = "";
	private Boolean active = false;
	private Boolean hidden = false;
	private Boolean deletable = true;

	public Filter() {
		id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getDeletable() {
		return deletable;
	}

	public void setDeletable(Boolean deleteable) {
		this.deletable = deleteable;
	}

	public Boolean getHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public FilterOperator getOperation() {
		return operation;
	}

	public void setOperation(FilterOperator operation) {
		this.operation = operation;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}
}
