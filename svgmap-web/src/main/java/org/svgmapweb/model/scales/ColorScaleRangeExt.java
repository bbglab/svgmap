/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.scales;

import edu.upf.bg.colorscale.ColorScaleRange;



public class ColorScaleRangeExt extends ColorScaleRange {
	private double mid;

	public ColorScaleRangeExt(double min, double max) {
		super(min, max);
		this.mid = 0;
	}

	public ColorScaleRangeExt(double min, double mid, double max) {
		super(min, max);
		this.mid = mid;
	}

	public double getMid() {
		return mid;
	}

	public void setMid(double mid) {
		this.mid = mid;
	}
}
