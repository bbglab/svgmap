/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.scales;

import edu.upf.bg.colorscale.ColorScalePoint;
import edu.upf.bg.colorscale.impl.LinearColorScale;
import edu.upf.bg.colorscale.impl.LinearTwoSidedColorScale;
import edu.upf.bg.colorscale.impl.PValueColorScale;
import java.awt.Color;
import java.text.ParseException;
import java.util.HashMap;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.protocol.http.WebSession;
import org.svgmapweb.db.jpa.ExperimentDataView;
import org.svgmapweb.db.jpa.ExperimentFile;
import org.svgmapweb.db.jpa.controlers.ExperimentDataViewJpaController;
import org.svgmapweb.model.utils.SvgmapConstants;
import org.svgmapweb.ui.SvgmapSession;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.ui.browser.image.scale.Linear2SidedScalePanel;
import org.svgmapweb.ui.browser.image.scale.LinearScalePanel;
import org.svgmapweb.ui.browser.image.scale.PvalueScalePanel;

public class ScalePanelFactory {

	public static IScalePanel createScalePanel(String panelType, ExperimentDataView data) throws Exception {
		IScalePanel panel = null;
		ExperimentFile mapping = ((WicketApplication) WicketApplication.get()).getExperimentFile(data.getIdData(), "mapping");
		if (panelType.equals(SvgmapConstants.LINEAR))
			panel = createLinearScalePanel(data.getIdData(), mapping);

		if (panelType.equals(SvgmapConstants.LINEAR2SIDED))
			panel = createLinear2SidedScalePanel(data.getIdData(), mapping);

		if (panelType.equals(SvgmapConstants.PVALUE))
			panel = createPvalueScalePanel(mapping);

		return panel;
	}

	public static IScalePanel createLinearScalePanel(Integer idData, ExperimentFile mapping) throws Exception{
		LinearScalePanel panel = null;
		ColorScaleRangeExt range = null;
		String defaultLinearScaleValues = null;
		try {
			SvgmapSession session = (SvgmapSession) WebSession.get();
			HashMap<String, String> scaleRangesExp = session.getScaleDefaultRangeType().get(idData);

			if (scaleRangesExp == null || scaleRangesExp.get(SvgmapConstants.LINEAR) == null) {				
				ExperimentDataView experiment = ExperimentDataViewJpaController.findExperimentDataView(idData);
				defaultLinearScaleValues = experiment.getLinearScaleDefaults();

			} else
				defaultLinearScaleValues = session.getScaleDefaultRangeType().get(idData).get(SvgmapConstants.LINEAR);

			if (defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_USER))
				range = session.getScaleUserRange().get(idData).get(SvgmapConstants.LINEAR);

			if (defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_ALL_DATA) ||
					defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_WITHOUT_OUTLIER))
				range = ((WicketApplication) Application.get()).getMappingsMatrixRanges(idData).get(defaultLinearScaleValues);

			if (defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_KEY)) {
				range = new ColorScaleRangeExt(0, 0);
			}

			LinearColorScale scale = new LinearColorScale(
						new ColorScalePoint(Double.parseDouble(range.getMin() + ""), Color.YELLOW),
						new ColorScalePoint(Double.parseDouble(range.getMax() + ""), Color.RED));

			panel = new LinearScalePanel(SvgmapConstants.LINEAR, scale);

			panel.setDefaultValueType(defaultLinearScaleValues);
			panel.setExperimentId(idData);
			panel.resetScale(scale);

			return panel;
		} catch (Exception e) {
			throw new WicketRuntimeException("Impossible to create linear scale panel.", e);
		}
	}

	public static IScalePanel createPvalueScalePanel(ExperimentFile mapping) throws ParseException {
		PValueColorScale scale = new PValueColorScale(0.05 , Color.RED, Color.YELLOW, Color.GRAY);

		IScalePanel panel;

		panel = new PvalueScalePanel(SvgmapConstants.PVALUE, scale);


		return panel;
	}

	public static IScalePanel createLinear2SidedScalePanel(Integer idData, ExperimentFile mapping){
		try {
			Linear2SidedScalePanel panel = null;
			ColorScaleRangeExt range = null;
			String defaultLinearScaleValues = null;

			SvgmapSession session = (SvgmapSession) WebSession.get();
			HashMap<String, String> scaleRangesExp = session.getScaleDefaultRangeType().get(idData);

			if (scaleRangesExp == null || scaleRangesExp.get(SvgmapConstants.LINEAR2SIDED) == null) {				
                ExperimentDataView experiment = ExperimentDataViewJpaController.findExperimentDataView(idData);
				defaultLinearScaleValues = experiment.getLinearScaleDefaults();
				
			} else
				defaultLinearScaleValues = session.getScaleDefaultRangeType().get(idData).get(SvgmapConstants.LINEAR2SIDED);

			if (defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_USER)) {
				range = session.getScaleUserRange().get(idData).get(SvgmapConstants.LINEAR2SIDED);
			}

			if (defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_ALL_DATA) || defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_WITHOUT_OUTLIER)) {
				range = ((WicketApplication) Application.get()).getMappingsMatrixRanges(idData).get(defaultLinearScaleValues);
			}

			if (defaultLinearScaleValues.equals(SvgmapConstants.SCALE_RANGES_KEY)) {
				range = new ColorScaleRangeExt(0, 0);
			}

			LinearTwoSidedColorScale scale = new LinearTwoSidedColorScale(
					new ColorScalePoint(Double.parseDouble(range.getMin() + ""), Color.GREEN),
					new ColorScalePoint(Double.parseDouble(range.getMid() + ""), Color.BLACK),
					new ColorScalePoint(Double.parseDouble(range.getMax() + ""), Color.RED));

			panel = new Linear2SidedScalePanel(SvgmapConstants.LINEAR2SIDED, scale);
			panel.setDefaultValueType(defaultLinearScaleValues);
			panel.setExperimentId(idData);
			panel.resetScale(scale);
			return panel;

		} catch (Exception ex) {
			throw new WicketRuntimeException("Impossible to create linear scale panel.", ex);
		}
	}
}
