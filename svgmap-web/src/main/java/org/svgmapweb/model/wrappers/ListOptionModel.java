/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.wrappers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.wicket.model.IModel;

public class ListOptionModel implements IModel<List<SelectOption>> {

	private IModel<String> model;
	private List<SelectOption> res = new ArrayList();

	public ListOptionModel(IModel<String> m) {
		super();
		model = m;
	}

	public List<SelectOption> getObject() {
		res.clear();

		if (model.getObject() != null) {
			for (String obj : Arrays.asList(model.getObject(), ";")) {
				SelectOption opt = new SelectOption(obj, obj);
				res.add(opt);
			}

		}

		return res;

	}

	public void setObject(List<SelectOption> object) {
		String value = "";
		for (SelectOption opt : object)
			value += ";" + opt.getKey();
		
		if (value.isEmpty())
			model.setObject(value);
		else
			model.setObject(value.substring(1));
	}

	public void detach() {
	}
}
