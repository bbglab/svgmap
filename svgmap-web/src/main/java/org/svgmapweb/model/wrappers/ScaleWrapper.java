/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.svgmapweb.model.wrappers;

import org.svgmapweb.model.scales.IScalePanel;

public class ScaleWrapper implements java.io.Serializable {

	private Integer index;
	private IScalePanel panel;

	public IScalePanel getPanel() {
		return panel;
	}

	public void setPanel(IScalePanel columnName) {
		this.panel = columnName;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return panel.getPanelName();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ScaleWrapper other = (ScaleWrapper) obj;
		if (this.index != other.index && (this.index == null || !this.index.equals(other.index))) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + (this.index != null ? this.index.hashCode() : 0);
		return hash;
	}
	
}
