/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.wrappers;

import org.apache.wicket.model.IModel;



public class ArrayModel<T> implements IModel<T> {

	private int position;
	private T[] array;

	public ArrayModel(T[] array, int position) {

		if (position >= array.length) {
			throw new RuntimeException("Out of bound array position");
		}

		this.array = array;
		this.position = position;
	}

	public T getObject() {
		return array[position];
	}

	public void setObject(T object) {
		array[position] = object;
	}

	public void detach() {
		// Nothing
	}


}
