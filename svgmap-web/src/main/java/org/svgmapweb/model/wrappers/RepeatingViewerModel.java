/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.wrappers;

import java.util.List;
import org.apache.wicket.model.IModel;
import org.svgmapweb.model.wrappers.ColumnWrapper;


public class RepeatingViewerModel implements IModel {

	private List<IModel<ColumnWrapper>> model;

	public Object getObject() {
		return model;
	}

	public void setObject(Object object) {
		model = (List<IModel<ColumnWrapper>>) object;
	}

	public void detach() {
	}

}

