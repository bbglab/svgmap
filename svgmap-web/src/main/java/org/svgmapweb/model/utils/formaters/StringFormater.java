/**
 *  Copyright 2011 Universitat Pompeu Fabra.
 *
 *  This software is open source and is licensed under the Open Software License
 *  version 3.0. You may obtain a copy of the License at
 *
 *      http://www.opensource.org/licenses/osl-3.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.svgmapweb.model.utils.formaters;

public class StringFormater implements ITextFormater {

	private int maxLength;
	private boolean addDots;

	public StringFormater(final int maxLength, final boolean addDots) {
		super();
		this.maxLength = maxLength;
		this.addDots = addDots;
	}

	@Override
	public String format(final Object value) {

		String formatedValue = (value == null ? "" : value.toString());

		if (formatedValue.length() > maxLength) {
			formatedValue = formatedValue.substring(0, maxLength) + (addDots ? "..." : "");
		}

		return formatedValue;

	}

}
