/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.utils;

public class SvgmapConstants {

	public static final int TAB_IMG = 0;
	public static final String IMAGE = "image";

	public static final int TAB_TABLE = 1;
	public static final String TABLE = "table";

	public static final int REGION_INDEX = 0;
	public static final int KEY_INDEX = 1;

	public static final String INDEX = "gene";
	public static final String PVAL = "p<00";
	public static final String PVAL1 = "1";
	public static final String PVAL2 = "5";
	
	public static final String LINEAR = "linear";
	public static final String LINEAR2SIDED = "linear2sided";
	public static final String PVALUE = "pvalue";

	public static final String TYPE_MAPPING = "mapping";
	public static final String TYPE_ANNOTATION = "annotation";
	public static final String TYPE_SVG = "svg";

	public static final String SCALE_RANGES_ALL_DATA = "data";
	public static final String SCALE_RANGES_WITHOUT_OUTLIER = "outlier";
	public static final String SCALE_RANGES_KEY = "key";
	public static final String SCALE_RANGES_USER = "user";

	public static final Integer NUM_TYPE_FILES = 3;	
	public static final Double MAX_MB = 50.0;
	public static final String CHR_LNK = "%%";

	// This constants may be deduced from key ids for jpa classes (just improve performance)
	public static final String[] keysExperimentDataView = new String[]{"idData"};
	public static final String[] keysExperimentFile = new String[]{"idFile"};


	public static final String LINEAR_SCALE_DATA = "data";
	public static final String LINEAR_SCALE_WITHOUT_OUTLIER = "outlier";
	public static final String LINEAR_SCALE_KEY = "key";
	public static final String LINEAR_SCALE_USER = "user";


}
