/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.util.file.Folder;
import org.svgmapweb.ui.WicketApplication;

public class FileUtils {

	public static void checkFileExists(File newFile) throws IllegalStateException {
		if (newFile.exists()) {
			throw new IllegalStateException("File already exists " + newFile.getAbsolutePath());
		}
	}

	public static Folder getUploadFolder() {
		return ((WicketApplication) Application.get()).getUploadFolder();
	}

	public static Folder getProcessFolder() {
		return ((WicketApplication) Application.get()).getProcessFolder();
	}

	public static Folder createLoadFolder(String nameFolder) throws SecurityException {

		//File uploadFolder = new File(System.getProperty("user.home") + File.separator + nameFolder );
		File uploadFolder = new File(nameFolder);

		// create folders
		uploadFolder.getAbsoluteFile().mkdirs();

		return new Folder(uploadFolder);
	}
	
	public static String asciiToHtml(String ascii) {
		return ascii.replace("&lt;", "<").replace("&gt;", ">");
	}

        /*
         * To convert the InputStream to String we use the
         * Reader.read(char[] buffer) method. We iterate until the
         * Reader return -1 which means there's no more data to
         * read. We use the StringWriter class to produce the string.
         */
    public static String convertStreamToString(InputStream is)
            throws IOException {

        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(
                        new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

	// These are the files to include in the ZIP file
	public static void makeZipFile(List<String> fileNames, String zipName) {
		byte[] buf = new byte[1024];

		String cleanFileNameRegEx = "(.+)_[0-9]{14}_[\\-0-9]+(\\.[a-z]{3})";
		Pattern pattern = Pattern.compile(cleanFileNameRegEx);

		try {
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipName));

			// Compress the files
			for (String absoluteFileName : fileNames) {
				FileInputStream in = new FileInputStream(absoluteFileName);


				// Add ZIP entry to output stream.
				String fileName = absoluteFileName.substring(absoluteFileName.lastIndexOf(File.separator));
				String fileNameInZip = fileName;
				Matcher matcher = pattern.matcher(fileName);

				if (matcher.matches()) {
					fileNameInZip = matcher.group(1) + matcher.group(2);
				}
				out.putNextEntry(new ZipEntry(fileNameInZip));
				// Transfer bytes from the file to the ZIP file
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				// Complete the entry
				out.closeEntry();
				in.close();
			}

			// Complete the ZIP file
			out.close();
		} catch (IOException e) {
			throw new WicketRuntimeException(e);
		}
	}

	public static byte[] createZip(Map files) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ZipOutputStream zipfile = new ZipOutputStream(bos);
		Iterator i = files.keySet().iterator();
		String fileName = null;
		ZipEntry zipentry = null;
		while (i.hasNext()) {
			fileName = (String) i.next();
			zipentry = new ZipEntry(fileName);
			zipfile.putNextEntry(zipentry);
			zipfile.write((byte[]) files.get(fileName));
		}
		zipfile.close();
		return bos.toByteArray();
	}
}
