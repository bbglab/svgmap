/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.utils;

import org.svgmapweb.model.wrappers.SelectOption;
import cern.colt.matrix.ObjectFactory2D;
import cern.colt.matrix.ObjectMatrix2D;
import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.csv.CSVParser;
import org.apache.wicket.Application;
import org.apache.wicket.WicketRuntimeException;
import org.gitools.matrix.model.MatrixView;
import org.gitools.persistence.PersistenceException;
import org.gitools.persistence.PersistenceUtils;
import org.gitools.utils.CSVStrategies;
import org.svgmapweb.model.RegionDetails;
import org.svgmapweb.ui.WicketApplication;
import org.svgmapweb.db.jpa.ExperimentFile;

public class SvgmapUtils {

	public static void loadOptionsFromMappingFile(MatrixView mappingMatrix, List<SelectOption> imgColumns, List<SelectOption> pvalColumns, boolean filter) {

		SelectOption c = null;

		for (int i = 0; i < mappingMatrix.getCellAttributes().size(); i++) {
			c = new SelectOption(mappingMatrix.getCellAttributes().get(i).getName(), mappingMatrix.getCellAttributes().get(i).getName());
			if (filter && mappingMatrix.getCellAttributes().get(i).getName().toLowerCase().startsWith(SvgmapConstants.PVAL))
				pvalColumns.add(c);
			else
				imgColumns.add(c);
		}
	}

	/**
	 * 	Get experiment header
	 * @param idData
	 * @param type
	 * @return
	 * @throws IOException
	 * @throws PersistenceException
	 */
	public static List<String> readHeader(Integer idData, String type) throws IOException, PersistenceException {
		List<String> header  = new ArrayList<String>();

		ExperimentFile file = ((WicketApplication) Application.get()).getExperimentFile(idData, type);

		if (file != null) {
			header.addAll(Arrays.asList(SvgmapUtils.readHeaderCSVfile(new File(FileUtils.getUploadFolder() + File.separator + file.getPath()))));
		}
		return header;
	}

	public static String[] readHeaderCSVfile(File file) throws IOException {
		Reader reader = PersistenceUtils.openReader(file);
		CSVParser parser = new CSVParser(reader, CSVStrategies.TSV);

		// header
		return parser.getLine();
	}

	public static ObjectMatrix2D parseTextFile2Matrix(File file) throws IOException, PersistenceException {
		return parseTextFile2MatrixHeader(file, false);
	}

	public static ObjectMatrix2D parseTextFile2MatrixHeader(File file, boolean addHeader) throws IOException, PersistenceException {

		Reader reader = PersistenceUtils.openReader(file);
		CSVParser parser = new CSVParser(reader, CSVStrategies.TSV);

		// header
		String[] hdr = parser.getLine();
		int numColumns = hdr.length;

		// body
		String[] fields;
		List<String> rawData = new ArrayList<String>();

		if (addHeader) {
			for (int i = 0; i < hdr.length; i++) {
				rawData.add(hdr[i]);
			}
		}

		while ((fields = parser.getLine()) != null) {
			if (fields.length > hdr.length) {
				throw new PersistenceException("Number of fields greater than number of header fields at line " + parser.getLineNumber());
			}

			for (int i = 0; i < fields.length; i++) {
				rawData.add(fields[i]);
			}

			for (int i = 0; i < (hdr.length - fields.length); i++) {
				rawData.add(new String(""));
			}
		}

		int numRows = rawData.size() / hdr.length;
		ObjectMatrix2D data = ObjectFactory2D.dense.make(numRows, numColumns);

		int offs = 0;
		for (int row = 0; row < numRows; row++) {
			for (int col = 0; col < numColumns; col++) {
				data.setQuick(row, col, rawData.get(offs++));
			}
		}

		rawData.clear();
		reader.close();

		return data;
	}


	public static String getDetailsAsString(List<String> header, List<RegionDetails> details) {

		StringBuilder str = new StringBuilder();

		for (String h : header) {
			str.append(h);
			str.append('\t');
		}
		str.append('\n');

		for (String h : header) {
			str.append(h);
			str.append('\t');
		}
		str.append('\n');

		for (RegionDetails reg : details) {
			str.append(reg.getId());
			str.append('\t');
			str.append(reg.getName());
			str.append('\t');
			str.append(reg.getRegion());
			str.append('\t');
			for (String val : reg.getValues()) {
				str.append(val);
				str.append('\t');
			}
			str.append('\n');
		}
		return str.toString();
	}

	public static List<String> parseKeywordsToList(String searchText) {
		List<String> listStr = new ArrayList();
		List<String> auxListStr = new ArrayList();

		if (searchText == null)
			return listStr;

		searchText = searchText.trim();
		searchText = searchText.replace("\r", "");

		if (searchText.isEmpty())
			return listStr;

		if (searchText.contains("\n"))
			auxListStr = Arrays.asList(searchText.split("\n"));
		else
			auxListStr.add(searchText);

		for (String res : auxListStr) {
			if (res.contains(","))
				for (String r : res.split(","))
					listStr.add(r.replace(" ", ""));
			else
			if (res.contains("\t"))
				for (String r : res.split("\t"))
					listStr.add(r.replace(" ", ""));
			else
				listStr.add(res.replace(" ", ""));
		}


		return listStr;
	}

	public static String roundToDecimals(double num, int dec) {
		StringBuilder pattern = new StringBuilder();
		pattern.append("########.");
		for (int i = 0; i < dec; i++)
			pattern.append("#");

		DecimalFormat twoDForm = new DecimalFormat(pattern.toString());

		return twoDForm.format(num);
	}

	public static String generateUniqueName() {
		return generateUniqueName(null);
	}

	public static String generateUniqueName(String[] nameParts) {
		Random generator = new Random();
		Date dateNow = new Date();
		SimpleDateFormat format = new SimpleDateFormat("MMddyyyyHHmmss_" + generator.nextInt());
		StringBuilder dateStr = new StringBuilder(format.format(dateNow));
		StringBuilder tmpFile = new StringBuilder();

		if (nameParts != null) {
			for (String n : nameParts) {
				tmpFile.append(n);
				tmpFile.append("_");
			}
		}

		return tmpFile.append(dateStr).toString();
	}

	public static List<String> getExcludedColumnsFromMappingFile(MatrixView mappingMatrix) {
		List<String> excludedColsRange = new ArrayList<String>();
		String name = "";

		for (int i = 0; i < mappingMatrix.getCellAttributes().size(); i++) {
			name = mappingMatrix.getCellAttributes().get(i).getName();
			if (name.toLowerCase().startsWith(SvgmapConstants.PVAL))
				excludedColsRange.add(name);
		}
		return excludedColsRange;
	}

	public static List<RegionDetails> mergeLists(Collection<List<RegionDetails>> values) {
		List<RegionDetails> lDetails = new ArrayList();
		for (List<RegionDetails> rows : values)
			lDetails.addAll(rows);

		return lDetails;
	}

	/**
	 * Load key names from annotations and mapping files
	 * @param idData
	 * @return
	 */
	public static List<String> loadKeyNamesExperiment(Integer idData) {
			Object obj; double val; String label;
			List<String> columnData = new ArrayList(0);

			try {
				Map<String, String> nameKeyAnnotationsData = ((WicketApplication) Application.get()).getKeyNameAnnotationsMap(idData);
				MatrixView mappingMatrix =  ((WicketApplication) Application.get()).getMappingMatrix(idData);

				if (nameKeyAnnotationsData == null) {
					for (int row = 0; row < mappingMatrix.getRowCount(); row++) {
						label = mappingMatrix.getRowLabel(row);
						columnData.add(label);
					}
				} else {
					for (int row = 0; row < mappingMatrix.getRowCount(); row++) {
						label = mappingMatrix.getRowLabel(row);
						columnData.add(label);
						if (label != null && nameKeyAnnotationsData.get(label) != null)
							columnData.add(nameKeyAnnotationsData.get(label));
					}
				}

				Collections.sort(columnData);

			} catch(Exception e) {
				e.printStackTrace();
				throw new WicketRuntimeException("Error while loading filter data");
			}

		return columnData;
	}

	/**
	 * Get Values From Form.
	 * @param upload
	 * @return
	 * @throws IOException
	 */
	public Collection<String> getValuesFromForm(String textArea) throws IOException {

		// Create list of items
		BufferedReader in = new BufferedReader(new CharArrayReader(textArea.toCharArray()));

		int c;
		Collection<String> values = new ArrayList<String>();
		String value = "";
		while ((c = in.read()) != -1) {
			if (c == ' ' || c == '\r') {
				continue; // Skip all spaces, returns
			}
			if (c == '\t' || c == '\n') {
				if (!value.equals("")) {
					values.add(value);
					value = "";
				}
			} else {
				value = value + ((char) c);
			}
		}
		if (!value.equals("")) {
			values.add(value);
			value = "";
		}
		return values;
	}

	public static StringBuilder createTextForExperimentSearch(Integer idData, int tab) throws PersistenceException, IOException {
		StringBuilder searchTxt = new StringBuilder();

		List<String> hdrMapp = SvgmapUtils.readHeader(idData, SvgmapConstants.TYPE_MAPPING);
		List<String> hdrAnn = SvgmapUtils.readHeader(idData, SvgmapConstants.TYPE_ANNOTATION);

		if (tab == SvgmapConstants.TAB_IMG)
			searchTxt.append("Keyword");
		else
			searchTxt.append("Keywords");

		if (hdrMapp != null && hdrMapp.size() > 2) {
			searchTxt.append("\n");
			searchTxt.append("(");
			searchTxt.append(hdrMapp.get(1));
			if (hdrAnn != null && hdrAnn.size() > 2) {
				searchTxt.append(" or ");
				searchTxt.append(hdrAnn.get(1));
			}
			searchTxt.append(")");
		} else {
			if (tab == SvgmapConstants.TAB_IMG)
				searchTxt.append("(key or name)");
			else
				searchTxt.append("(keys or names)");
		}
		return searchTxt;
	}

	public static boolean containsIgnoreCase(List<String> keyList, String key) {
		for (String k : keyList)
			if (k.equalsIgnoreCase(key))
				return true;
		return false;
	}

	/**
	 * All keywords name found in annotationMatrix are replaced by the keyId
	 * Those keywords that they weren't found, are leave it in the output list
	 * @param expData
	 * @param keywords
	 * @return
	 */
	public static List<String> convertKeywordsToKeys(Integer idData, String rawKey) throws Exception {

		if (rawKey == null || rawKey.isEmpty())
			return new ArrayList<String>();

		Map<String, String> expData = ((WicketApplication) Application.get()).getKeyNameAnnotationsMap(idData);
		List<String> keyList = SvgmapUtils.parseKeywordsToList(rawKey);

		if (expData == null || expData.isEmpty())
			return keyList;

		//Converting gene names to idGenes
		List<String> keysId = new ArrayList();
		List<String> keywordsAux = new ArrayList();

		for (String k : expData.keySet()) {
			for (String keyword : keyList) {
				if (keyword.equalsIgnoreCase(expData.get(k))) {
					keysId.add(k);
					keywordsAux.add(keyword);
				}
			}
		}

		keyList.removeAll(keywordsAux);
		keyList.addAll(keysId);
		return keyList;
	}
}
