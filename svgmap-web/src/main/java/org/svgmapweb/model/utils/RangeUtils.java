/**
 *
 * Copyright 2011 Universitat Pompeu Fabra.
 *
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 *
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model.utils;

import cern.colt.list.DoubleArrayList;
import hep.aida.bin.QuantileBin1D;
import java.util.HashMap;
import java.util.List;
import org.gitools.matrix.model.MatrixView;
import org.svgmapweb.model.scales.ColorScaleRangeExt;
import weka.filters.unsupervised.attribute.InterquartileRange;


public class RangeUtils {

	public static ColorScaleRangeExt computeRangeColumns(MatrixView mappingsMatrix, List<String> excludeCols) {
		Double val = 0.0, max = 0.0, min = Double.MAX_VALUE;
		HashMap<String, ColorScaleRangeExt> ranges = new HashMap<String, ColorScaleRangeExt>();

		for (int i = 0; i < mappingsMatrix.getCellAttributes().size(); i++) {
			if (excludeCols != null && excludeCols.contains(mappingsMatrix.getCellAttributes().get(i).getName())) {
				for (int r = 0; r < mappingsMatrix.getRowCount(); r++) {
					for (int c = 0; c < mappingsMatrix.getColumnCount(); c++) {
							val = (Double) mappingsMatrix.getCellValue(r, c, i);
						if (val != null && ! val.isNaN()) {
							if (val > max)
								max = val;
							if (val < min)
								min = val;
						}
					}
				}
			}
		}
		return new ColorScaleRangeExt(min, (min+max)/2, max);
	}

	public static ColorScaleRangeExt computeRangeWithoutOutliers(MatrixView mappingsMatrix, List<String> includeCols) {
			Double val = 0.0, max = 0.0, min = Double.MAX_VALUE;

			InterquartileRange dataWithououtliers = new InterquartileRange();
			dataWithououtliers.getOutlierFactor();

			DoubleArrayList allValues = new DoubleArrayList();

			for (int i = 0; i < mappingsMatrix.getCellAttributes().size(); i++) {
				if (includeCols != null && includeCols.contains(mappingsMatrix.getCellAttributes().get(i).getName())) {
				for (int r = 0; r < mappingsMatrix.getRowCount(); r++) {
					for (int c = 0; c < mappingsMatrix.getColumnCount(); c++) {
							val = (Double) mappingsMatrix.getCellValue(r, c, i);
						if (val != null && ! val.isNaN()) {
							allValues.add(val);
							if (val > max) 	max = val;
							if (val < min) 	min = val;
						}
					}
				}
			}
			}

			QuantileBin1D quantileBin = new QuantileBin1D(0.0);
			quantileBin.addAllOf(allValues);
			double Q1 = quantileBin.quantile(0.25);
			double Q3 = quantileBin.quantile(0.75);
			double IQR = Q3-Q1;
			Double IQRmin = Q1-1.5*IQR;
			Double IQRmax = Q3+1.5*IQR;
			if (min < IQRmin) min = IQRmin;
			if (max > IQRmax) max = IQRmax;

			return new ColorScaleRangeExt(min, (min+max)/2, max);
	}

	public static ColorScaleRangeExt computeRangeColumnsKey(MatrixView mappingsMatrix, String key, List<String> includeCols) {
		Double val = 0.0, max = 0.0, min = Double.MAX_VALUE;

		for (int i = 0; i < mappingsMatrix.getCellAttributes().size(); i++) {

			if (includeCols != null && includeCols.contains(mappingsMatrix.getCellAttributes().get(i).getName())) {
				for (int c = 0; c < mappingsMatrix.getColumnCount(); c++) {
					for (int r = 0; r < mappingsMatrix.getRowCount(); r++) {
						if (mappingsMatrix.getRowLabel(r).equalsIgnoreCase(key)) {
								val = (Double) mappingsMatrix.getCellValue(r, c, i);
							if (val != null && ! val.isNaN()) {
								if (val > max)
									max = val;
								if (val < min)
									min = val;
							}
						}
					}
				}
			}
		}
		return new ColorScaleRangeExt(min, (min+max)/2, max);
	}
}
