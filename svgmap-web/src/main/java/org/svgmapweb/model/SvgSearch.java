/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model;

import java.util.ArrayList;
import java.util.List;
import org.svgmapweb.model.wrappers.ColumnWrapper;
import org.svgmapweb.model.wrappers.ScaleWrapper;
import org.svgmapweb.model.filters.Filter;
import org.svgmapweb.db.jpa.ExperimentDataView;


public class SvgSearch implements java.io.Serializable{

	/*keys selected (comma separated)*/
	private String keyword = null;

	private String searchText = null;

	/*Image column selected*/
	private List<ColumnWrapper> imgColumnOptionsSelected = new ArrayList<ColumnWrapper>();

	/*selected scale*/
	private ScaleWrapper scale = null;

	/*Selected experiment */
	private ExperimentDataView experiment = new ExperimentDataView();

	/*Selected tab*/
	private Integer selectedTab = null;

	/*generated scale*/
	private String ScalefileName  = null;

	/*Generated experiment files*/
	private List<String> ExperimentFileNames  = new ArrayList<String>();

	/*Selected filters for a search*/
	private List<Filter> ExperimentFilters = new ArrayList<Filter>();


	public List<String> getExperimentFileNames() {
		return ExperimentFileNames;
	}

	public void setExperimentFileNames(List<String> ExperimentFileNames) {
		this.ExperimentFileNames = ExperimentFileNames;
	}

	public  ExperimentDataView getExperiment() {
		return experiment;
	}

	public void setExperiment(ExperimentDataView experiment) {
		this.experiment = experiment;
	}

	public ScaleWrapper getScale() {
		return scale;
	}

	public void setScale(ScaleWrapper scale) {
		this.scale = scale;
	}

	public List<ColumnWrapper> getImgColumnOptionsSelected() {
		return imgColumnOptionsSelected;
	}

	public void setImgColumnOptionsSelected(List<ColumnWrapper> columns) {
		this.imgColumnOptionsSelected = columns;
	}


	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getSelectedTab() {
		return selectedTab;
	}

	public void setSelectedTab(Integer selectedTab) {
		this.selectedTab = selectedTab;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getScalefileName() {
		return ScalefileName;
	}

	public void setScalefileName(String ScalefileName) {
		this.ScalefileName = ScalefileName;
	}

	public List<Filter> getExperimentFilters() {
		return ExperimentFilters;
	}

	public void setExperimentFilters(List<Filter> ExperimentFilters) {
		this.ExperimentFilters = ExperimentFilters;
	}
}
