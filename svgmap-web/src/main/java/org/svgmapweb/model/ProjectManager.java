/**
 * 
 * Copyright 2011 Universitat Pompeu Fabra.
 * 
 * This software is open source and is licensed under the Open Software License
 * version 3.0.
 * 
 * You may obtain a copy of the License at
 * http://www.opensource.org/licenses/osl-3.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.
 * 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.svgmapweb.model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


public class ProjectManager {

	private File projectFile;
	private Project project;

	private Unmarshaller u;
	private Marshaller m;

	private static JAXBContext jc = null;

	private static JAXBContext getJAXBContext() {
		if (jc == null) {
			try {
				jc = JAXBContext.newInstance(Project.class);
			} catch (JAXBException e) {
				e.printStackTrace();
				throw new RuntimeException("ERROR on JAXB context creation", e);
			}
		}
		return jc;
	}

	private static Unmarshaller getUnmarshaller(JAXBContext jc, ProjectManager projectManager) throws JAXBException {
		Unmarshaller unmarshaller = jc.createUnmarshaller();

		return unmarshaller;
	}

	private static Marshaller getMarshaller(JAXBContext jc, ProjectManager projectManager) throws JAXBException {
		Marshaller marshaller = getJAXBContext().createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		return marshaller;
	}

	public ProjectManager(File projectFile) throws JAXBException {
		this(projectFile, getJAXBContext());
	}


	public ProjectManager(File projectFile, JAXBContext jaxbContext) throws JAXBException {
		super();

		this.projectFile = projectFile;
		this.u = getUnmarshaller(jaxbContext, this);
		this.m = getMarshaller(jaxbContext, this);

		if (!this.projectFile.exists()) {
			try {
				this.projectFile.createNewFile();
			} catch (IOException e) {
				throw new RuntimeException("ERROR on file creation. [file=" + projectFile + "]");
			}
			this.project = new Project();
			try {
				marshal();
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("ERROR on project marshalling");
			}
		} else {
			try {
				unmarshal();
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("ERROR on project unmarshalling");
			}
		}
	}

	private void unmarshal() throws IOException, JAXBException {

		// Unmarshal the project
		project = (Project) u.unmarshal(new BufferedInputStream(new FileInputStream(projectFile)));

	}

	private void marshal() throws JAXBException, IOException {

		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		// Marshal the project XML
		m.marshal(project, new FileOutputStream(projectFile));

		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

	}

	public Project getProject() {
		return project;
	}
}
