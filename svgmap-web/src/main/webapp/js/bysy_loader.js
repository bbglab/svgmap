 function setupFunc() {
   document.getElementsByTagName('body')[0].onclick = clickFunc;
   hideBusysign();
   placeFloatingLayer(document.getElementById('bysy_indicator'));
   if (Wicket != undefined){
         Wicket.Ajax.registerPreCallHandler(showBusysign);
         Wicket.Ajax.registerPostCallHandler(hideBusysign);
         Wicket.Ajax.registerFailureHandler(hideBusysign);
   }
 }

 function hideBusysign() {
   hideFloatingLayer('bysy_indicator');
 }

 function showBusysign() {
   showFloatingLayer('bysy_indicator');
 }

 function clickFunc(eventData) {
   var clickedElement = (window.event) ? event.srcElement : eventData.target;

   /* If is not desired that a element not use bysy_indicator */
   if (eventData.explicitOriginalTarget.parentNode.className != undefined && eventData.explicitOriginalTarget.parentNode.className == "no-bysy_indicator") {
                hideBusysign();
   } else {
                if (clickedElement.tagName.toUpperCase() == 'BUTTON' || clickedElement.tagName.toUpperCase() == 'A' || clickedElement.parentNode.tagName.toUpperCase() == 'A'
                 || (clickedElement.tagName.toUpperCase() == 'INPUT' && (clickedElement.type.toUpperCase() == 'BUTTON' || clickedElement.type.toUpperCase() == 'SUBMIT'))) {
                 showBusysign();
           }
   }
 }



<!-- Copyright 2003, Sandeep Gangadharan -->
<!-- For more free scripts go to http://sivamdesign.com/scripts/ -->*/


var y1 = 50;   // change the # on the left to adjuct the Y co-ordinate
var dom = false;
(document.getElementById) ? dom = true : false;

function hideFloatingLayer(id) {
	if (dom) {
		document.getElementById(id).style.display='none';
	}
}


function showFloatingLayer(id) {
  if (document.getElementById(id)) {
	if (dom) {
		document.getElementById(id).style.display='block';
		//placeFloatingLayer(document.getElementById(id));
	}
  }
}

function placeFloatingLayer(i) {

	  if (i) {
		var w_height;
		w_height2 = window.innerHeight;
		w_height = document.documentElement.clientHeight;
		var i_height;
		i_height = i.offsetHeight;
		if (dom && !document.all) {i.style.top = (window.pageYOffset + w_height/2 - i_height/2) + "px";}
/*		if (dom && !document.all) {i.style.top = (window.pageYOffset + w_height2/2 - i_height/2) + "px";}*/
		if (document.all) {i.style.top = document.documentElement.scrollTop + (document.documentElement.clientHeight - (document.documentElement.clientHeight-y1)) + "px";}
		window.setTimeout(function (){ placeFloatingLayer(i);}, 250); }
 }


 window.onload = setupFunc;

