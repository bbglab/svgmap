--
-- 
-- Copyright 2011 Universitat Pompeu Fabra.
-- 
-- This software is open source and is licensed under the Open Software License
-- version 3.0.
-- 
-- You may obtain a copy of the License at
-- http://www.opensource.org/licenses/osl-3.0
-- 
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
-- implied.
-- 
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- 
-- Definition of table `ExperimentDataType`
--

CREATE TABLE  `ExperimentDataType` (
  `idData` int(11) NOT NULL,
  `idFile` int(11) NOT NULL,
  `type` varchar(150) NOT NULL,
  PRIMARY KEY (`idData`,`idFile`)
);

--
-- Definition of table `ExperimentFile`
--

CREATE TABLE  `ExperimentFile` (
  `idFile` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  PRIMARY KEY (`idFile`)
);

--
-- Definition of table `ExperimentDataView`
--

CREATE TABLE  `ExperimentDataView` (
  `idData` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `creationDate` datetime DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `numImages` int(11) NOT NULL DEFAULT '1',
  `pvalueFilter` int(11) NOT NULL DEFAULT '0',
  `imageOptions` varchar(4000) DEFAULT '',
  `pvalueOptions` varchar(4000) DEFAULT '',
  `scaleOptions` varchar(4000) DEFAULT NULL,
  `geneLink` varchar(400) DEFAULT NULL,
  `defaultTabSelected` varchar(400) DEFAULT 'image',
  `linearScaleDefaults` varchar(400) DEFAULT 'outlier',
  `relevantKeys` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`idData`)
);

